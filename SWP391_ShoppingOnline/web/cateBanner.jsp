<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Home | iLocal Shop</title>
    <link rel="icon" type="image/x-icon" href="img/logo.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="path/to/your/bootstrap.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

    <!-- Your custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <style>
        .slider {
            height: 100%;
            border-radius: 10px;
            display: none;
            animation: slide 0.2s linear forwards;
        }

        @keyframes slide {
            0% {
                width: 0%;
            }
            100% {
                width: 100%;
            }
        }

        .slider-nav {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            font-size: 2em;
            cursor: pointer;
        }

        .fa-chevron-left {
            left: 10px;
        }

        .fa-chevron-right {
            right: 10px;
        }

        .slider-indicators {
            display: flex;
            justify-content: center;
            margin-top: 10px;
        }

        .slider-indicator {
            width: 10px;
            height: 10px;
            background-color: #ccc;
            border-radius: 50%;
            margin: 0 5px;
            cursor: pointer;
        }

        .slider-indicator.active {
            background-color: #333;
        }
    </style>
</head>
<body>
<section class="hero" style="height: 70vh;">

    <div class="side-bar">
        <h4><i class="fa-solid fa-bars"></i>Brands</h4>
        <ul>
            <li>
                <div onclick="scrollToAnchor('anchor')"><a href="home#anchor">All</a></div>
            </li>
            <c:forEach items="${listCategory}" var="listC">
                <li>
                    <div onclick="location.href = 'home?category=${listC.cateID}#category'">${listC.cateName}</div>
                </li>
            </c:forEach>
        </ul>
    </div>

    <div id="slider" class="content">
        <i class="fas fa-chevron-left slider-nav" onclick="changeSlider(-1)" style="color: #FFFF;"></i>
        <c:forEach items="${listSlider}" var="slider" varStatus="status" >
            <img id="slider${status.index}" class="slider" src="./img/${slider.sliderImage}" alt="#">
        </c:forEach>
        <i class="fas fa-chevron-right slider-nav" onclick="changeSlider(1)" style="color: #FFFF;"></i>
           <div class="slider-indicators">
        <c:forEach items="${listSlider}" var="slider" varStatus="status">
            <div style="color: #FFFF;"class="slider-indicator" onclick="jumpToSlide(${status.index})"></div>
        </c:forEach>
    </div>
    </div>

</section>

<!-- Bootstrap JS (popper and bootstrap.min.js) -->
<script src="path/to/your/jquery.slim.min.js"></script>
<script src="path/to/your/popper.min.js"></script>
<script src="path/to/your/bootstrap.min.js"></script>

<script>
    let sliderList = document.querySelectorAll('#slider img');
    let sliderIndex = 0;

    function changeSlider(step) {
        sliderList[sliderIndex].style.display = 'none';
        sliderIndex += step;

        if (sliderIndex > sliderList.length - 1) {
            sliderIndex = 0;
        }

        if (sliderIndex < 0) {
            sliderIndex = sliderList.length - 1;
        }

        sliderList[sliderIndex].style.display = 'block';
        sliderList[sliderIndex].style.transition = 'transform 0.5s';

        updateSliderIndicators();
    }

    function jumpToSlide(index) {
        sliderList[sliderIndex].style.display = 'none';
        sliderIndex = index;
        sliderList[sliderIndex].style.display = 'block';
        sliderList[sliderIndex].style.transition = 'transform 0.5s';
        updateSliderIndicators();
    }

    function updateSliderIndicators() {
        document.querySelectorAll('.slider-indicator').forEach((indicator, index) => {
            indicator.classList.remove('active');
            if (index === sliderIndex) {
                indicator.classList.add('active');
            }
        });
    }

    changeSlider(0);

    setInterval(function () {
        changeSlider(1);
    }, 5000); // 1000 milliseconds = 1 second
</script>

</body>
</html>
