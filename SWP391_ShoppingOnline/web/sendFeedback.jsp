<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${product.proName} | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <!-- Link Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
        <!-- Link File CSS -->
        <link rel="stylesheet" href="css/styles.css">
        <!-- Add your custom CSS styles here -->
        <link rel="stylesheet" href="css/product-detail.css">
        <style>
            *{
                margin: 0;
                padding: 0;
            }
            .rate {
                float: left;
                height: 46px;
                padding: 0 10px;
            }
            .rate:not(:checked) > input {
                position:absolute;
                top:-9999px;
            }
            .rate:not(:checked) > label {
                float:right;
                width:1em;
                overflow:hidden;
                white-space:nowrap;
                cursor:pointer;
                font-size:30px;
                color:#ccc;
            }
            .rate:not(:checked) > label:before {
                content: '★ ';
            }
            .rate > input:checked ~ label {
                color: #ffc700;
            }
            .rate:not(:checked) > label:hover,
            .rate:not(:checked) > label:hover ~ label {
                color: #deb217;
            }
            .rate > input:checked + label:hover,
            .rate > input:checked + label:hover ~ label,
            .rate > input:checked ~ label:hover,
            .rate > input:checked ~ label:hover ~ label,
            .rate > label:hover ~ input:checked ~ label {
                color: #c59b08;
            }

        </style>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
        <form action="SendFeedback" method="post" onsubmit="return checkInput();">
            <div class="product-detail-container">
                <select name="pid" class="product-description">

                    <c:forEach items="${productList}" var="p">
                        <option value="${p.proID}">${p.proName}</option> 
                    </c:forEach>

                </select>
            </div>
        <div class="product-details">
            <h2>Send feedback</h2>
                <span><b>Feedback content:</b></span>
                <textarea style="width: 100%; padding: 5px;" name="feedbackContent" id="feedbackContent"></textarea>
                <input type="hidden" name="ordId" value="${ordId}" >

                <div><b>Feedback Rate: </b></div>
                <div style="margin-right: 20px">
                    <div class="rate">
                        <input type="radio" id="star5" name="rate" value="5" required>
                        <label for="star5" title="text">5 stars</label>
                        <input type="radio" id="star4" name="rate" value="4" >
                        <label for="star4" title="text">4 stars</label>
                        <input type="radio" id="star3" name="rate" value="3" >
                        <label for="star3" title="text">3 stars</label>
                        <input type="radio" id="star2" name="rate" value="2" >
                        <label for="star2" title="text">2 stars</label>
                        <input type="radio" id="star1" name="rate" value="1" >
                        <label for="star1" title="text">1 star</label>
                    </div>
                </div>
                <br>
                <div><b>Image: </b></div>
                <input class="form-control" id="imgadd" onchange="changeimgadd()" name="image" type="file" >

                <input name="proimage" id="imageadd" value="" type="hidden" >
                <br><!-- comment -->
                <image  src="" id="demoimgadd" style="margin-top: 5px;" width="30%">

<!--                    <input name="pid" value="${product.proID}" type="hidden">-->
                <br><br><!-- comment -->
                <button type="submit" style="padding: 8px; font-size: 12px" class="btn btn-primary">Submit</button>

            </form>
        </div>
    </div>


    <jsp:include page="footer.jsp"></jsp:include>
</body>
<script>
    function changeimgadd() {
        var file = document.getElementById("imgadd").files[0];
        if (file.name.match(/.+\.(jpg|png|jpeg)/i)) {
            if (file.size / (1024 * 1024) < 5) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(file);
                fileReader.onload = function () {
                    document.getElementById("imageadd").value = (fileReader.result);
                    document.getElementById("demoimgadd").src = (fileReader.result);
                }
            } else {
                uploadError();
            }
        } else {
            uploadError();
        }
    }
    function uploadError() {
        alert('Please upload photo file < 5MB')
        document.getElementById("imgadd").files[0].value = ''
        document.getElementById("imgadd").type = '';
        document.getElementById("imgadd").type = 'file';
    }

</script>

</html>
