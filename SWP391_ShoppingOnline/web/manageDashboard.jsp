<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dashboard | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
        <!-- ======= Styles ====== -->
        <link rel="stylesheet" href="css/admin_manager.css">
        <style>
            table th {
                text-align: center;
            }
            .pagination input {
                width: 60px;
                margin-left: 5px;
                margin-right: 5px;
                text-align: center;
            }
            .pagination {
                margin-top: 20px;
            }
            .details {
                display: block;
            }
        </style>
    </head>
    <body>
        <!-- ================ Order Details List ================= -->

        <div class="details">
            <div class="recentOrders">
                <form action="../sale/manageOrder" method="post" id="form">
                    Select status:
                    <select class="form-control" id="statusOrder" name="status">
                        <option value="-1" ${statusOrder==null ? 'selected' : '' }>All status</option>
                        <option value="1" ${statusOrder==1 ? 'selected' : '' }>Created</option>
                        <option value="2" ${statusOrder==2 ? 'selected' : '' }>Paid</option>
                    </select>
                    <table>
                        <thead>
                            <tr>
                                <td style="text-align: center; width: 30%">Contact Name</td>
                                <td style="text-align: center">Price</td>
                                <td style="text-align: center">Order Date</td>
                                <td style="text-align: center">Payment</td>
                                <td style="text-align: center">Action</td>
                            </tr>
                        </thead>

                        <tbody>
                            <c:forEach items="${listOrderNull}" var="o">
                                <tr>
                                    <td style="width: 30%; text-align: center">${o.orderContactName}</td>
                                    <td style="text-align: center;"><fmt:formatNumber value="${o.orderTotalPrice}" type="number" pattern="#,##0" /></td>
                                    <td style="width: 23%; text-align: center">${o.orderDate}</td>

                                    <c:if test="${o.orderSoID eq '1'}">
                                        <td style="text-align: center;">Created</td>
                                        <td style="text-align: center;"><button class="status delivered" style="border: none; background-color: red" disabled>Select</button></td>
                                    </c:if>

                                    <c:if test="${o.orderSoID eq '2'}">
                                        <td style="text-align: center;">Paid</td>

                                <input type="hidden" name="orderID" value="${o.orderID}">
                                <td style="text-align: center;"><a href="../sale/manageOrder?orderID=${o.orderID}" class="status delivered"  style="border: none; text-decoration: none">Select</a></td>

                            </c:if>
                            </tr>
                        </c:forEach>

                        </tbody>

                    </table>
                </form>


            </div>
            <input type="hidden" id="pageNo" name="pageNo" value="${currentPage}">
            <input type="hidden" id="totalPage" name="pageNo" value="${totalPage}">
            <div class="pagination" style="text-align: center">
                Page
                <input type="number" value="${currentPage}" min="1" max="${totalPage}" id="currentPage"/>
                /${totalPage}</div>

        </div>
        <script>
            function submitForm() {
                document.getElementById("form").submit();
            }

            // handle filter statusOrder
            const status = document.querySelector('#statusOrder');
            status.addEventListener('change', () => {
                const statusOrder = document.querySelector('#statusOrder').value;
                const pageNo = document.querySelector('#pageNo').value;
                window.location.href = 'home?statusOrder=' + statusOrder + '&pageNo=1';
            });

            // handle pagination
            const page = document.querySelector('#currentPage');

            page.addEventListener('change', () => {
                const currentPageRaw = document.getElementById('currentPage').value;
                const totalPage = document.getElementById('totalPage').value;
                validatePage(currentPageRaw, totalPage);
                const currentPage = document.getElementById('currentPage').value;
                changePage(currentPage);
            });
            function changePage(pageNo) {
                const statusOrder = document.querySelector('#statusOrder').value;
                window.location.href = 'home?statusOrder=' + statusOrder + '&pageNo=' + pageNo;
            }

            function validatePage(pageNo, totalPage) {
                if (pageNo >= totalPage) {
                    document.getElementById('currentPage').value = totalPage;
                } else if (pageNo <= 1) {
                    document.getElementById('currentPage').value = 1;
                }
            }

        </script>
    </body>
</html>
