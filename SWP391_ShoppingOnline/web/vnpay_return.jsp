<%@page import="java.net.URLEncoder"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="controller.vnpay.Config"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>

<%@page import="dal.OrderDAO"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <title>KẾT QUẢ THANH TOÁN</title>
        <!-- Bootstrap core CSS -->
        <link href="./assets/bootstrap.min.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="./assets/jumbotron-narrow.css" rel="stylesheet"> 
        <script src="./assets/jquery-1.11.3.min.js"></script>
    </head>
    <body style="padding: 0">

        <jsp:include page="header.jsp"></jsp:include>

        <%
            //Begin process return from VNPAY
            Map fields = new HashMap();
            for (Enumeration params = request.getParameterNames(); params.hasMoreElements();) {
                String fieldName = URLEncoder.encode((String) params.nextElement(), StandardCharsets.US_ASCII.toString());
                String fieldValue = URLEncoder.encode(request.getParameter(fieldName), StandardCharsets.US_ASCII.toString());
                if ((fieldValue != null) && (fieldValue.length() > 0)) {
                    fields.put(fieldName, fieldValue);
                }
            }

            String vnp_SecureHash = request.getParameter("vnp_SecureHash");
            if (fields.containsKey("vnp_SecureHashType")) {
                fields.remove("vnp_SecureHashType");
            }
            if (fields.containsKey("vnp_SecureHash")) {
                fields.remove("vnp_SecureHash");
            }
            String signValue = Config.hashAllFields(fields);

            String code = request.getParameter("vnp_TxnRef");
        %>

        <!--Begin display -->
        <div class="container">
            <div class="header clearfix">
                <h3 class="text-muted">Purchase Result</h3>
            </div>
            <div class="table-responsive">
                <div class="form-group">
                    <label >Payment transaction code: </label>
                    <label id="code"><%=code%></label>
                </div>    
                <div class="form-group">
                    <label >Amount: </label>
                    <label id="amount"><%= formatNumberWithCommas(request.getParameter("vnp_Amount")) %></label>

                    <%!
                      public String formatNumberWithCommas(String number) {
                        try {
                            number = number.substring(0, number.length()-2);
                          long amount = Long.parseLong(number);
                          java.text.NumberFormat numberFormat = java.text.NumberFormat.getNumberInstance(java.util.Locale.US);
                          return numberFormat.format(amount);
                        } catch (NumberFormatException e) {
                          return number; // Return the original value if it's not a valid number
                        }
                      }
                    %>
                </div>  
                <div class="form-group">
                    <label >Transaction description: </label>
                    <label id="info"><%=request.getParameter("vnp_OrderInfo")%></label>
                </div> 
                <div class="form-group">
                    <label >Payment error code: </label>
                    <label><%=request.getParameter("vnp_ResponseCode")%></label>
                </div> 
                <div class="form-group">
                    <label >Transaction code at CTT VNPAY-QR: </label>
                    <label id="code2"><%=request.getParameter("vnp_TransactionNo")%></label>
                </div> 
                <div class="form-group">
                    <label >Payment bank code: </label>
                    <label id="bank"><%=request.getParameter("vnp_BankCode")%></label>
                </div> 
                <div class="form-group">
                    <label >Payment time: </label>
                    <label id="paydate"><%=request.getParameter("vnp_PayDate")%></label>
                </div> 
                <div class="form-group">
                    <label >Transaction status: </label>
                    <label id="billResult">
                        <%
                            if (signValue.equals(vnp_SecureHash)) {
                                if ("00".equals(request.getParameter("vnp_TransactionStatus"))) {
                                    out.print("Success");

                                    String ordId = (String) request.getSession(true).getAttribute(code);
                                    new OrderDAO().updateOrderSoID(ordId, 2);
                                    
                                    //send mail
                                    
                                } else {
                                    out.print("Failed");
                                }

                            } else {
                                out.print("invalid signature");
                            }
                        %>
                    </label>
                </div> 
            </div>       
        </div>  

        <br><br>

        <jsp:include page="footer.jsp"></jsp:include>


            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@emailjs/browser@3/dist/email.min.js"></script>
            <script type="text/javascript">
                (function () {
                    emailjs.init("8-sm4RCWIP_ZiQGV3");
                })();
            </script>
            <script>
                let code = document.getElementById('code').textContent;
                let amount = document.getElementById('amount').textContent;
                let info = document.getElementById('info').textContent;
                let bank = document.getElementById('bank').textContent;
                let paydate = document.getElementById('paydate').textContent;
                let billResult = document.getElementById('billResult').textContent;

                let content = 'Your bill info: \n';

                content += 'Merchant Transaction Code: ' + code + '\n';
                content += 'Amount: ' + amount + '\n';
                content += 'Order info: ' + info + '\n';
                content += 'Bank Code: ' + bank + '\n';
                content += 'Pay Date: ' + paydate + '\n';
                content += 'Payment Status: ' + billResult + '\n';


                let param = {
                    email: '${sessionScope.customer.cusEmail}',
                    username: '${sessionScope.customer.cusContactName}',
                    content: content,
                }

                if (billResult.includes('Success'))
//                    console.log(param)
                    emailjs.send('service_v4mrmpl', 'template_8xmohqn', param);
                else
                    console.log('send bill error!')
        </script>

    </body>
</html>
