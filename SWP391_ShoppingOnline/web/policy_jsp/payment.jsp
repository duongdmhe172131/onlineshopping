<%-- 
    Document   : payment
    Created on : Oct 16, 2023, 3:04:52 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
    </head>
    <body>

        <jsp:include page="../header.jsp"></jsp:include>

            <div style="margin: 5px 150px 20px">
                <h1>Payment methods</h1>
                <h4 style="text-align: center; display: block; font-size: 30px">Instructions for safe payment when shopping at iLocalShop</h4><br/>
                <h4 style="text-decoration: underline">Method 1: Direct bank transfer payment to iLocalShop’s account</h4>
                <h4 style="font-weight: 100">
                    If you live far away or you don't have time to go to the store, you can choose to pay by bank transfer to Hoang Kien's account. 
                    (Contact us for transfer information).
                </h4>
                <h4 style="font-weight: 100">
                    After you transfer, please contact us to notify us of shipping information. 
                    After receiving the money, Hoang Kien staff will directly deliver the goods 
                    if your address is in the inner city districts of Hanoi or the delivery staff will deliver the goods to your registered 
                    address within 2 - 4 working days.
                </h4><br/>
                <h4 style="text-decoration: underline">Method 2: Pay directly upon receipt</h4>
                <h4 style="font-weight: 100">
                    This is a form of payment COD (Cash on Delivery). You will pay directly to the courier after receiving the goods. 
                    With this form, to ensure the order is confirmed, please transfer 500,000 VND in advance to Hoang Kien to confirm the purchase. 
                    Hoang Kien will send the goods the same day after receiving advance payment. When you receive the goods, you pay the remaining amount.
                </h4><br/>
            </div>

        <jsp:include page="../footer.jsp"></jsp:include>

    </body>
</html>
