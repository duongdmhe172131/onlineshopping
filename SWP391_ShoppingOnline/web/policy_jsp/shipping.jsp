<%-- 
    Document   : shipping
    Created on : Oct 16, 2023, 3:44:39 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Shipping | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">

    </head>
    <body>

        <jsp:include page="../header.jsp"></jsp:include>

            <div style="margin: 5px 150px 20px">
                <h1>Shipping methods</h1>
                <h4 style="text-align: center; display: block; font-size: 30px">Shipping methods</h4><br/>
                <h4 style="text-decoration: underline">Method 1: Free delivery within Hanoi</h4>
                <h4 style="font-weight: 100">
                    If you don't want to go directly to the store.
                    iLocalShop is ready to deliver free of charge within urban districts of Hanoi (except for accessory products). 
                    Please call the hotline to order and receive the product at your desired address.
                </h4><br/>
                <h4 style="text-decoration: underline">Method 2: Use Cash on Delivery (COD)</h4>
                <h4 style="font-weight: 100">
                    For those of you who are far from Hanoi and want to use COD (payment on delivery), please contact iLocalShop for instructions. 
                    This is a form of payment COD (Cash on Delivery) - You will pay directly to the courier after receiving the goods. 
                    With this form, to ensure the order is confirmed, please transfer 500,000VND in advance to iLocalShop to confirm the purchase. 
                    iLocalShop will send the goods the same day after receiving advance payment. When you receive the goods, you pay the remaining amount.
                </h4><br/>
            </div>

        <jsp:include page="../footer.jsp"></jsp:include>

    </body>
</html>
