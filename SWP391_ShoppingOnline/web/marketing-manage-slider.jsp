<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Manage Slider | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <!-- Include Bootstrap CSS via CDN link -->
        <!-- ======= Styles ====== -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin_manager.css">
           

    </head>

    <body>
        <div class="container-fluid">
            <!-- Navigation -->
            <jsp:include page="admin-navigation.jsp"></jsp:include>

                <!-- Main Content -->
                <div class="main" style="margin-left: 50px; margin-right: 50px;">
                    <div class="topbar">
                        <!-- <div class="toggle">
            <ion-icon name="menu-outline"></ion-icon>
        </div>
        <div class="user">
            <img src="assets/imgs/customer01.jpg" alt="">
        </div> -->
                    </div>

                    <div class="row" style="margin-right: 70px;  padding: 10px; border: 1.5px solid #000;">
                        <input type="hidden" id="pageNo" name="pageNo" value="${currentPage}">
                    <div class="col-12" style="margin-bottom: 40px;">
                        <h1>Slider</h1>
                    </div>

                    <div class="col-4">
                        <div class="text-left">
                            <a class="btn btn-success mt-5" style="color: white" href="add-slider">Add new</a>
                        </div>
                    </div>
                    <div class="col-12" style="margin-top: 10px;">
                        <div class="table-responsive">
                            <table class="table table-striped">

                                <thead>
                                    <tr>
                                        <th scope="col">Slider ID</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Slider Title</th>
                                        <th scope="col">Slider Note</th>
                                        <th scope="col">Slider Order</th>
                                        <th scope="col">Slider Status</th>
                                        <th scope="col">Details</th>
                                    </tr>
                                </thead>
                                <tbody>     

                                    <c:forEach items="${sliderList}" var="slider">
                                        <tr>
                                            <td>${slider.sliderID}</td>
                                            <td><img src="${pageContext.request.contextPath}/img/${slider.sliderImage}" alt="img" style="width: 80px; height: 80px;" /></td>
                                            <td>${slider.sliderTitle}</td>
                                            <td>${slider.sliderNote}</td> 
                                            <td>
                                                <c:if test="${slider.sliderStatus > 0}">
                                                    <input class="form-control w-50" type="number" min="1" onchange="updateStatus(this)"
                                                           value="${slider.sliderStatus}" data-id="${slider.sliderID}">
                                                </c:if>
                                            </td>  
                                            <td>
                                                <c:choose>
                                                    <c:when test="${slider.sliderStatus > 0}">
                                                        <a href="updateStatus-slider?status=0&sliderId=${slider.sliderID}">
                                                            <button type="button" class="btn btn-danger">
                                                                Hide
                                                            </button>
                                                        </a>
                                                    </c:when>
                                                    <c:when test="${slider.sliderStatus == 0}">
                                                        <a href="updateStatus-slider?status=1&sliderId=${slider.sliderID}">
                                                            <button type="button" class="btn btn-success">
                                                                View
                                                            </button>
                                                        </a>
                                                    </c:when>
                                                </c:choose>
                                            </td>
                                            <td>
                                                <a href="./sliderDetails?sliderId=${slider.sliderID}" class="btn btn-danger">Edit</a>
                                            </td>

                                        </tr>
                                    </c:forEach>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </body>

    <script>

        function updateStatus(slider) {

            window.location.href = './updateStatus-slider?status=' + slider.value + '&sliderId=' + slider.dataset.id;

        }

    </script>

    <!-- =========== Scripts =========  -->
    <script src="js/admin_manager.js"></script>
    <!-- ====== ionicons ======= -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>


</html>