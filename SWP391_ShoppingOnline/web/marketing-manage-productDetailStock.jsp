<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Manage Product | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <!-- Include Bootstrap CSS via CDN link -->
        <!-- ======= Styles ====== -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin_manager.css">
    </head>

    <body>
        <div class="container-fluid">
            <!-- Navigation -->
            <jsp:include page="admin-navigation.jsp"></jsp:include>

                <!-- Main Content -->
                <div class="main" style="margin-left: 50px; margin-right: 50px;">
                    <div class="topbar">
                        <!-- <div class="toggle">
            <ion-icon name="menu-outline"></ion-icon>
        </div>
        <div class="user">
            <img src="assets/imgs/customer01.jpg" alt="">
        </div> -->
                    </div>

                    <div class="row" style="margin-right: 70px;  padding: 10px; border: 1.5px solid #000;">
                        <input type="hidden" id="pageNo" name="pageNo" value="${currentPage}">
                    <div class="col-12" style="margin-bottom: 40px;">
                        <h1>Stock</h1>
                        <c:if test="${param.exist != null}">
                            <h6 style="color: red;">Account alrealdy exist!</h6>
                        </c:if>
                        <c:if test="${param.error != null}">
                            <h6 style="color: red;">In-valid information to add new customer!</h6>
                        </c:if>
                    </div>
                    <div class="col-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search color" value="${search}"
                                   id="search"  name="search">
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="button" id="btnSearch">
                                    <ion-icon name="search-outline"></ion-icon>
                                </button>
                            </div>
                        </div>
                    </div>


                    <div class="col-3">
                        <select class="form-control" id="status" name="status">
                            <option value="-1" ${status==null ? 'selected' : '' }>All status</option>
                            <option value="1" ${status==1 ? 'selected' : '' }>View</option>
                            <option value="0" ${status==0 ? 'selected' : '' }>Hide</option>
                        </select>
                    </div>


                    <script>
                        // handle filter search
                        const searchInput = document.querySelector('#search');
                        searchInput.addEventListener('keydown', (event) => {
                            if (event.key === 'Enter') {
                                performSearch();
                            }
                        });
                        const btnSearch = document.querySelector('#btnSearch');
                        btnSearch.addEventListener('click', () => {
                            performSearch(); // call function
                        });
                        function performSearch() {
                            const search = document.querySelector('#search').value;
                            const status = document.querySelector('#status').value;
                            const pageNo = document.querySelector('#pageNo').value;
                            window.location.href = 'manage-productDetailStock?search=' + search +
                                    '&status=' + status + '&pageNo=1' + '&proId=' + ${proId};
                        }
                        ;

                        // handle filter status
                        const status = document.querySelector('#status');
                        status.addEventListener('change', () => {
                            const search = document.querySelector('#search').value;
                            const status = document.querySelector('#status').value;
                            const pageNo = document.querySelector('#pageNo').value;

                            window.location.href = 'manage-productDetailStock?search=' + search +
                                    '&status=' + status + '&pageNo=1' + '&proId=' + ${proId};
                        });



                        // handle pagination
                        function changePage(pageNo) {
                            const search = document.querySelector('#search').value;
                            const status = document.querySelector('#status').value;

                            window.location.href = 'manage-productDetailStock?search=' + search +
                                    '&status=' + status + '&pageNo=' + pageNo + '&proId=' + ${proId};
                        }

                    </script>    




                    <div class="col-3"></div>
                    <div class="col-3">
                        <div class="text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#addnewModal">
                                <a href="addProductDetail?proId=${proId}" style="color: white;"><ion-icon style="margin-top: 2px;" name="add-outline"></ion-icon> Add New</a>
                            </button>
                        </div>
                    </div>
                    <div class="col-12" style="margin-top: 10px;">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Product Image</th>
                                        <th scope="col">Product Name</th>
                                        <th scope="col">Color</th>
                                        <th scope="col">Product Sold</th>
                                        <th scope="col">Product Available</th>
                                        <th scope="col">Product Detail</th>
                                        <th scope="col">Action</th>

                                    </tr>
                                </thead>
                                <tbody>

                                <input type="text" value="${product.proImage}" name="proImage" hidden>

                                <c:forEach items="${listPdStock}" var="listPdStock">
                                    <tr>

                                        <td><img src="${pageContext.request.contextPath}/pdimg/${listPdStock.pdImage}" style="width: 75px; display: table; margin: 0px -10px;" alt=""></td>
                                        <td>${listPdStock.proName}</td>
                                        <td>${listPdStock.pdColor}</td>    
                                        <td>${listPdStock.pdSold}</td>    
                                        <td>${listPdStock.pdAvailable}</td>                                      
                                        <td><a href="edit-productStock?proId=${listPdStock.proID}&pdId=${listPdStock.pdId}">Details</a></td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${listPdStock.pdStatus == 1}">
                                                    <a href="updateStatusProductDetail?status=0&proId=${listPdStock.proID}&pdId=${listPdStock.pdId}">
                                                        <button type="button" class="btn btn-danger">
                                                            Hide
                                                        </button>
                                                    </a>
                                                </c:when>
                                                <c:when test="${listPdStock.pdStatus == 0}">
                                                    <a href="updateStatusProductDetail?status=1&proId=${listPdStock.proID}&pdId=${listPdStock.pdId}">
                                                        <button type="button" class="btn btn-success">
                                                            View
                                                        </button>
                                                    </a>
                                                </c:when>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>



                        <input type="hidden" id="pageNo" name="pageNo" value="${currentPage}">
                        <input type="hidden" id="totalPage" name="pageNo" value="${totalPage}">
                        <div class="pagination" style="text-align: center">
                            Page
                            <input type="number" value="${currentPage}" min="1" max="${totalPage}" id="currentPage"/>
                            /${totalPage}</div>



                    </div>
                </div>



            </div>
        </div>
    </body>


    <!-- =========== Scripts =========  -->
    <script src="js/admin_manager.js"></script>
    <!-- ====== ionicons ======= -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
    <script>
                        var alertMessage = ${sessionScope.alertMessage};

                        if (alertMessage) {
                            alert("Update product detail successfully!");
                        }

        <%
                session.removeAttribute("alertMessage");
        %>
    </script>
    <script>
        // handle pagination
        const page = document.querySelector('#currentPage');

        page.addEventListener('change', () => {
            const currentPageRaw = document.getElementById('currentPage').value;
            const totalPage = document.getElementById('totalPage').value;
            validatePage(currentPageRaw, totalPage);
            const currentPage = document.getElementById('currentPage').value;
            changePage(currentPage);
        });

        function validatePage(pageNo, totalPage) {
            if (pageNo >= totalPage) {
                console.log("sakdfjklasd");
                document.getElementById('currentPage').value = totalPage;
            } else if (pageNo <= 1) {
                document.getElementById('currentPage').value = 1;
            }
        }
    </script>
</html>