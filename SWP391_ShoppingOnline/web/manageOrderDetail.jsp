<%-- 
    Document   : manageOrderDetail
    Created on : Oct 28, 2023, 2:42:40 PM
    Author     : windy
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            * {
                padding: 0;
                margin: 0;
                box-sizing: border-box;
            }
            body {
                background-color: #172856;
            }
            .container {
                background-color: #172856;
                padding-top: 20px;
                height: 100%;
                padding-bottom: 20px;
                display: flex;
                justify-content: space-between;
            }
            .contact-information {
                width: 20%;
                background-color: #fffcf5;
                margin-top: 20px;
                padding-left: 20px;
                padding-right: 20px;
                padding-top: 20px;
                line-height: 25px;
                margin-left: 20px;
                height: 230px;
                border: #999 dashed thin;
                border-radius: 5px;
            }
            .contact-information label {
                font-weight: bold;
            }
            .order {
                width: 75%;
                float: right;
                margin-right: 20px;
                margin-top: 20px;
                padding-bottom: 20px;
            }
            .order-information {
                border: #999 dashed thin;
                border-radius: 5px;
                background-color: #fffcf5;
                padding-left: 20px;
                padding-right: 20px;
                padding-top: 20px;
                width: 100%;
                padding-bottom: 20px;
                line-height: 25px;
            }
            .order-information label {
                font-weight: bold;
            }
            .order-detail{
                background-color: #fffcf5;
                border-radius: 5px;
                width: 100%;
                margin-top: 20px;
            }
            .order-detail table {
                width: 100%;
                margin-left: 20px;
                padding-top: 20px;
                line-height: 25px;
                padding-bottom: 20px;
            }
            .order-detail img {
                width: 50px;
                height: 50px;
            }
            .return {
                padding-top: 20px;
                margin-left: 20px;
            }
            .return a {
                color: white;
                text-decoration: none;
                font-size: 25px;
            }
        </style>
    </head>
    <body>
        <div class="return">
            <a href="./manageOrder">&#60; Back to list</a>
        </div>

        <div class="container">
            <c:set value="${order}" var="o"></c:set>
                <div class="contact-information">
                    <!--                <table>
                                        <tr>
                                            <td>Contact Name:  </td>
                                            <td>${o.orderContactName}</td>
                                        </tr>
                                    </table>-->
                <label>Contact Name: </label> <span>${o.orderContactName}</span><br>
                <label>Phone: </label> <span>${o.orderPhone}</span><br>
                <label>Address: </label> <span>${o.orderAddress}</span>
            </div>
            <div class="order">
                <div class="order-information">
                    <label>Order Date: </label> <span>${o.orderDate}</span><br>
                    <c:forEach items="${listStatusOrder}" var="statusOrder">
                        <c:if test="${o.orderSoID == statusOrder.soID}">
                            <label>OrderStatus: </label> <span>${statusOrder.soName}</span><br>
                        </c:if>
                    </c:forEach>
                    <label>Change Status: </label>
                    <form action="manageOrderDetail" method="post" id="form">
                        <input type="hidden" name="orderID" value="${orderID}"/>
                        <select class="form-control" id="statusOrder" name="status" onchange="notice(${o.orderSoID})">
                            <c:forEach items="${listStatusOrder}" var="s">
                                <option value="${s.soID}" ${s.soID==o.orderSoID ? 'selected' : '' }>${s.soName}</option>
                            </c:forEach>
                        </select>
                    </form>
                    <label>Total: </label> <span style="color: red"><fmt:formatNumber value="${o.orderTotalPrice}" type="number" pattern="#,##0" /></span><br>
                </div>
                <div class="order-detail">
                    <table>
                        <c:forEach items="${listItem}" var="i">
                            <tr>
                                <td><img src="../pdimg/${i.pdimgURL}"></td>
                                <td>${i.proName}</td>
                                <td>${i.pdColor}</td>
                                <td style="padding-right: 20px">${i.quantity}</td>
                                <td><fmt:formatNumber value="${i.proPrice}" type="number" pattern="#,##0" /></td>
                            </tr>
                        </c:forEach>
                    </table>


                </div>
            </div>
        </div>
        <script>
            function notice(preStatus) {
                let notice = confirm("You want to change status of this order?");
                if (notice) {
                    document.getElementById("form").submit();
                } else {
                    document.getElementById("statusOrder").value = preStatus;
                }
            }
        </script>
    </body>
</html>
