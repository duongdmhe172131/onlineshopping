/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
const listPro = document.getElementsByClassName('item');
//console.log(typeof (listPro));
//console.log(listPro);
let arr = Array.from(listPro);
//console.log(arr);

//console.log(arr[0]);
const itemsPerPage = 4;
const paginationContainer = "#pagination";
const temp = "#items";

pagination(arr, itemsPerPage, paginationContainer);
//setupPagination();
//showItems(3);

function pagination(arr, itemsPerPage, paginationContainer) {
    let currentPage = 1;
    const totalPages = Math.ceil(arr.length / itemsPerPage);

    function setupPagination() {
        const pagination = document.querySelector(paginationContainer);
        pagination.innerHTML = "";

        for (let i = 1; i <= totalPages; i++) {
            const link = document.createElement("a");
            link.href = "#";
            link.innerText = i;
            
            link.addEventListener("click", function(event){
                event.preventDefault();
                currentPage = i;
                showItems(currentPage);
            });
            pagination.appendChild(link);
        }
    }

    function showItems(currentPage) {
        const startIndex = (currentPage - 1) * itemsPerPage;
        const endIndex = startIndex + itemsPerPage;
        const pageItems = arr.slice(startIndex, endIndex);

        const itemsContainer = document.querySelector(temp);
        itemsContainer.innerHTML = "";

        for (let i = 0; i < pageItems.length; i++) {
            itemsContainer.appendChild(pageItems[i]);
        }
    }
    
    showItems(currentPage);
    setupPagination();
}

