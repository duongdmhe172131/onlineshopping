<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${product.proName} | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <!-- Link Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
        <!-- Link File CSS -->
        <link rel="stylesheet" href="css/styles.css">
        <!-- Add your custom CSS styles here -->
        <link rel="stylesheet" href="css/product-detail.css">
        <style>
            *{
                margin: 0;
                padding: 0;
            }

            .rate {
                float: left;
                height: 46px;
                padding: 0 10px;
            }
            .rate:not(:checked) > input {
                position:absolute;
                top:-9999px;
            }
            .rate:not(:checked) > label {
                float:right;
                width:1em;
                overflow:hidden;
                white-space:nowrap;
                cursor:pointer;
                font-size:30px;
                color:#ccc;
            }
            .rate:not(:checked) > label:before {
                content: '★ ';
            }
            .rate > input:checked ~ label {
                color: #ffc700;
            }
            .rate:not(:checked) > label:hover,
            .rate:not(:checked) > label:hover ~ label {
                color: #deb217;
            }
            .rate > input:checked + label:hover,
            .rate > input:checked + label:hover ~ label,
            .rate > input:checked ~ label:hover,
            .rate > input:checked ~ label:hover ~ label,
            .rate > label:hover ~ input:checked ~ label {
                color: #c59b08;
            }
            
        </style>
    </head>
    <body>

        <jsp:include page="header.jsp"></jsp:include>

            <div class="product-detail-container">
                <div class="product-image">
                    <img id="pdImage" src="img/${product.proImage}" alt="${product.proName}">
            </div>
            <div class="product-details">
                <h1>${product.proName}</h1>
                <input type="hidden" id="proID" value="${product.proID}"/>
                <p class="product-price">Price: <span><fmt:formatNumber value="${product.proPrice}" type="number" pattern="#,##0" /> VND</span></p>
                <br>
                <h2>Colors:</h2>
                <form id="color-form" action="/cart" method="post">
                    <select name="color" id="color-select" onchange="changeImage(this.value)">
                        <c:forEach items="${listProductDetail}" var="pd">
                            <option value="${pd.pdID}@${pd.pdImage}">${pd.pdColor}</option>
                        </c:forEach>
                    </select>
                </form>
                <div class="product-description">
                    <h2>Description:</h2>
                    <p>${product.proDetail}</p>
                </div>
                <br>
                <c:choose>
                    <c:when test="${empty sessionScope['customer']}">
                        <a href="${pageContext.request.contextPath}/login" class="add-to-cart-button">Add to Cart</a>
<!--                        <a href="${pageContext.request.contextPath}/login" class="add-to-cart-button">Add Feedback</a>-->
                    </c:when>
                    <c:otherwise>
                        <button id="add-to-cart-button" onclick="addToCart()" class="add-to-cart-button">Add to Cart</button>
<!--                        <a href="SendFeedback?proID=${product.proID}" class="add-to-cart-button">Add Feedback</a>-->
                    </c:otherwise> 
                </c:choose>
            </div>
        </div>

        <div class="product-image-gallery">
            <h2>Product Gallery:</h2>
            <div class="image-gallery">
                <c:forEach items="${listImg}" var="img">
                    <img src="img/${img.imageURL}" alt="${img.imageName}">
                </c:forEach>
            </div>
        </div>

        <script>
            function changeImage(img) {
                document.getElementById('pdImage').src = 'pdimg/' + img.split('@')[1];
            }
        </script>

        <div class="product-image-gallery">

            <c:if test="${listFb.size()>0}">
                <h2>Product Feedbacks:</h2>
            </c:if>

            <div class="image-gallery">

                <c:forEach items="${listFb}" var="fb">
                    <div style="display: block !important; width: 100% !important;padding: 20px;box-shadow: 0 0 5px 5px lightgray; border-radius: 10px; margin: 15px;">
                        <h4 style="text-align: left;">Username: ${fb.customer.cusContactName}</h4>
                        <p style="text-align: left;">Feedback time: ${fb.fbDate}</p>
                        <p style="text-align: left;">Content: ${fb.fbContent}</p>
                        <div class="rate">
                            <input type="radio" id="star5" name="rate${fb.fbId}" value="5" ${fb.fbStar==5?"checked":""}/>
                            <label for="star5" title="text">5 stars</label>
                            <input type="radio" id="star4" name="rate${fb.fbId}" value="4" ${fb.fbStar==1?"checked":""}/>
                            <label for="star4" title="text">4 stars</label>
                            <input type="radio" id="star3" name="rate${fb.fbId}" value="3" ${fb.fbStar==3?"checked":""}/>
                            <label for="star3" title="text">3 stars</label>
                            <input type="radio" id="star2" name="rate${fb.fbId}" value="2" ${fb.fbStar==2?"checked":""} />
                            <label for="star2" title="text">2 stars</label>
                            <input type="radio" id="star1" name="rate${fb.fbId}" value="1" ${fb.fbStar==1?"checked":""} />
                            <label for="star1" title="text">1 star</label>
                        </div>
                        <img src="${fb.fbImage}" onclick="changeSize${fb.fbId}()" id="feedbackImg${fb.fbId}" width="150px"  >
                    </div>
                    <script>
                        function changeSize${fb.fbId}() {
                            var curSize = document.getElementById("feedbackImg${fb.fbId}").width;
                            console.log(curSize)
                            if (curSize == 148) {
                                document.getElementById("feedbackImg${fb.fbId}").style.width = "600px";
                            } else {
                                document.getElementById("feedbackImg${fb.fbId}").style.width = "200px";


                            }
                        }
                    </script>
                </c:forEach>

            </div>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>
        </body>

       
        
        <script>
            function addToCart() {
                var pdID = document.getElementById("color-select").value.split('@')[0];
                var proID = document.getElementById("proID").value;
                //var url = "/cart?pdID=" + pdID;
                window.location.href = '${pageContext.request.contextPath}/cart?pdID=' + pdID + '&proID=' + proID;
            }
    </script>
    



</html>
