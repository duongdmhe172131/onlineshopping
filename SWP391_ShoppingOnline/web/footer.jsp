<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Home | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <!-- Link Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

        <!-- Link File CSS -->
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <div class="footer">
            <div class="contanier">
                <div class="row">
                    <h3>ilocal<span>shop</span></h3>
                    <p>Business Registration Certificate No. 0106786986 issued by Hanoi Department of Planning and Investment on March 11, 2015.</p>
                    <div class="social-links">
                        <a href="#"><i class="fa-brands fa-facebook-f"></i></a>
                        <a href="#"><i class="fa-brands fa-instagram"></i></a>
                    </div>
                </div>
                <div class="row">
                    <h3>policy</h3>
                    <ul>
                        <li><a href="${pageContext.request.contextPath}/payment_policy">Payment policy</a></li>
                        <li><a href="${pageContext.request.contextPath}/shipping_policy">Shipping policy</a></li>
                      
                    </ul>
                </div>

                <div class="row">
                    <h3>contact</h3>
                    <ul>
                        <li>15A Hue Street, Hoan Kiem, Hanoi, Viet Nam</li>
                        <li>Hotline: 0123456789</li>
                        <li>iLocalShop@gmail.com</li>
                    </ul>
                </div>

                <div class="row">
                    <h3>Payment merhoods</h3>
                    <p>Variety of different payment methods</p>
                    <div class="payment-img">
                        <img src="img/p1.jpg" alt="">
                        <img src="img/p2.jpg" alt="">
                        <img src="img/p3.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="copy-right">
                <p>&copy; Copyright belongs to iLocal Shop | Designed by <span>SWP391G6</span></p>
            </div>
        </div>
    </body>
</html>
