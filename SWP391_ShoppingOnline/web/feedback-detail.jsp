<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Manage Feedback | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <!-- Include Bootstrap CSS via CDN link -->
        <!-- ======= Styles ====== -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin_manager.css">
    </head>

    <body>
        <div class="container-fluid">
            <!-- Navigation -->
            <jsp:include page="admin-navigation.jsp"></jsp:include>

                <!-- Main Content -->
                <div class="main" style="margin-left: 50px; margin-right: 50px;">
                    <div class="topbar">
                        <!-- <div class="toggle">
            <ion-icon name="menu-outline"></ion-icon>
        </div>
        <div class="user">
            <img src="assets/imgs/customer01.jpg" alt="">
        </div> -->
                    </div>

                    <div class="row" style="margin-right: 70px;  padding: 10px; border: 1.5px solid #000;">
                        <div>
                            <h1>Feedback Detail</h1>

                            <form method="post" action="FeedbackDetail">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 style="text-align: center;">Customer Infor</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Contact Name: </label>
                                        <input readonly type="text" value="${f.customer.cusContactName}" name="" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>User Name: </label>
                                    <input readonly type="text" value="${f.customer.cusUsername}" name="" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Email: </label>
                                    <input readonly type="text" value="${f.customer.cusEmail}" name="" class="form-control">
                                </div>

                                <div class="col-md-6">
                                    <label> Phone: </label>
                                    <input readonly type="text" value="${f.customer.cusPhone}" name="" class="form-control">
                                </div>
                                <div class="col-md-12">
                                    <h3 style="text-align: center;">Feedback Infor</h3>
                                </div>
                                <div class="col-md-6">
                                    <label>Id: </label>
                                    <input readonly type="text" value="${f.fbId}" name="fid" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Product Name: </label>
                                    <input readonly type="text" value="${f.product.proName}" name="" class="form-control">
                                </div>

                                <div class="col-md-6">
                                    <label>Star: </label>
                                    <input readonly type="text" value="${f.fbStar}&#11088;" name="" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Create Date: </label>
                                    <input readonly type="text" value="${f.fbDate}" name="" class="form-control">
                                </div>
                                <div class="col-md-12">
                                    <label>Content: </label>
                                    <textarea readonly type="text" rows="3"  name="" class="form-control">${f.getFbContent()} 
                                   </textarea>
                                </div>
                                <div class="col-md-12">
                                    <label>Image: </label>
                                    <img src="${f.fbImage}" width="40%" alt="alt" >
                                </div>
                                <div class="col-md-6">
                                    <label>Status: </label>
                                    <input  type="radio" value="1" name="ss" ${f.fbStatus == 1? "checked":""} >Active
                                    <input  type="radio" value="0" name="ss"${f.fbStatus == 1? "":"checked"}>Inactive
                                </div>
                                <div class="col-md-12">
                                    <button style="margin: 10px" class="btn btn-primary" type="submit" >Update</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <!-- =========== Scripts =========  -->
     <script src="js/admin_manager.js"></script>
    <!-- ====== ionicons ======= -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
    <script src="js/admin_manager.js"></script>
    <!-- ====== ionicons ======= -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#tablepro").DataTable({bFilter: false, bInfo: false, paging: true, lengthChange: false});
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>

</html>