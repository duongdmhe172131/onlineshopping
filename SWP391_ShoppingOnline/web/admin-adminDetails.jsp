<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Manage Admin | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <!-- Include Bootstrap CSS via CDN link -->
        <!-- ======= Styles ====== -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin_manager.css">
    </head>

    <body>
        <div class="container-fluid">
            <!-- Navigation -->
            <jsp:include page="admin-navigation.jsp"></jsp:include>

                <!-- Main Content -->
                <div class="main" style="margin-left: 50px; margin-right: 50px;">
                    <div class="topbar">
                        <!-- <div class="toggle">
        <ion-icon name="menu-outline"></ion-icon>
    </div>
    <div class="user">
        <img src="assets/imgs/customer01.jpg" alt="">
    </div> -->
                    </div>

                    <div class="row" style="margin-right: 70px;  padding: 10px; border: 1.5px solid #000;">
                        <input type="hidden" id="pageNo" name="pageNo" value="${currentPage}">
                    <div class="col-12" style="margin-bottom: 40px;">
                        <h1>Account details</h1>
                    </div>
                    <form action="admin-details" method="post">
                        <input type="text" value="${data.adId}" name="id" hidden>
                        <div class="form-group row">
                            <div class="col-4">
                                <label for="firstName">First Name:</label>
                                <input type="text" class="form-control" id="firstName" name="firstName"
                                       value="${data.adFirstName}" required>
                            </div>
                            <div class="col-4">
                                <label for="lastName">Last Name:</label>
                                <input type="text" class="form-control" id="lastName" name="lastName"
                                       value="${data.adLastName}" required>
                            </div>
                            <div class="col-4">
                                <label for="email">Date Of Birth:</label>
                                <input type="date" class="form-control" id="dob" name="dob"
                                       value="${data.adDob}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-4">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       value="${data.adEmail}" required>
                            </div>
                            <div class="col-4">
                                <label for="phone">Phone Number:</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       value="${data.adPhone}">
                            </div>
                            <div class="col-4">
                                <label for="phone">Adress:</label>
                                <input type="text" class="form-control" id="address" name="address"
                                       value="${data.adAddress}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-4">
                                <label for="username">Username:</label>
                                <input type="text" class="form-control" id="username" name="username"
                                       value="${data.adUsername}" readonly>
                            </div>

                            <div class="col-2">
                                <label for="roleId">Role:</label>
                                <select class="form-control" id="roleId" name="roleId">
                                    <c:forEach items="${listRole}" var="role">
                                        <option value="${role.roleID}" ${role.roleID==data.adRoleId ? 'selected'
                                                         : '' }>
                                                    ${role.roleName}
                                                </option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <label for="roleId">Status:</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="1" ${status==1 ? 'selected' : '' }>Active</option>
                                        <option value="0" ${status==0 ? 'selected' : '' }>In-Active</option>
                                    </select>
                                </div>
                            </div>
                          <a class="btn btn-primary" href="./manage">Cancel</a>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>

                </div>
            </div>
        </body>


        <!-- =========== Scripts =========  -->
        <script src="js/admin_manager.js"></script>
        <!-- ====== ionicons ======= -->
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

    </html>