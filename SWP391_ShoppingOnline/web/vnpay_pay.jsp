<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Tạo mới đơn hàng</title>
        <!-- Bootstrap core CSS -->
        <link href="./assets/bootstrap.min.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="./assets/jumbotron-narrow.css" rel="stylesheet">      
        <script src="./assets/jquery-1.11.3.min.js"></script>
        
        <style>
            h2{
                font-family: 'Roboto',sans-serif;
                font-size: 26px;
                margin-top: 20px;
                margin-bottom: 10px;
                font-weight: 500;
                line-height: 1.1;
                color: inherit;
            }
            h4{
                font-family: 'Roboto',sans-serif;
                margin-top: 20px;
                margin-bottom: 10px;
                line-height: 1.1;
                color: #333;
                font-weight: lighter;
            }
            table{
                width: 100%;
                background-color: transparent;
                border-spacing: 0;
                border-collapse: collapse;
                /*border-color: gray;*/
                text-indent: initial;
                display: table;
                border: 2px gray solid;
            }
            *{
                /*                margin: 10px;
                                padding: 110px;*/
            }
            th, td{
                border: 2px gray solid;
            }
            .btn{
                /*float: right;*/

                margin: 10px 0px;
                background: #e24d85;
                color: #fff;
                padding: 10px 15px;
                font-weight: bold;
                display: inline-block;
                font-size: 14px;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }
            .info{
                /*width: 80%;*/
            }
            td{
                padding-left: 10px;
                padding-right: 10px;
                text-align: center;
            }
        </style>
    </head>

    <body>

        <jsp:include page="header.jsp"></jsp:include>

            <div class="container">

                <div class="header clearfix">
                    <h3 class="text-muted">Purchase Order: </h3>
                </div> 

                <div class="table-responsive">
                    <form action="vnpayajax" id="frmCreateOrder" method="post">  

                        <div class="form-group">
                            <label >Order : ${ordId}</label>
                    </div>

                    <div class="form-group">
                        <label >Amount : ${amount}</label>
                    </div>

                    <div class="form-group">
                        <label >Contact name : ${order.orderContactName}</label>
                    </div>

                    <div class="form-group">
                        <label >Phone : ${order.orderPhone}</label>
                    </div>

                    <div class="form-group">
                        <label >Address : ${order.orderAddress}</label>
                    </div>

                    <div class="form-group">
                        <label >Note : ${order.orderNote}</label>
                    </div>

                    <input type="hidden" name="ordId" value="${ordId}">

                    <div class="form-group" style="width: 25%;">
                        <input class="form-control" data-val="true" data-val-number="The field Amount must be a number." data-val-required="The Amount field is required." id="amount" name="amount" type="hidden" value="${price}" readonly/>
                    </div>

                    <div class="form-group">
                        <input type="hidden" Checked="True" id="bankCode" name="bankCode" value="">
                    </div>

                    <div class="form-group">
                        <input type="hidden" id="language" name="language" value="en">
                    </div>

                    <button type="submit" class="btn btn-default" href>Confirm</button>

                </form>
            </div>
        </div>

        <div class="row" style="margin-left: 110px; margin-right: 100px; margin-bottom: 50px">
                
                <table>
                    <thead>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    </thead>
                    <tbody><tr>
                        <c:forEach items="${productOfOrderList}" var="o">
                            <%--<c:forEach items="${orderDetailList}" var="od">--%>
                            <tr>
                                <td class="product-image" style="text-align: center; width: 10%; padding: 5px"><img src="pdimg/${o.pdImage}" alt="alt" style="height: 30vh"/></td>
                                <td style="text-align: center">${o.proName}</td>
                                <td style="">${o.pdColor}</td>
                                <td><span><fmt:formatNumber value="${o.proPrice}" type="number" pattern="#,##0" /> VND</span></td>
                                <td style="text-align: center">${o.odQuantity}</td>
                            </tr>
                            <%--</c:forEach>--%>
                        </c:forEach>
                    </tbody>
                </table>
                
                
            </div>

<br><br>

<jsp:include page="footer.jsp"></jsp:include>

<link href="https://pay.vnpay.vn/lib/vnpay/vnpay.css" rel="stylesheet" />
<script src="https://pay.vnpay.vn/lib/vnpay/vnpay.min.js"></script>
<script type="text/javascript">
    $("#frmCreateOrder").submit(function () {
        var postData = $("#frmCreateOrder").serialize();
        var submitUrl = $("#frmCreateOrder").attr("action");
        $.ajax({
            type: "POST",
            url: submitUrl,
            data: postData,
            dataType: 'JSON',
            success: function (x) {
                if (x.code === '00') {
                    if (window.vnpay) {
                        vnpay.open({width: 768, height: 600, url: x.data});
                    } else {
                        location.href = x.data;
                    }
                    return false;
                } else {
                    alert(x.Message);
                }
            }
        });
        return false;
    });
</script>       
</body>
</html>
