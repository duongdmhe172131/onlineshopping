<%-- 
    Document   : changePassword
    Created on : Oct 12, 2023, 4:46:09 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Change Password | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
    </head>
    <style>
        input{
            margin: 0;
            padding: 5px;
            margin-bottom: 10px;
            width: 99%
        }

    </style>
    <body>
        <jsp:include page="../header.jsp"></jsp:include>

            <div style="border:solid 1px #dedede; padding:30px; padding-bottom: 80px;width: 50%;margin: auto">
                <form action="changePassword" method="post">
                    <h4 style="color: red; text-align: center">${requestScope.msg}</h4><br/>
                <h4 style="color: green; text-align: center">${requestScope.success}</h4><br/>
                <strong>Current password(*)</strong><br/>
                <input type="password" name="curPassword" required><br/>
                <strong>New password(*)</strong><br/>
                <input type="password" name="newPassword" required><br/>
                <strong>Re-enter new password(*)</strong><br/>
                <input type="password" name="reEnterPassword" required><br/>
                <% Boolean showCaptcha = (Boolean) request.getAttribute("showCaptcha");%>
                <% 
                            if(showCaptcha!=null && showCaptcha) {%>
                <div class="g-recaptcha" data-sitekey="6Ldw_2koAAAAABdyOVbhOa4bJ5Awpiw7w_FxtYxS"></div>
                <%}%>
                <input class="btn" style="width: 30%; margin-left: 240px; display: inline-block" type="submit" value="Change password"> 
                <p style="display: inline-block">or <a href="${pageContext.request.contextPath}/profile">Cancel</a></p>
            </form>
        </div>

        <jsp:include page="../footer.jsp"></jsp:include>
        </body>

        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <script>
            document.getElementById('loginForm').addEventListener('submit', function (event) {
                var showCaptcha = <%= request.getAttribute("showCaptcha") %>;

                if (showCaptcha) {
                    var recaptchaResponse = grecaptcha.getResponse();
                    if (!recaptchaResponse) {
                        event.preventDefault();
                        alert("Please complete the CAPTCHA.");
                    }
                }
            });
    </script>
</html>
