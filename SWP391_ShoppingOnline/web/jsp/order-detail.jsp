<%-- 
    Document   : order-detail
    Created on : Oct 24, 2023, 3:53:51 PM
    Author     : Dell
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${product.proName} | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <!-- Link Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
        <!-- Link File CSS -->
        <link rel="stylesheet" href="css/styles.css">
        <!-- Add your custom CSS styles here -->
        <link rel="stylesheet" href="css/product-detail.css">
    </head>
    <body>
        <style>
            h2{
                font-family: 'Roboto',sans-serif;
                font-size: 26px;
                margin-top: 20px;
                margin-bottom: 10px;
                font-weight: 500;
                line-height: 1.1;
                color: inherit;
            }
            h4{
                font-family: 'Roboto',sans-serif;
                margin-top: 20px;
                margin-bottom: 10px;
                line-height: 1.1;
                color: #333;
                font-weight: lighter;
            }
            table{
                width: 100%;
                background-color: transparent;
                border-spacing: 0;
                border-collapse: collapse;
                /*border-color: gray;*/
                text-indent: initial;
                display: table;
                border: 2px gray solid;
            }
            *{
                /*                margin: 10px;
                                padding: 110px;*/
            }
            th, td{
                border: 2px gray solid;
            }
            .btn{
                /*float: right;*/

                margin: 10px 0px;
                background: #e24d85;
                color: #fff;
                padding: 10px 15px;
                font-weight: bold;
                display: inline-block;
                font-size: 14px;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }
            .info{
                /*width: 80%;*/
            }
            td{
                padding-left: 10px;
                padding-right: 10px;
                text-align: center;
            }
        </style>

        <jsp:include page="../header.jsp"></jsp:include>

            <div class="row" style="margin-left: 110px; margin-right: 100px; margin-bottom: 50px">
                <div class="info">
                    <h2>Order detail</h2>
                    <h4 style="font-weight: bolder; margin: 5px">Hello, ${customer.cusContactName}!</h4>
                    <h4 style="font-weight: bolder; margin: 5px">Total price: ${order.price}</h4>
                    <h4 style="font-weight: bolder; margin: 5px">Note: ${order.orderNote}</h4>
                <!--<h4 style="margin: 5px">Update your account information to have the best store policies and security</h4>-->
                <div style="margin-left: 80%; font-size: 12px;">
                    <a href="profile" type="button" class="btn" style="margin-top: 0">Return to your profile</a>
                </div>
                <table>
                    <thead>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    </thead>
                    <tbody><tr>
                        <c:forEach items="${productOfOrderList}" var="o">
                            <%--<c:forEach items="${orderDetailList}" var="od">--%>
                            <tr>
                                <td class="product-image" style="text-align: center; width: 10%; padding: 5px"><img src="pdimg/${o.pdImage}" alt="alt"/></td>
                                <td style="text-align: center">${o.proName}</td>
                                <td style="">${o.pdColor}</td>
                                <td><span><fmt:formatNumber value="${o.proPrice}" type="number" pattern="#,##0" /> VND</span></td>
                                <td style="text-align: center">${o.odQuantity}</td>
                            </tr>
                            <%--</c:forEach>--%>
                        </c:forEach>
                    </tbody>
                </table>
                
                
            </div>
        </div>
    </div>
    <jsp:include page="../footer.jsp"></jsp:include>

</body>
</html>
