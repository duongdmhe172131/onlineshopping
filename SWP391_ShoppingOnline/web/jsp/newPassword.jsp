<!DOCTYPE html>
<!-- Website - www.codingnepalweb.com -->
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8" />
        <title>Reset Password | iLocal Shop</title>     
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" type="image/x-icon" href="img/account.png">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cssForgot.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="title-text" style="">
                <div class="title login">New Password</div>
                <br>
            </div>
         

            <div class="form-container">
                <div class="form-inner">
                    <form action="${pageContext.request.contextPath}/forgotPassword?service=resetPassword" method="post"  class="login">
                        <div class="field">
                            <input name="newPassword" type="password" placeholder="New Password" required>
                        </div>

                       <div class="field">
                            <input name="confirmPassword" type="password" placeholder="Confirm Password" required>
                        </div>
                        <p style="color: red">${message}</p>
                        <div class="field btn">
                            <div class="btn-layer"></div>
                            <input type="submit" value="Submit">
                        </div>

                    </form>
                </div>
            </div>
        </div>


    </body>
</html>
