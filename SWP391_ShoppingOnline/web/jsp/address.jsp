<%-- 
    Document   : address
    Created on : Oct 11, 2023, 12:55:32 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Address | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <!-- Link Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

        <!-- Link File CSS -->
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <jsp:include page="../header.jsp"></jsp:include>

            <a href="profile" type="button"style="font-size: 17px; margin-left: 55px; ">
                <svg width="10px" height="10px" viewBox="159.94277954101562 191.94850158691406 736.0572204589844 640.074462890625" class="icon" xmlns="http://www.w3.org/2000/svg"><path fill="#000000" d="M224 480h640a32 32 0 110 64H224a32 32 0 010-64z"/><path fill="#000000" d="M237.248 512l265.408 265.344a32 32 0 01-45.312 45.312l-288-288a32 32 0 010-45.312l288-288a32 32 0 1145.312 45.312L237.248 512z"/></svg>
                 Your profile</a>
            <div class="row" style="margin-left: 110px; margin-right: 100px; margin-bottom: 50px; margin-top: 20px">
                <div class="info">
                    <h2>Address information</h2>
                    <h4 style="font-weight: bolder; margin: 5px">Hello, ${customer.cusContactName}!</h4>
                <h4 style="margin: 5px">Update your account information to have the best store policies and security</h4>
                <div style="margin-left: 980px; font-size: 12px;">
                </div>
                <div style="border:solid 1px #dedede; padding:30px; padding-bottom: 80px;width: 40%;">
                    <h3 style="margin: 0;padding: 5px;border-bottom: 1px solid #ccc;margin-bottom: 10px;">${customer.cusContactName}</h3>
                    <p>
                        <strong style="display:inline-block;width:80px">Address:</strong>${addressDetail}<br/>
                        <strong style="display:inline-block;width:80px">City:</strong>${city}<br/>
                        <strong style="display:inline-block;width:80px">District:</strong>${district}<br/>
                        <strong style="display:inline-block;width:80px">Ward:</strong>${ward}<br/>
                        <strong style="display:inline-block;width:80px">Phone:</strong>${customer.cusPhone}<br/>
                    </p>
                    <a class="btn" style="float: left; font-size: 15px; padding: 10px; margin-top: 15px; margin-left: 180px" href="${pageContext.request.contextPath}/updateAddress">Update</a>
                </div>
            </div>
        </div>        
        <jsp:include page="../footer.jsp"></jsp:include>
    </body>
</html>
