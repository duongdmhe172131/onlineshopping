<!DOCTYPE html>
<!-- Website - www.codingnepalweb.com -->
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8" />
        <title>Account | iLocal Shop</title>     
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" type="image/x-icon" href="img/account.png">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cssLogin.css"/>

        <style>

            .error {
                font-size: 14px;
                font-weight: bold;

                margin-top: 5px;

            }
        </style>
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="title-text">
            <div class="title login">Login</div>
            <div class="title signup">Sign up</div>

        </div>
        <div class="form-container">
            <div class="slide-controls">
                <input type="radio" name="slide" id="login" checked>
                <input type="radio" name="slide" id="signup">
                <label for="login" class="slide login">Log in</label>
                <label for="signup" class="slide signup">Sign up</label>
                <div class="slider-tab"></div>
            </div>


            <div class="form-inner">
                <form action="${pageContext.request.contextPath}/login" method="post" class="login" id="loginForm">
                    <div class="field">
                        <input name="username" type="text" placeholder="Username or Email" required>
                    </div>
                    <div class="field">
                        <input name="password" type="password" placeholder="Password" required>
                    </div>

                    <p class="text-danger" style="color: red;">${message}</p>

                    <% Boolean showCaptcha = (Boolean) request.getAttribute("showCaptcha");%>
                    <% 
                            if(showCaptcha!=null && showCaptcha) {%>
                    <div class="g-recaptcha" data-sitekey="6Ldw_2koAAAAABdyOVbhOa4bJ5Awpiw7w_FxtYxS"></div>
                    <%}%>
                    <br>
                    <div class="pass-link"><a href="${pageContext.request.contextPath}/forgotPassword">Forgot password?</a></div>
                    <div class="field btn">
                        <div class="btn-layer"></div>
                        <input type="submit" value="Login">
                    </div>
                    <div class="signup-link">Not a member? <a href="">Sign up now</a></div>

                    <div class="signup-link"> OR</div>

                    <div class="googlebtn">
                        <a href="https://accounts.google.com/o/oauth2/auth?scope=email%20profile&redirect_uri=http://localhost:9999/SWP391_ShoppingOnline/LoginGoogleController&response_type=code
                           &client_id=528312189213-ha8n1d8bbdqccj5br52i2obpkq86c3hu.apps.googleusercontent.com&approval_prompt=force">Login with Google</a>
                    </div>

                </form>





                <form action="${pageContext.request.contextPath}/register" method="post" class="signup" id="registerForm">
                    <div class="field">
                        <input name ="cusName" type="text" placeholder="Contact Name" required>
                    </div>
                    <div class="field">
                        <input name="email" type="text" placeholder="Email" required>
                        <span class="error" style="color: red;">${emailError}</span>

                    </div>
                    <div class="field">
                        <input name="phone" type="text" placeholder="Phone" required>
                        <span class="error" style="color: red;">${phoneError}</span>


                    </div>
                    <div class="field">
                        <input name="cusUsername" type="text" placeholder="Username" required>
                        <span class="error" style="color: red;">${usernameError}</span>

                    </div>
                    <div class="field">
                        <input name="cusPassword"type="password" placeholder="Password" required>
                        <span class="error" style="color: red;">${passwordError}</span>


                    </div>
                    <div class="field">
                        <input name="rePassword" type="password" placeholder="Confirm password" required>
                        <span class="error" style="color: red;">${rePasswordError}</span>

                    </div>
                    <div class="field btn">
                        <div class="btn-layer"></div>
                        <input type="hidden" name ="service" value="emailValid">
                        <input type="submit" value="Sign up">
                    </div>
                </form>

            </div>
        </div>
    </div>
</body>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<script>
    document.getElementById('loginForm').addEventListener('submit', function (event) {
        var showCaptcha = <%= request.getAttribute("showCaptcha") %>;

        if (showCaptcha) {
            var recaptchaResponse = grecaptcha.getResponse();
            if (!recaptchaResponse) {
                event.preventDefault();
                alert("Please complete the CAPTCHA.");
            }
        }
    });
</script>

<script>
    const loginText = document.querySelector(".title-text .login");
    const loginForm = document.querySelector("form.login");
    const loginBtn = document.querySelector("label.login");
    const signupBtn = document.querySelector("label.signup");
    const signupLink = document.querySelector("form .signup-link a");
    signupBtn.onclick = (() => {
        loginForm.style.marginLeft = "-50%";
        loginText.style.marginLeft = "-50%";
    });
    loginBtn.onclick = (() => {
        loginForm.style.marginLeft = "0%";
        loginText.style.marginLeft = "0%";
    });
    signupLink.onclick = (() => {
        signupBtn.click();
        return false;
    });





    function toggleForms() {
        const loginForm = document.querySelector("form.login");
        const signupForm = document.querySelector("form.signup");
        const loginText = document.querySelector(".title-text .login");
        const signupBtn = document.querySelector("label.signup");
        loginForm.style.marginLeft = "-50%";
        loginText.style.marginLeft = "-50%";
        signupBtn.click();
    }


    function showErrorMessages() {

        const emailError = "${emailError}";
        if (emailError && emailError.trim() !== "") {
            toggleForms();
        }

        const phoneError = "${phoneError}";
        if (phoneError && phoneError.trim() !== "") {
            toggleForms();
        }

        const usernameError = "${usernameError}";
        if (usernameError && usernameError.trim() !== "") {
            toggleForms();
        }

        const passwordError = "${passwordError}";
        if (passwordError && passwordError.trim() !== "") {
            toggleForms();
        }

        const rePasswordError = "${rePasswordError}";
        if (rePasswordError && rePasswordError.trim() !== "") {
            toggleForms();
        }
    }

    showErrorMessages();
</script>

<script>
    // Check if registration was successful
    var registrationSuccess = ${sessionScope.registrationSuccess};

    if (registrationSuccess) {
        alert("Please check your email for confirmation!");
    }
</script>


<script>
    // Check if registration was successful
    var alertMessage = ${sessionScope.alertMessage};

    if (alertMessage) {
        alert("Active account successfully !");
    }
</script>


</html>
