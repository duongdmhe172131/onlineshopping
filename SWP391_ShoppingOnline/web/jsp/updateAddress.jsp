<%-- 
    Document   : updateAddress
    Created on : Oct 11, 2023, 3:07:03 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <title>Update Address | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
    </head>
    <style>
        input{
            margin: 0;
            padding: 5px;
            margin-bottom: 10px;
            width: 99%
        }
        /* Popup container - can be anything you want */
        .popup {
            position: relative;
            display: inline-block;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* The actual popup */
        .popup .popuptext {
            visibility: hidden;
            width: 160px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 8px 0;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -80px;
        }

        /* Popup arrow */
        .popup .popuptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        /* Toggle this class - hide and show the popup */
        .popup .show {
            visibility: visible;
            -webkit-animation: fadeIn 1s;
            animation: fadeIn 1s;
        }

        /* Add animation (fade in the popup) */
        @-webkit-keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        @keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity:1 ;
            }
        }
    </style>
    <body>
        <jsp:include page="../header.jsp"></jsp:include>
            <div style="border:solid 1px #dedede; padding:30px; padding-bottom: 80px;width: 40%;margin-left: 300px">
                <form action="updateAddress" method="post">
                    <strong>Name</strong><p style="cursor: pointer; display: inline-block">(*)</p><br/>
                    <input type="text" name="cusName" placeholder="Name" value="${customer.cusContactName}" required><br/>
                <strong>Address</strong>
                <div class="popup" onclick="myFunction()">(*)
                    <span class="popuptext" id="myPopup"></span>
                </div><br/>

                <input type="text" name="cusAddress" placeholder="Address" value="${addressDetail}" required><br/>
                
                
                <div class="pay_item address">
                    <div class="pay_item_title" style="font-weight: bold;">City</div>
                    <select id="city" name="city" style="width: 30%" required>
                        <option value="${city}" selected>${city}</option>           
                    </select>
                </div>
                <div class="pay_item address">
                    <div class="pay_item_title" style="font-weight: bold;">District</div>
                    <select id="district" name="district" style="width: 30%">
                        <option value="${district}" selected>${district}</option>
                    </select>
                </div>
                <div class="pay_item address">
                    <div class="pay_item_title" style="font-weight: bold;">Ward</div>
                    <select id="ward" name="ward" style="width: 30%">
                        <option value="${ward}" selected>${ward}</option>
                    </select>
                </div>
                <strong>Phone</strong>
                <input type="tel" name="cusPhone" placeholder="Phone" value="${customer.cusPhone}" required><br/>

                <strong>Profile image</strong><p style="cursor: pointer; display: inline-block">(*)</p><br/>
                <input style="margin: 0;padding: 5px;border-bottom: 1px solid #ccc;margin-bottom: 10px;"type="file" name="image" accept="image/jpeg, image/png">

                <strong>${msg}</strong><p style="cursor: pointer; display: inline-block"></p><br/>

                <button class="btn" type="submit" value="Update">Update</button>
                <p style="display: inline-block">or <a href="${pageContext.request.contextPath}/profile">Cancel</a></p>
            </form>
        </div>
        <jsp:include page="../footer.jsp"></jsp:include>
            <script>
                // When the user clicks on div, open the popup
                function myFunction() {
                    var popup = document.getElementById("myPopup");
                    popup.classList.toggle("show");
                }
            </script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
            <script>
                const host = "https://provinces.open-api.vn/api/";
                var callAPI = (api) => {
                    return axios.get(api)
                            .then((response) => {
                                renderData(response.data, "city");
                            });
                }
                callAPI('https://provinces.open-api.vn/api/?depth=1');
                var callApiDistrict = (api) => {
                    return axios.get(api)
                            .then((response) => {
                                renderData(response.data.districts, "district");
                            });
                }
                var callApiWard = (api) => {
                    return axios.get(api)
                            .then((response) => {
                                renderData(response.data.wards, "ward");
                            });
                }

                let address = {
                    city: '${city}',
                    district: '${district}',
                    ward: '${ward}'
                }

                var renderData = (array, select) => {
                    let row = ' <option disable value="' + address[select] + '">' + address[select] + '</option>';
                    array.forEach(element => {
                        row += `<option data-id="` + element.code + `" value="` + element.name + `">` + element.name + `</option>`
                    });
                    document.querySelector("#" + select).innerHTML = row
                }

                $("#city").change(() => {
                    callApiDistrict(host + "p/" + $("#city").find(':selected').data('id') + "?depth=2");

                });
                $("#district").change(() => {
                    callApiWard(host + "d/" + $("#district").find(':selected').data('id') + "?depth=2");

                });
                $("#ward").change(() => {

                })

        </script>

    </body>
</html>
