<!DOCTYPE html>
<!-- Website - www.codingnepalweb.com -->
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8" />
        <title>Active account | iLocal Shop</title>     
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" type="image/x-icon" href="img/account.png">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cssForgot.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="title-text" style="">
                <div class="title login">Active your Account</div>
                <br>
            </div>
            <div class="form-container">
                <div class="form-inner">
                    <form action="VerifyEmailHandler" method="post"  class="login">
                        <div class="field">
                            <input name="name" type="text" value="${user}" readonly="">
                            <input name="email" type="text" value="${email}" readonly="" hidden="">

                        </div>
                        <div style="margin-top: 30px;">   
                            <img src="captchaServlet" alt="Captcha">
                        </div>
                        <div class="field">
                            <input name="captcha" type="text" required>
                        </div>
                        <p style="color: red">${message}</p>
                        <div class="field btn">
                            <div class="btn-layer"></div>
                            <input type="submit" value="Submit">
                        </div>
                    </form                </div>
            </div>
        </div>
    </body>

    <script>
        var alertMessage = ${sessionScope.alertMessage};

        if (alertMessage) {
            alert("Update product detail successfully!");
        }

        <%
                session.removeAttribute("alertMessage");
        %>
    </script>
</html>