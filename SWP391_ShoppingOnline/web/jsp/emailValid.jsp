

<!DOCTYPE html>
<!-- Website - www.codingnepalweb.com -->
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8" />
        <title>Valid your Email | iLocal Shop</title>     
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" type="image/x-icon" href="img/account.png">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cssForgot.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="title-text" style="">
                <div class="title login">Enter OTP for email validation</div>
                <br>
            </div>
            <div class="subtitle">Check OTP in your mail</div>

            <div class="form-container">
                <div class="form-inner">
                    <form action="${pageContext.request.contextPath}/register" method="post"  class="login">
                        <div class="field">
                            <input name="otp" type="text" placeholder="OTP" required>
                        </div>

                        <p style="color: red">${otpError}</p>
                        <div class="field btn">
                            <div class="btn-layer"></div>

                            <input name="cusName" type="hidden" value="${sessionScope.cusName}">
                            <input name="email" type="hidden" value="${sessionScope.email}">
                            <input name="cusName" type="hidden" value="${sessionScope.cusName}">
                            <input name="email" type="hidden" value="${sessionScope.email}">
                            <input name="cusName" type="hidden" value="${sessionScope.cusName}">
                            <input type="submit"value="verifyOTP" name="service">
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </body>
</html>
