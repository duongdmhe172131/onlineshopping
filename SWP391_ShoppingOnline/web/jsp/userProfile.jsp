<%-- 
    Document   : userProfile
    Created on : Oct 2, 2023, 10:34:35 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Profile | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <!-- Link Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

        <!-- Link File CSS -->
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <style>
            h2{
                font-family: 'Roboto',sans-serif;
                font-size: 26px;
                margin-top: 20px;
                margin-bottom: 10px;
                font-weight: 500;
                line-height: 1.1;
                color: inherit;
            }
            h4{
                font-family: 'Roboto',sans-serif;
                margin-top: 20px;
                margin-bottom: 10px;
                line-height: 1.1;
                color: #333;
                font-weight: lighter;
            }
            table{
                width: 100%;
                background-color: transparent;
                border-spacing: 0;
                border-collapse: collapse;
                /*border-color: gray;*/
                text-indent: initial;
                display: table;
                border: 2px gray solid;
            }
            *{
                /*                margin: 10px;
                                padding: 110px;*/
            }
            th, td{
                border: 2px gray solid;
                text-align: center;
            }
            .btn{
                /*float: right;*/

                margin: 10px 0px;
                background: #e24d85;
                color: #fff;
                padding: 10px 15px;
                font-weight: bold;
                display: inline-block;
                font-size: 14px;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }
            .info{
                /*width: 80%;*/
            }
            a:link {
                text-decoration: none;
            }

            a:visited {
                text-decoration: none;
            }

            a:hover {
                text-decoration: underline;
            }

            a:active {
                text-decoration: underline;
            }
        </style>
        <jsp:include page="../header.jsp"></jsp:include>

            <div class="row" style="margin-left: 110px; margin-right: 100px; margin-bottom: 50px">
                <div class="info">
                    <h2>Address information</h2>
                    <h4 style="font-weight: bolder; margin: 5px">Hello, ${customer.cusContactName}!</h4>
                <h4 style="margin: 5px">Update your account information to have the best store policies and security</h4>
                <h4 style="font-weight: bolder">Order information</h4>
                <table>
                    <thead>
                    <th>Order ID</th>
                    <th>Order time</th>
                    <th>Total</th>
                    <th>Payment Method</th>
                    <th>Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                        <c:forEach items="${listCusOrder}" var="o">
                            <tr>
                                <td style="text-align: center"><a href="${pageContext.request.contextPath}/order-detail?orderID=${o.orderID}">${o.orderID}</a></td>
                                <td>${o.orderDate}</td>
                                <td><span><fmt:formatNumber value="${o.orderTotalPrice}" type="number" pattern="#,##0" /> VND</span></td>
                                <td>${o.paymentMethod}</td>
                                <td style="text-align:center;">
                                    ${o.getOrderStatus()}
                                </td>
                                <td>
                                    <c:if test="${o.orderSoID == 1}">
                                        <a style="margin-left:20px" href="purchase?ordId=${o.orderID}">Purchase</a>
                                    </c:if>
                                    <c:if test="${o.orderSoID == 4}">
                                        <a style="margin-left:20px" href="SendFeedback?ordId=${o.orderID}">Feedback</a>
                                    </c:if>
                                </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <div class="row">
                    <div style="display: inline-block;">
                        <a href="${pageContext.request.contextPath}/changePassword" type="button" class="btn">Change password</a>
                    </div>
                    <div style="display: inline-block; margin-left: 30px;">
                        <a href="${pageContext.request.contextPath}/address" type="button" class="btn">Address information</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../footer.jsp"></jsp:include>    
</body>
</html>
