<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Manage Order | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <!-- ======= Styles ====== -->
        <link rel="stylesheet" href="css/admin_manager.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin_manager.css">
        <style>
            .pagination {
                margin-left: 36%;
            }
            .pagination input {
                width: 60px;
                margin-left: 5px;
                margin-right: 5px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <!-- Navigation -->
            <jsp:include page="admin-navigation.jsp"></jsp:include>

                <!-- Main Content -->
                <div class="main" style="margin-left: 295px; width: 80%; position: static">
                    <div class="topbar">

                    </div>

                    <div class="row" style="margin-right: 70px;  padding: 10px; border: 1.5px solid #000;">
                        <input type="hidden" id="pageNo" name="pageNo" value="${currentPage}">
                    <div class="col-12" style="margin-bottom: 40px;">
                        <h1>Order Manager</h1>

                    </div>
                    <div class="col-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" value="${search}"
                                   id="search">
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="button" id="btnSearch">
                                    <ion-icon name="search-outline"></ion-icon>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        Start date<input type="date" placeholder="Start date" id="startDate" value="${startDate}"/>
                    </div>
                    <div class="col-3">
                        End date<input type="date" placeholder="End date" id="endDate" value="${endDate}"/>
                    </div>
                    <div class="col-3">
                        <select class="form-control" id="statusOrder" name="status">
                            <option value="-1" ${statusOrder==null ? 'selected' : '' }>All status</option>
                            <c:forEach items="${listStatusOrder}" var="s">
                                <option value="${s.soID}" ${s.soID==statusOrder ? 'selected' : '' }>${s.soName}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-12" style="margin-top: 10px;">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Order Date</th>
                                        <th scope="col">Order Status</th>
                                        <th scope="col">Details</th>
                                        <!--<th scope="col">Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listOrder}" var="o">
                                        <tr>
                                            <td scope="col">${o.orderID}</td>
                                            <c:forEach items="${listCustomer}" var="cus">
                                                <c:if test="${o.orderCusID == cus.cusId}">
                                                    <td scope="col">${cus.cusUsername}</td>
                                                </c:if>
                                            </c:forEach>

                                            <td scope="col">${o.orderDate}</td>
                                            <c:forEach items="${listStatusOrder}" var="so">
                                                <c:if test="${o.orderSoID == so.soID}">
                                                    <td scope="col">${so.soName}</td>
                                                </c:if>
                                            </c:forEach>

                                            <td scope="col"><a href="manageOrderDetail?orderID=${o.orderID}">Details</a></td>
                                            <!--                                            <td scope="col">
                                            
                                                                                <li style="list-style: none">Select</li>
                                                                                <li style="list-style-type: none">Delete</li>
                                            
                                                                                </td>-->
                                        </tr>

                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" id="pageNo" name="pageNo" value="${currentPage}">
                        <input type="hidden" id="totalPage" name="pageNo" value="${totalPage}">
                        <div class="pagination" style="text-align: center">
                            Page
                            <input type="number" value="${currentPage}" min="1" max="${totalPage}" id="currentPage"/>
                            /${totalPage}</div>

                    </div>
                </div>


            </div>
        </div>

        <script src="js/admin_manager.js"></script>
        <!-- ====== ionicons ======= -->
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>           
        <script>
            // handle filter search
            const searchInput = document.querySelector('#search');
            searchInput.addEventListener('keydown', (event) => {
                if (event.key === 'Enter') {
                    performSearch();
                }
            });
            const btnSearch = document.querySelector('#btnSearch');
            btnSearch.addEventListener('click', () => {
                performSearch(); // call function
            });
            function performSearch() {
                const search = document.querySelector('#search').value;
                const startDate = document.querySelector('#startDate').value;
                const endDate = document.querySelector('#endDate').value;
                const statusOrder = document.querySelector('#statusOrder').value;
                const pageNo = document.querySelector('#pageNo').value;
                window.location.href = 'manageOrder?search=' + search + '&startDate=' + startDate +
                        '&endDate=' + endDate +
                        '&statusOrder=' + statusOrder + '&pageNo=1';
            }
            ;

            // handle filter start date
            const sDate = document.querySelector('#startDate');
            sDate.addEventListener('change', () => {
                const search = document.querySelector('#search').value;
                const startDate = document.querySelector('#startDate').value;
                const endDate = document.querySelector('#endDate').value;
                const statusOrder = document.querySelector('#statusOrder').value;
                const pageNo = document.querySelector('#pageNo').value;
                window.location.href = 'manageOrder?search=' + search + '&startDate=' + startDate +
                        '&endDate=' + endDate +
                        '&statusOrder=' + statusOrder + '&pageNo=1';
            });

            // handle filter end date
            const eDate = document.querySelector('#endDate');
            eDate.addEventListener('change', () => {
                const search = document.querySelector('#search').value;
                const startDate = document.querySelector('#startDate').value;
                const endDate = document.querySelector('#endDate').value;
                const statusOrder = document.querySelector('#statusOrder').value;
                const pageNo = document.querySelector('#pageNo').value;
                window.location.href = 'manageOrder?search=' + search + '&startDate=' + startDate +
                        '&endDate=' + endDate +
                        '&statusOrder=' + statusOrder + '&pageNo=1';
            });

            // handle filter statusOrder
            const status = document.querySelector('#statusOrder');
            status.addEventListener('change', () => {
                const search = document.querySelector('#search').value;
                const startDate = document.querySelector('#startDate').value;
                const endDate = document.querySelector('#endDate').value;
                const statusOrder = document.querySelector('#statusOrder').value;
                const pageNo = document.querySelector('#pageNo').value;
                window.location.href = 'manageOrder?search=' + search + '&startDate=' + startDate +
                        '&endDate=' + endDate +
                        '&statusOrder=' + statusOrder + '&pageNo=1';
            });

            // handle pagination
            const page = document.querySelector('#currentPage');

            page.addEventListener('change', () => {
                const currentPageRaw = document.getElementById('currentPage').value;
                const totalPage = document.getElementById('totalPage').value;
                validatePage(currentPageRaw, totalPage);
                const currentPage = document.getElementById('currentPage').value;
                changePage(currentPage);
            });
            function changePage(pageNo) {
                const search = document.querySelector('#search').value;
                const startDate = document.querySelector('#startDate').value;
                const endDate = document.querySelector('#endDate').value;
                const statusOrder = document.querySelector('#statusOrder').value;
                window.location.href = 'manageOrder?search=' + search + '&startDate=' + startDate +
                        '&endDate=' + endDate +
                        '&statusOrder=' + statusOrder + '&pageNo=' + pageNo;
            }

            function validatePage(pageNo, totalPage) {
                if (pageNo >= totalPage) {
                    console.log("sakdfjklasd");
                    document.getElementById('currentPage').value = totalPage;
                } else if (pageNo <= 1) {
                    document.getElementById('currentPage').value = 1;
                }
            }

        </script>
    </body>

</html>
