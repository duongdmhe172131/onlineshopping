<%-- 
    Document   : feedbacklist
    Created on : Oct 18, 2023, 3:39:46 PM
    Author     : 84375
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="product-feedback-list">
    <h2>Feedback:</h2>
    <table class="product-feedback-table">
        <thead>
            <tr>
                <th>Rating</th>
                <th>Review</th>
                <th>Reviewer</th>
                <th>Posted Date</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${feedbackList}" var="feedback">
                <tr>
                    <td>${feedback.rating}</td>
                    <td>${feedback.review}</td>
                    <td>${feedback.reviewerName}</td>
                    <td><fmt:formatDate value="${feedback.postedDate}" pattern="dd/MM/yyyy HH:mm" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
