<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Danh sách Bài Ðăng | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="/index.html"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true" >
            <jsp:param name="feedback" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Feedback Detail</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                            <form method="post" action="FeedbackDetail">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Id: </label>
                                        <input readonly type="text" value="${f.fbId}" name="fid" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>User Name: </label>
                                        <input readonly type="text" value="${f.customer.cusName}" name="" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Email: </label>
                                        <input readonly type="text" value="${f.customer.cusEmail}" name="" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Product Name: </label>
                                        <input readonly type="text" value="${f.product.pName}" name="" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label> Product Brand: </label>
                                        <input readonly type="text" value="${f.product.pBrand}" name="" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Star: </label>
                                        <input readonly type="text" value="${f.fbStar}&#11088;" name="" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Create Date: </label>
                                        <input readonly type="text" value="${f.fbDate}" name="" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Content: </label>
                                        <input readonly type="text" value="${f.getFbContent()}" name="" class="form-control">
                                    </div>
                                    <div class="col-md-12">
                                        <label>Image: </label>
                                        <img src="${f.fbImage}" width="40%" alt="alt" >
                                    </div>
                                    <div class="col-md-6">
                                        <label>Status: </label>
                                        <input  type="radio" value="1" name="ss" ${f.fbStatus == 1? "checked":""} >Active
                                        <input  type="radio" value="0" name="ss"${f.fbStatus == 1? "":"checked"}>Inactive
                                    </div>
                                    <div class="col-md-12">
                                        <button style="margin: 10px" class="btn btn-primary" type="submit" >Update</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Essential javascripts for application to work-->
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="./js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="./js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    </body>
</html>