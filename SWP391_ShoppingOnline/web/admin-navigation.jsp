<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="navigation" style="left:0;">
    <ul>
        <li>
            <a href="#">
                <span class="icon">
                    <img src="${pageContext.request.contextPath}/img/logo.png" width="40" height="40" />
                </span>
                <span class="title">iLocal<span class="shop-text">Shop</span></span>
            </a>
        </li>
        


                <c:if test="${sessionScope.role == 3}">
                    <li>
                        <a href="../admin/home">
                            <span class="icon">
                                <ion-icon name="bar-chart"></ion-icon>
                            </span>
                            <span class="title">Dashboard</span>
                        </a>
                    </li>
                </c:if>

                <c:if test="${sessionScope.role == 3}">
                    <li>
                        <a href="../sale/manageOrder">
                            <span class="icon">
                                <ion-icon name="cart"></ion-icon>
                            </span>
                            <span class="title">Orders</span>
                        </a>
                    </li>
                </c:if>





                <c:if test="${sessionScope.role == 2}">
                    <li>

                        <a href="../marketing/manage-product">

                            <span class="icon">
                                <ion-icon name="archive"></ion-icon>
                            </span>
                            <span class="title">Products</span>
                        </a>
                    </li>
                </c:if>

                <c:if test="${sessionScope.role == 2}">
                    <li>
                        <a href="../admin/FeedbackList">
                            <span class="icon">
                                <ion-icon name="chatbubble"></ion-icon>
                            </span>
                            <span class="title">Feedbacks</span>
                        </a>
                    </li>
                </c:if>


                <c:if test="${sessionScope.role == 2}">
                    <li>
                        <a href="../marketing/manage-slider">
                            <span class="icon">
                                <ion-icon name="tablet-landscape"></ion-icon>
                            </span>
                            <span class="title">Sliders</span>
                        </a>
                    </li>
                </c:if>

                <c:if test="${sessionScope.role == 1}">
                    <li>
                        <a href="manage">
                            <span class="icon">
                                <ion-icon name="people"></ion-icon>
                            </span>
                            <span class="title">Manager</span>
                        </a>
                    </li>
                </c:if>

                <c:if test="${sessionScope.role == 1}">
                    <li>
                        <a href="customer">
                            <span class="icon">
                                <ion-icon name="people"></ion-icon>
                            </span>
                            <span class="title">Customer</span>
                        </a>
                    </li>
                </c:if>
                <li>
                    <a href="../admin/profile">
                        <span class="icon">
                            <ion-icon name="people"></ion-icon>
                        </span>
                        <span class="title">My Profile</span>
                    </a>
                </li>
                <li>
                    <a href="../admin/logout">
                        <span class="icon">
                            <ion-icon name="log-out"></ion-icon>
                        </span>
                        <span class="title">Sign Out</span>
                    </a>
                </li>
            </ul>
        </div>