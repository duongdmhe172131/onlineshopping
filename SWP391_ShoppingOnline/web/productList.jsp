<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Home | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <!-- Link Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

        <!-- Link File CSS -->
        <link rel="stylesheet" href="css/styles.css">
        <style>
            #prePage, #nextPage {
                padding: 5px 10px 5px 10px;
                font-size: 15px;
                font-weight: bold;
                background-color: #172856;
                color: white;
                border-radius: 5px;
                border: none;
            }
            #prePage:hover, #nextPage:hover {
                background-color: white;
                color: #172856;
            }
            
            
        </style>
    </head>
    <body>
        <section class="sec-2" id="anchor">

            <div class="content" style="margin-top: 10px">
                <div class="titile">
                    <h4>products list</h4>
                    <div style="padding-right:20px; display: flex">
                        <h4>View as</h4>
                        <form action="search#category" method="get" id="formSort" style="margin-left:-5px; margin-top: 7%">
                            <input type="hidden" name="status" value="${status}"/>
                            <input type="hidden" name="productName" value="${productName}"/>
                            <input type="hidden" name="cateID" value="${cateID}"/>
                            <select id="select" name="sort" onchange="submitForm()" style="font-size: 15px">
                                <option value="Default" >Default</option>
                                <option value="HighToLow" ${sortOpt == 'HighToLow' ? 'selected':''}>High to low</option>
                                <option value="LowToHigh" ${sortOpt == 'LowToHigh' ? 'selected':''}>Low to high</option>
                            </select>
                        </form>
                    </div>

                </div>
                <div class="all-items" id="items">
                    <c:choose>
                        <c:when test="${listProduct.size()==0}">  
                            <h4 style="">New Product will coming soon!</h4>
                        </c:when>
                        <c:otherwise>
                            <c:forEach items="${listProduct}" var="p">
                                <div class="item" pro-price="" id="pro">
                                    <div class="div-img">
                                        <a href="${pageContext.request.contextPath}/productdetail?proID=${p.proID}"></a><img src="img/${p.proImage}" alt=""></a>
                                    </div>
                                    <div class="div-text">
                                        <br>
                                        <h4><a href="${pageContext.request.contextPath}/productdetail?proID=${p.proID}">${p.proName}</a></h4>
                                        <br>
                                        <div class="stars">
                                            <c:if test="${p.averageStar >= 1}">
                                                <i class="fa-solid fa-star"></i>
                                            </c:if>
                                            <c:if test="${p.averageStar >= 2}">
                                                <i class="fa-solid fa-star"></i>
                                            </c:if>
                                            <c:if test="${p.averageStar >= 3}">
                                                <i class="fa-solid fa-star"></i>
                                            </c:if>
                                            <c:if test="${p.averageStar >= 4}">
                                                <i class="fa-solid fa-star"></i>
                                            </c:if>
                                            <c:if test="${p.averageStar >= 5}">
                                                <i class="fa-solid fa-star"></i>
                                            </c:if>
                                        </div>
                                        <div class="price">
                                            <span><fmt:formatNumber value="${p.proPrice}" type="number" pattern="#,##0" /> VND</span>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </section>

        <input type="hidden" id="pageNo" name="pageNo" value="${currentPage}">
        <input type="hidden" id="category" name="category" value="${cateID}">
        <input type="hidden" id="sortOpt" name="sortOpt" value="${sortOpt}">
        <input type="hidden" id="option" name="option" value="${option}">
        <input type="hidden" id="sortStatus" name="sortStatus" value="${sortStatus}">
        <input type="hidden" id="totalPage" name="totalPage" value="${totalPage}">
        <div class="pagination" style="text-align: center" >
           <button style="margin-right: 20px;" ${currentPage == 1 ? "disabled" : ""} id="prePage">Back</button>
            Page
            <input type="number" value="${currentPage}" min="1" max="${totalPage}" id="currentPage" style="text-align: center; margin-bottom: 20px"/>
            /${totalPage}
            <button style="margin-left: 20px;" ${currentPage == totalPage ? "disabled" : ""} id="nextPage">Next</button>
        </div>

<!--        <div id="pagination" class="pagination-container" style=" margin-bottom: 12px;">
            <c:forEach begin="1" end="${totalPage}" step="1" var="page">
                <c:choose>
                    <c:when test="${option == null && sortStatus == null}">
                        <a href="home?currentPage=${page}&category=${cateID}#category">${page}</a>
                    </c:when>
                    <c:otherwise>
                        <a href="search?currentPage=${page}&productName=${productName}&cateID=${cateID}&sort=${sortOpt}#category">${page}</a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </div>-->
        <script>
            // handle pagination
            const page = document.querySelector('#currentPage');
            
            page.addEventListener('change', () => {
                const currentPageRaw = document.getElementById('currentPage').value;
                const totalPage = document.getElementById('totalPage').value;
                const currentPage = validatePage(currentPageRaw, totalPage);
                changePage(currentPage);
            });
            
            const prePage = document.querySelector('#prePage');
            
            prePage.addEventListener('click', () => {
                const currentPageRaw = document.getElementById('currentPage').value;
                changePage2(parseInt(currentPageRaw) - 1);
            });
            const nextPage = document.querySelector('#nextPage');
            
            nextPage.addEventListener('click', () => {
                const currentPageRaw = document.getElementById('currentPage').value;
                changePage2(parseInt(currentPageRaw) + 1);
            });
            function changePage2(pageNo) {
                const search = document.querySelector('#productName').value;
                const category = document.querySelector('#category').value;
                const pageNocurrentPage = document.getElementById('currentPage').value;
                const sortOpt = document.querySelector('#sortOpt').value;
                const option = document.querySelector('#option').value;
                const sortStatus = document.querySelector('#sortStatus').value;
                if(option === null && sortStatus === null){
                    console.log("if");
                    window.location.href = 'home?currentPage=' + pageNo +'&category=' 
                            + category + '#category';
                } else {
                    console.log("else");
                    window.location.href = 'search?currentPage=' + pageNo + '&productName=' +
                            search + '&cateID=' + category + '&sort=' +
                            sortOpt + '#category';
                }
            }
            
            function changePage(pageNo) {
                console.log("changePage");
                console.log(pageNo);
                const search = document.querySelector('#productName').value;
                const category = document.querySelector('#category').value;
                const currentPage = document.getElementById('currentPage').value;
                const sortOpt = document.querySelector('#sortOpt').value;
                const option = document.querySelector('#option').value;
                const sortStatus = document.querySelector('#sortStatus').value;
                if(option === null && sortStatus === null){
                    console.log("if");
                    window.location.href = 'home?currentPage=' + currentPage +'&category=' 
                            + category + '#category';
                } else {
                    console.log("else");
                    window.location.href = 'search?currentPage=' + currentPage + '&productName=' +
                            search + '&cateID=' + category + '&sort=' +
                            sortOpt + '#category';
                }
            }
            
            function validatePage(pageNo, totalPage) {
                if(pageNo >= totalPage){
                    document.getElementById('currentPage').value = totalPage;
                } else if(pageNo <=1){
                    document.getElementById('currentPage').value = 1;
                }
            }
        </script>
    </body> 

</html>
