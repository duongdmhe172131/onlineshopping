<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Home | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <!-- Link Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

        <!-- Link File CSS -->
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>



        <jsp:include page="header.jsp"></jsp:include>

        <jsp:include page="cateBanner.jsp"></jsp:include>

        <jsp:include page="productList.jsp"></jsp:include>

        <jsp:include page="footer.jsp"></jsp:include>


        </body>

        <script>
            let input = document.getElementById('productName');

            input.addEventListener('keyup', function (event) {
                let val = event.target.value;
                if (val === '') {
                } else {
                    document.getElementById('searchIcon').addEventListener('click', function () {
                        document.querySelector('form').submit();
                    });
                }
            });



            function submitForm() {
                document.getElementById('formSort').submit();
                document.getElementById('searchIcon').submit();
            }
            function scroll() {
                if (window.location.href.includes("#category")) {
                    const element = document.getElementById('anchor');
                    element.scrollIntoView({behavior: 'smooth'});
                }
            }
            document.addEventListener('DOMContentLoaded', scroll())

            function scrollSmoothToAnchor() {
                const element = document.getElementById('anchor');
                element.scrollIntoView({behavior: 'smooth', block: 'start'});
            }


        function scrollSmoothToAnchor() {
            const element = document.getElementById('anchor');
            element.scrollIntoView({behavior: 'smooth', block: 'start'});
        }

        document.addEventListener('DOMContentLoaded', function () {
            if (window.location.href.includes("#anchor")) {
                scrollSmoothToAnchor();
            }
        });
    </script>
<!--     <script type="text/javascript" src="js/sortPrice.js"></script>-->



        <script>
            document.querySelectorAll('a[href^="#"]').forEach(anchor => {
                anchor.addEventListener('click', function (e) {
                    e.preventDefault();

                    document.querySelector(this.getAttribute('href')).scrollIntoView({
                        behavior: 'smooth'
                    });
                });
            });

            function scrollToAnchor(targetId) {
                const targetElement = document.getElementById(targetId);
                if (targetElement) {
                    targetElement.scrollIntoView({
                        behavior: 'smooth'
                    });
                }
            }

        </script>




    <!--    <script type="text/javascript" src="js/sortPrice.js"></script>-->
    <!--    <script>
            let list = [
    <c:forEach items="${listProduct}" var="pro">
        '<c:out value="${pro.proPrice}"/>',
    </c:forEach>
    ];
    console.log(list);
    
    let temp = document.querySelectorAll(".item");
  
    let new_list = Array.from(list);
    let arr = [];
    
    for(let i=0; i<new_list.length; i++){
        price = Number(new_list[i]);
        temp[i].setAttribute('pro-Price', price);
        arr.push(price);
    }
    
    let option = document.getElementById("select");
    console.log(option.value);
    option.onchange = sortPrice;
    
    function sortPrice(){
        if(option.value === 'Default'){
            
        }
        if(option.value === 'HighToLow'){
            arr.sort(function(a,b){return a-b});
            while (temp.firstChild()){
                
            }
        }
    }
    
    
</script>-->
</html>
