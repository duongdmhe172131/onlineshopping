<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Manage Feedback | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/logo.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <!-- Include Bootstrap CSS via CDN link -->
        <!-- ======= Styles ====== -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin_manager.css">
    </head>

    <body>
        <div class="container-fluid">
            <!-- Navigation -->
            <jsp:include page="admin-navigation.jsp"></jsp:include>

                <!-- Main Content -->
                <div class="main" style="margin-left: 50px; margin-right: 50px;">
                    <div class="topbar">
                        <!-- <div class="toggle">
            <ion-icon name="menu-outline"></ion-icon>
        </div>
        <div class="user">
            <img src="assets/imgs/customer01.jpg" alt="">
        </div> -->
                    </div>

                    <div class="row" style="margin-right: 70px;  padding: 10px; border: 1.5px solid #000;">
                        <div class="tile-body">
                            <h1>Feedback List</h1>
                            <div class="row element-button" style="margin-bottom: 15px;">
                                <div class="col-sm-2">
                                </div>

                                <div class="col-sm-8">
                                    <form style="display: flex; justify-content: space-around; align-items: center; width: 100%;" method="get" action="FeedbackList">
                                        <label style="margin-right: 5px;"> Status:</label>
                                        <select onchange="this.form.submit()" class="form-control" name="status">
                                            <option value="" ${param['status']==""?"selected":""}>All</option>     
                                        <option value="1" ${param['status']=="1"?"selected":""}>Active</option>
                                        <option value="0" ${param['status']=="0"?"selected":""}>Inactive</option>
                                    </select>
                                    <label style="margin-right: 5px;">Sort: </label>
                                    <select onchange="this.form.submit()" name="star" class="form-control">
                                        <option value="" ${param['star']==""?"selected":""}>All</option>     
                                        <option value="1" ${param['star']=="1"?"selected":""}>1</option>     
                                        <option value="2" ${param['star']=="2"?"selected":""}>2</option>     
                                        <option value="3" ${param['star']=="3"?"selected":""}>3</option>     
                                        <option value="4" ${param['star']=="4"?"selected":""}>4</option>     
                                        <option value="5" ${param['star']=="5"?"selected":""}>5</option>     
                                    </select>
                                    <label style="margin-right: 5px;"> Search:</label> 
                                    <input style="margin-right: 5px;" type="text" maxlength="50" value="${param['search']}"  placeholder="Search UserName or ProductName" name="search" class="form-control">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </form>
                            </div>
                        </div>

                        <table id="tablepro"  class="table table-hover table-bordered" >
                            <thead>
                                <tr>
                                    <th>ID 	&#8645;</th>
                                    <th>User Name 	&#8645;</th>
                                    <th>Product Name 	&#8645;</th>
                                    <th>Product Image 	&#8645;</th>
                                    <th>Star 	&#8645;</th>
                                    <th>Create Date 	&#8645;</th>
                                    <th>Status 	&#8645; </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="f" items="${fl}">
                                    <tr>
                                        <td><a href="FeedbackDetail?fid=${f.fbId}" class="btn btn-primary">${f.fbId}</a></td>
                                        <td>${f.customer.cusUsername}</td>
                                        <td>${f.product.proName}</td>
                                        <td><img src="${pageContext.request.contextPath}/img/${f.product.proImage}" style="width: 75px; display: table; margin: 0px -10px;" alt=""></td>
                                        <td>${f.fbStar}&#11088;</td>
                                        <td>${f.fbDate}</td>
                                        <td>${f.fbStatus==1?"Active":"Inactive"}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <!-- =========== Scripts =========  -->
    <script src="js/admin_manager.js"></script>
    <!-- ====== ionicons ======= -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
                                        $(document).ready(function () {
                                            $("#tablepro").DataTable({bFilter: false, bInfo: false, paging: true, lengthChange: false});
                                        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>

</html>