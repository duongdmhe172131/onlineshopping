<%-- 
    Document   : cartContact
    Created on : Oct 15, 2023, 10:21:52 PM
    Author     : windy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Information Order | iLocal Shop</title>
        <link rel="icon" type="image/x-icon" href="img/logo.png">
        <link href="css/cssCart.css" rel="stylesheet" type="text/css"/>
        <style>
            h1 {
                text-align: center;
            }

        </style>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
            <h1>Payment information</h1>
            <form id="check-out" action="order" method="post">

                <input type="hidden" name="price" value="${price}">

            <c:set value="${customer}" var="c"></c:set>
                <div class="pay_item name">
                    <div class="pay_item_title">Name</div>
                    <input class="pay_item_text" type="text" name="name" value="${c.cusContactName}" required/>
            </div>
            <div class="pay_item phone">
                <div class="pay_item_title">Phone number</div>
                <input class="pay_item_text" type="text" name="phone" value="${c.cusPhone}" required/>
            </div>
            <div class="pay_item address">
                <div class="pay_item_title">Address</div>
                <input class="pay_item_text" type="text" name="address" value="${addressDetail}" required/>
            </div>

            <div class="pay_item address">
                <div class="pay_item_title">City</div>
                <select id="city" name="city" style="width:20%;" required>
                    <option value="${city}" selected>${city}</option>           
                </select>
            </div>
            <div class="pay_item address">
                <div class="pay_item_title">District</div>
                <select id="district" style="width:20%;" name="district">
                    <option value="${district}" selected>${district}</option>
                </select>
            </div>
            <div class="pay_item address">
                <div class="pay_item_title">Ward</div>
                <select id="ward" name="ward" style="width:20%;">
                    <option value="${ward}" selected>${ward}</option>
                </select>
            </div>

            <!--            <div class="pay_item address">
                            <div class="pay_item_title">Address</div>
                            <input class="pay_item_text" type="text" name="address" value="${c.cusAddress}" required/>
                        </div>-->
            <div class="pay_item">
                <div class="pay_item_title">Payment</div>
                <div style="display :flex; justify-content: between; align-items: center; width: 30%">
                    <div style="margin-right: 10%">
                        <input type="radio" name="paymentMethod" value="deposit" required>
                        <label for="paymentMethod">Deposit</label>
                    </div>
                    <div>
                        <input type="radio" name="paymentMethod" value="fullpayment">
                        <label for="paymentMethod">Full payment</label>
                    </div>
                </div>

            </div>
            <div class="pay_item note">
                <div class="pay_item_title">Note</div>
                <textarea class="pay_item_textarea" name="note"></textarea>
            </div>
            <div class="pay_item note">
                <div class="pay_item_title">${msg}</div>
            </div>
            <button class="modePayment" style="color: white">Confirm</button>
        </form>

        <br><br>

        <jsp:include page="footer.jsp"></jsp:include>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
            <script>
                const host = "https://provinces.open-api.vn/api/";
                var callAPI = (api) => {
                    return axios.get(api)
                            .then((response) => {
                                renderData(response.data, "city");
                            });
                }
                callAPI('https://provinces.open-api.vn/api/?depth=1');
                var callApiDistrict = (api) => {
                    return axios.get(api)
                            .then((response) => {
                                renderData(response.data.districts, "district");
                            });
                }
                var callApiWard = (api) => {
                    return axios.get(api)
                            .then((response) => {
                                renderData(response.data.wards, "ward");
                            });
                }

                var renderData = (array, select) => {
                    let row = ' <option disable value="">${city}</option>';
                    array.forEach(element => {
                        row += `<option data-id="` + element.code + `" value="` + element.name + `">` + element.name + `</option>`
                    });
                    document.querySelector("#" + select).innerHTML = row
                }

                $("#city").change(() => {
                    callApiDistrict(host + "p/" + $("#city").find(':selected').data('id') + "?depth=2");
                    printResult();
                });
                $("#district").change(() => {
                    callApiWard(host + "d/" + $("#district").find(':selected').data('id') + "?depth=2");
                    printResult();
                });
                $("#ward").change(() => {
                    printResult();
                })

                var printResult = () => {
                    if ($("#district").find(':selected').data('id') != "" && $("#city").find(':selected').data('id') != "" &&
                            $("#ward").find(':selected').data('id') != "") {
                        let result = $("#city option:selected").text() +
                                " | " + $("#district option:selected").text() + " | " +
                                $("#ward option:selected").text();
                        $("#result").text(result)
                    }
                }
        </script>

    </body>
</html>
