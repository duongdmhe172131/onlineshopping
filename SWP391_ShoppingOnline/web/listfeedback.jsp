<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Danh sách Bài Ðăng | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="/index.html"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true" >
            <jsp:param name="feedback" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Danh sách Feedback ${fl.size()}</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                            <div class="row element-button">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-delete btn-sm print-file" type="button" title="In" onclick="myApp.printTable()"><i
                                            class="fas fa-print"></i> In dữ liệu</a>
                                </div>

                                <div class="col-sm-8">
                                    <form style="display: flex; justify-content: space-around; align-items: center; width: 100%;" method="get" action="FeedbackList">
                                        <label style="margin-right: 5px;"> Status:</label> <select class="form-control" name="status">
                                            <option value="" ${param['status']==""?"selected":""}>All</option>     
                                            <option value="1" ${param['status']=="1"?"selected":""}>Active</option>
                                            <option value="0" ${param['status']=="0"?"selected":""}>Inactive</option>
                                        </select>
                                        <label style="margin-right: 5px;">Sort: </label>
                                        <select name="star" class="form-control">
                                            <option value="" ${param['star']==""?"selected":""}>All</option>     
                                            <option value="1" ${param['star']=="1"?"selected":""}>1</option>     
                                            <option value="2" ${param['star']=="2"?"selected":""}>2</option>     
                                            <option value="3" ${param['star']=="3"?"selected":""}>3</option>     
                                            <option value="4" ${param['star']=="4"?"selected":""}>4</option>     
                                            <option value="5" ${param['star']=="5"?"selected":""}>5</option>     
                                            </select>
                                        <label style="margin-right: 5px;"> Search:</label> 
                                        <input style="margin-right: 5px;" type="text" maxlength="50" value="${param['search']}"  placeholder="Search UserName or ProductName" name="search" class="form-control">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </form>
                                </div>
                            </div>

                            <table id="tablepro"  class="table table-hover table-bordered" >
                                <thead>
                                    <tr>
                                        <th>ID 	&#8645;</th>
                                        <th>User Name 	&#8645;</th>
                                        <th>Product Name 	&#8645;</th>
                                        <th>Star 	&#8645;</th>
                                        <th>Create Date 	&#8645;</th>
                                        <th>Status 	&#8645; </th>
                                        <th>Detail 	&#8645;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="f" items="${fl}">
                                        <tr>
                                            <td>${f.fbId}</td>
                                            <td>${f.customer.cusName}</td>
                                            <td>${f.product.pName}</td>
                                            <td>${f.fbStar}&#11088;</td>
                                            <td>${f.fbDate}</td>
                                            <td>${f.fbStatus==1?"Active":"Inactive"}</td>
                                            <td><a href="FeedbackDetail?fid=${f.fbId}" class="btn btn-primary">Detail</a></td>

                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Essential javascripts for application to work-->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
        <script>
                                        $(document).ready(function () {
                                            $("#tablepro").DataTable({bFilter: false, bInfo: false, paging: true, lengthChange: false});
                                        });
        </script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="./js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    </body>
</html>