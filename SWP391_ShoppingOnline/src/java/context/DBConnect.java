/*er, choose License Headers in Project Properties.
 * To change this template fil
 * To change this license heade, choose Tools | Templates
 * and open the template in the editor.
 */
package context;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DBConnect {

    public Connection conn = null;

    public DBConnect() {
        try {
            String user = System.getenv("DB_USERNAME");
            String pass = System.getenv("DB_PASSWORD");
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SE1746_KS_SWP391_G6";
            //goi driver
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, user, pass);
            System.out.println("Connected");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//
//    public DBConnect() {
//        this("jdbc:sqlserver://localhost:1433;databaseName=SE1746_KS_SWP391_G6",
//                "sa", "0000");
//
//    }
    public ResultSet getData(String sql) {
        ResultSet rs = null;
        Statement state;
        try {
            state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            rs = state.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public static void main(String[] args) {
         DBConnect obj = new DBConnect();
//        System.out.println(System.getenv("DB_USERNAME") + ", " +System.getenv("DB_PASSWORD") );
    }
}
