/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.ProductOfOrder;

/**
 *
 * @author Dell
 */
public class ProductOfOrderDAO extends DBConnect {

    public ArrayList<ProductOfOrder> getProductOfOrderbyProID(String id) {
        ArrayList<ProductOfOrder> proOfOrder = new ArrayList<>();
        String sql = "SELECT proID, proCateID, proName, proPrice, proImage, proDetail, proStatus, odQuantity, odOrderID, pdColor, pdID\n"
                + "FROM Product, OrderDetail, ProductDetail\n"
                + "WHERE pdProID = proID and odOrderID = ? and odPdID = pdID";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                boolean proStatus = rs.getBoolean(7);
                int odQuantity = rs.getInt(8);
                int odOrderID = rs.getInt(9);
                String pdColor = rs.getString(10);
                
                ProductOfOrder listProductOfOrder = new ProductOfOrder(proID, proCateID, proName, proPrice, proImage, proDetail, proStatus, odQuantity, odOrderID, pdColor);
                listProductOfOrder.setPdId(rs.getInt("pdId"));
                proOfOrder.add(listProductOfOrder);
            }
        } catch (Exception e) {
            System.out.println("getProductOfOrderbyProID: " + e.getMessage());
        }
        return proOfOrder;
    }
    public static void main(String[] args) {
        ProductOfOrderDAO dao = new ProductOfOrderDAO();
        System.out.println(dao.getProductOfOrderbyProID("5"));
    }
}
