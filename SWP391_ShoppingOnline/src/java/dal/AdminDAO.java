/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Admin;
import context.DBConnect;
import java.util.ArrayList;

/**
 *
 * @author BEAN
 */
public class AdminDAO extends DBConnect {

    public Admin login(String identifier, String password) {
        String sql = " select * from Admin\n"
                + "where (adUsername = ? OR adEmail = ?) and adPassword = ?";
        
        try {
            System.out.println("try");
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, identifier);
            st.setString(2, identifier);
            st.setString(3, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adId = rs.getInt(1);
                String adLastName = rs.getString(2);
                String adFirstName = rs.getString(3);
                String adUsername = rs.getString(4);
                String adPassword = rs.getString(5);
                String adImage = rs.getString(6);
                boolean adGender = rs.getBoolean(7);
                Date adGoB = rs.getDate(8);
                String adPhone = rs.getString(9);
                String adEmail = rs.getString(10);
                String adAddress = rs.getString(11);
                int adStatus = rs.getInt(12);
                int adRoleID = rs.getInt(13);
                Admin admin = new Admin(adId, adLastName, adFirstName, adUsername, adPassword, adImage, adGender, adGoB,
                        adPhone, adEmail, adAddress, adStatus, adRoleID);
                return admin;
            }
        } catch (Exception e) {
            System.out.println("adminLogin: " + e.getMessage());
        }
        return null;
    }

    public Admin checkAdminExistByUsername(String username) {
        String sql = "select * from Admin where adUsername = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adId = rs.getInt(1);
                String adLastName = rs.getString(2);
                String adFirstName = rs.getString(3);
                String adUsername = rs.getString(4);
                String adPassword = rs.getString(5);
                String adImage = rs.getString(6);
                boolean adGender = rs.getBoolean(7);
                Date adGoB = rs.getDate(8);
                String adPhone = rs.getString(9);
                String adEmail = rs.getString(10);
                String adAddress = rs.getString(11);
                int adStatus = rs.getInt(12);
                int adRoleID = rs.getInt(13);
                Admin admin = new Admin(adId, adLastName, adFirstName, adUsername, adPassword, adImage, adGender, adGoB,
                        adPhone, adEmail, adAddress, adStatus, adRoleID);
                return admin;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void changePassword(String email, String password) {
        String sql = "UPDATE Admin SET adPassword = ? WHERE adEmail = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, password);
            st.setString(2, email);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Admin checkAdminExistEmail(String email) {
        String sql = "select * from Admin where adEmail  = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adId = rs.getInt(1);
                String adLastName = rs.getString(2);
                String adFirstName = rs.getString(3);
                String adUsername = rs.getString(4);
                String adPassword = rs.getString(5);
                String adImage = rs.getString(6);
                boolean adGender = rs.getBoolean(7);
                Date adGoB = rs.getDate(8);
                String adPhone = rs.getString(9);
                String adEmail = rs.getString(10);
                String adAddress = rs.getString(11);
                int adStatus = rs.getInt(12);
                int adRoleID = rs.getInt(13);
                Admin admin = new Admin(adId, adLastName, adFirstName, adUsername, adPassword, adImage, adGender, adGoB,
                        adPhone, adEmail, adAddress, adStatus, adRoleID);
                return admin;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public ArrayList<Admin> getListAdmin() {
        ArrayList<Admin> listAdmin = new ArrayList<>();
        String sql = "select * from Admin where adStatus = 1";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adId = rs.getInt(1);
                String adLastName = rs.getString(2);
                String adFirstName = rs.getString(3);
                String adUsername = rs.getString(4);
                String adPassword = rs.getString(5);
                String adImage = rs.getString(6);
                boolean adGender = rs.getBoolean(7);
                Date adGoB = rs.getDate(8);
                String adPhone = rs.getString(9);
                String adEmail = rs.getString(10);
                String adAddress = rs.getString(11);
                int adStatus = rs.getInt(12);
                int adRoleID = rs.getInt(13);
                Admin admin = new Admin(adId, adLastName, adFirstName, adUsername, adPassword, adImage, adGender, adGoB,
                        adPhone, adEmail, adAddress, adStatus, adRoleID);
                listAdmin.add(admin);
            }
        } catch (Exception e) {
        }
        return listAdmin;
    }

    public ArrayList<Admin> getListAdminByFilter(int roleId, int status, String search, int pageNo, int pageSize) {
        ArrayList<Admin> listAdmin = new ArrayList<>();
        String sql = "SELECT * FROM Admin";
        boolean whereAdded = false; // A flag to track whether "WHERE" has been added to the SQL query.
        if (roleId != -1 || status != -1 || !search.isEmpty()) {
            sql += " WHERE";
            if (roleId != -1) {
                sql += " adRoleId = ?";
                whereAdded = true;
            }
            if (status != -1) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " adStatus = ?";
                whereAdded = true;
            }
            if (!search.isEmpty()) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " (adLastName LIKE ? OR adFirstName LIKE ? OR adUsername LIKE ? OR adEmail LIKE ?)";
            }
        }

        sql += " ORDER BY adId OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1; // Start with the first parameter index
            if (roleId != -1) {
                st.setInt(parameterIndex, roleId);
                parameterIndex++;
            }
            if (status != -1) {
                st.setInt(parameterIndex, status);
                parameterIndex++;
            }
            if (!search.isEmpty()) {
                for (int i = 0; i < 4; i++) {
                    st.setString(parameterIndex, "%" + search + "%");
                    parameterIndex++;
                }
            }
            // Set the limit and offset parameters for pagination
            st.setInt(parameterIndex, (pageNo - 1) * pageSize);
            parameterIndex++;
            st.setInt(parameterIndex, pageSize);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adId = rs.getInt(1);
                String adLastName = rs.getString(2);
                String adFirstName = rs.getString(3);
                String adUsername = rs.getString(4);
                String adPassword = rs.getString(5);
                String adImage = rs.getString(6);
                boolean adGender = rs.getBoolean(7);
                Date adGoB = rs.getDate(8);
                String adPhone = rs.getString(9);
                String adEmail = rs.getString(10);
                String adAddress = rs.getString(11);
                int adStatus = rs.getInt(12);
                int adRoleID = rs.getInt(13);
                Admin admin = new Admin(adId, adLastName, adFirstName, adUsername, adPassword, adImage, adGender, adGoB,
                        adPhone, adEmail, adAddress, adStatus, adRoleID);
                listAdmin.add(admin);
            }
        } catch (Exception e) {
        }

        return listAdmin;
    }

    public int getTotalPage(int roleId, int status, String search, int pageSize) {
        String sql = "SELECT COUNT(*) FROM Admin";
        boolean whereAdded = false; // A flag to track whether "WHERE" has been added to the SQL query.
        if (roleId != -1 || status != -1 || !search.isEmpty()) {
            sql += " WHERE";
            if (roleId != -1) {
                sql += " adRoleId = ?";
                whereAdded = true;
            }
            if (status != -1) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " adStatus = ?";
                whereAdded = true;
            }
            if (!search.isEmpty()) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " (adLastName LIKE ? OR adFirstName LIKE ? OR adUsername LIKE ? OR adEmail LIKE ?)";
            }
        }

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1; // Start with the first parameter index
            if (roleId != -1) {
                st.setInt(parameterIndex, roleId);
                parameterIndex++;
            }
            if (status != -1) {
                st.setInt(parameterIndex, status);
                parameterIndex++;
            }
            if (!search.isEmpty()) {
                for (int i = 0; i < 4; i++) {
                    st.setString(parameterIndex, "%" + search + "%");
                    parameterIndex++;
                }
            }
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int totalRecord = rs.getInt(1);
                int totalPage = totalRecord / pageSize;
                if (totalRecord % pageSize != 0) {
                    totalPage++;
                }
                return totalPage;
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public String getRoleName(int roleID) {
        String roleName = null;
        String sql = "select roleName from ROLE where roleID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, roleID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                roleName = rs.getString("roleName");
            }
        } catch (Exception e) {
        }
        return roleName;
    }

    public void addNewAdmin(String adLastName, String adFirstName, String adEmail, String adUsername, String adPassword,
            int adRoleId) {
        String sql = "INSERT INTO [Admin]"
                + "values (?, ?, ?, ?, '', '','', '', ?, '', 1, ?)";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, adLastName);
            st.setString(2, adFirstName);
            st.setString(3, adUsername);
            st.setString(4, adPassword);
            st.setString(5, adEmail);
            st.setInt(6, adRoleId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void addNewAdmin(String adLastName, String adFirstName, String adEmail, String adPhone, String adUsername,
            String adPassword, int adRoleId, int adGender) {
        String sql = "INSERT INTO [SE1746_KS_SWP391_G6].[dbo].[Admin] "
                + "([adLastName], [adFirstName], [adUsername], [adPassword], [adEmail], [adPhone], [adRoleID], [adGender], [adDoB], [adStatus], [adAddress]) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1, '')";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, adLastName);
            st.setString(2, adFirstName);
            st.setString(3, adUsername);
            st.setString(4, adPassword);
            st.setString(5, adEmail);
            st.setString(6, adPhone);
            st.setInt(7, adRoleId);
            st.setInt(8, adGender);
            st.setDate(9, new java.sql.Date(new java.util.Date().getTime())); // set default value to today's date
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public int getRole(String identifier) {
        String sql = "select adRoleID from admin where (adUsername = ? or adEmail = ?)";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, identifier);
            st.setString(2, identifier);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adRoleID = rs.getInt(1);
                return adRoleID;
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public ArrayList<Admin> searchAdminByName(String name) {
        ArrayList<Admin> listAdmin = new ArrayList<>();
        String sql = "select * from Admin where adLastName LIKE ? Or adFirstName like ? OR CONCAT(adFirstName, ' ', adLastName) LIKE ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, "%" + name + "%");
            st.setString(2, "%" + name + "%");
            st.setString(3, "%" + name + "%");

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adId = rs.getInt(1);
                String adLastName = rs.getString(2);
                String adFirstName = rs.getString(3);
                String adUsername = rs.getString(4);
                String adPassword = rs.getString(5);
                String adImage = rs.getString(6);
                boolean adGender = rs.getBoolean(7);
                Date adGoB = rs.getDate(8);
                String adPhone = rs.getString(9);
                String adEmail = rs.getString(10);
                String adAddress = rs.getString(11);
                int adStatus = rs.getInt(12);
                int adRoleID = rs.getInt(13);
                Admin admin = new Admin(adId, adLastName, adFirstName, adUsername, adPassword, adImage, adGender, adGoB,
                        adPhone, adEmail, adAddress, adStatus, adRoleID);
                listAdmin.add(admin);
            }
        } catch (Exception e) {
        }
        return listAdmin;
    }

    public ArrayList<Admin> filterByRoleID(int roleID) {
        ArrayList<Admin> listAdmin = new ArrayList<>();
        String sql = "select * from Admin where adroleID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, roleID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adId = rs.getInt(1);
                String adLastName = rs.getString(2);
                String adFirstName = rs.getString(3);
                String adUsername = rs.getString(4);
                String adPassword = rs.getString(5);
                String adImage = rs.getString(6);
                boolean adGender = rs.getBoolean(7);
                Date adGoB = rs.getDate(8);
                String adPhone = rs.getString(9);
                String adEmail = rs.getString(10);
                String adAddress = rs.getString(11);
                int adStatus = rs.getInt(12);
                int adRoleID = rs.getInt(13);
                Admin admin = new Admin(adId, adLastName, adFirstName, adUsername, adPassword, adImage, adGender, adGoB,
                        adPhone, adEmail, adAddress, adStatus, adRoleID);
                listAdmin.add(admin);
            }
        } catch (Exception e) {
        }
        return listAdmin;
    }

    public void deleteAdmin(int adID) {
        String sql = "update Admin set adStatus = 0 where adID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, adID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void update(String firstName, String lastName, String email, int roleID, int adID) {
        String sql = "update Admin set adFirstName = ?,adLastName = ?, adEmail = ? , adRoleID = ? where adID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, firstName);
            st.setString(2, lastName);
            st.setString(3, email);
            st.setInt(4, roleID);
            st.setInt(5, adID);

            st.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public void updatePassword(String password, String adEmail) {
        String sql = "update Admin set adPassword = ? where adEmail = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, password);
            st.setString(2, adEmail);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateAdmin(Admin admin) {
        String sql = "update Admin set adFirstName = ?, adLastName = ?, adEmail = ?, adRoleID = ?, adPhone = ?, adAddress = ?, adDOB = ?, adStatus = ? where adID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, admin.getAdFirstName());
            st.setString(2, admin.getAdLastName());
            st.setString(3, admin.getAdEmail());
            st.setInt(4, admin.getAdRoleId());
            st.setString(5, admin.getAdPhone());
            st.setString(6, admin.getAdAddress());
            st.setDate(7, admin.getAdDob());
            st.setInt(8, admin.getAdStatus());
            st.setInt(9, admin.getAdId());

            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Admin getAdminByAdID(int adID) {
        String sql = "select * from admin where adID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, adID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int adId = rs.getInt(1);
                String adLastName = rs.getString(2);
                String adFirstName = rs.getString(3);
                String adUsername = rs.getString(4);
                String adPassword = rs.getString(5);
                String adImage = rs.getString(6);
                boolean adGender = rs.getBoolean(7);
                Date adGoB = rs.getDate(8);
                String adPhone = rs.getString(9);
                String adEmail = rs.getString(10);
                String adAddress = rs.getString(11);
                int adStatus = rs.getInt(12);
                int adRoleID = rs.getInt(13);
                Admin admin = new Admin(adId, adLastName, adFirstName, adUsername, adPassword, adImage, adGender, adGoB,
                        adPhone, adEmail, adAddress, adStatus, adRoleID);
                return admin;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public boolean updateStatusAdmin(int adID, int status) {
        String sql = "update Admin set adStatus = ? where adID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            System.out.println(">>" + status + adID);
            st.setInt(1, status);
            st.setInt(2, adID);
            st.executeUpdate();
            return true;
        } catch (Exception e) {
        }
        return false;
    }
    
    
    public void updateProfileAdmin(String adImage, String firstName, String lastName, String address, String username) {
        String sql = "update Admin set adImage = ?, adFirstName = ?, adLastName = ?, adAddress = ? where adUsername = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, adImage);
            st.setString(2, firstName);
            st.setString(3, lastName);
            st.setString(4, address);
            st.setString(5, username);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    
    
    
    

    public static void main(String[] args) {
        AdminDAO dao = new AdminDAO();
     dao.updateProfileAdmin("", "helo", "duong", "213", "duong1");
    }
}
