/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Category;
import model.Customer;

/**
 *
 * @author windy
 */
public class CategoryDAO extends DBConnect {

    public ArrayList<Category> getListCategory() {
        ArrayList<Category> listC = new ArrayList<>();
        String sql = "select * from Category where cateStatus = 1";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cateID = rs.getInt(1);
                String cateName = rs.getString(2);
                int cateStatus = rs.getInt(3);

                Category c = new Category(cateID, cateName, cateStatus);
                listC.add(c);
            }
        } catch (Exception e) {
            System.out.println("ListCategory: " + e.getMessage());
        }
        return listC;
    }
    
    public String getCateNameByCateID(String cateID) {
        String sql = "select cateName from Category where cateID = ?";
        String cateName = null;
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, cateID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                cateName = rs.getString("cateName");
            }

        } catch (Exception e) {
        }
        return cateName;
    }

    public static void main(String[] args) {
        ArrayList<Category> listC = new ArrayList<>();
        CategoryDAO d = new CategoryDAO();
        listC = d.getListCategory();
        for (Category c : listC) {
            System.out.println(c.toString());
        }
    }

}
