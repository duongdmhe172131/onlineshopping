/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Order;

/**
 *
 * @author Dell
 */
public class OrderDAO extends DBConnect {

    public ArrayList<Order> getOrder() {
        ArrayList<Order> listO = new ArrayList<>();
        String sql = "select * from [Order]";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int orderCusID = rs.getInt(2);
                Date orderDate = rs.getDate(3);
                double orderTotalPrice = rs.getDouble(4);
                String orderContactName = rs.getString(5);
                String orderPhone = rs.getString(6);
                String orderAddress = rs.getString(7);
                int orderSoID = rs.getInt(8);
                int orderAdID = rs.getInt(9);
                String orderNote = rs.getString(10);

                Order o = new Order(orderID, orderCusID, orderDate, orderTotalPrice,
                        orderContactName, orderPhone, orderAddress, orderSoID, orderAdID, orderNote);

                listO.add(o);
            }
        } catch (Exception e) {
            System.out.println("ListOrder: " + e.getMessage());
        }
        return listO;
    }
    
    public Order getOrderByIdForPurchase(String ordId) {
        
        String sql = "select * from [Order] WHERE orderID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, ordId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int orderCusID = rs.getInt(2);
                Date orderDate = rs.getDate(3);
                double orderTotalPrice = rs.getDouble(4);
                String orderContactName = rs.getString(5);
                String orderPhone = rs.getString(6);
                String orderAddress = rs.getString(7);
                int orderSoID = rs.getInt(8);
                int orderAdID = rs.getInt(9);
                String orderNote = rs.getString(11);

                Order o = new Order();
                
                o.setOrderID(orderID);
                o.setOrderTotalPrice(orderTotalPrice);
                o.setOrderNote(orderNote);

                return o;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<Order> getOrderByCusId(int cusId) {
        ArrayList<Order> listO = new ArrayList<>();
        String sql = "select * from [Order] WHERE orderCusID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, cusId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int orderCusID = rs.getInt(2);
                Date orderDate = rs.getDate(3);
                double orderTotalPrice = rs.getDouble(4);
                String orderContactName = rs.getString(5);
                String orderPhone = rs.getString(6);
                String orderAddress = rs.getString(7);
                int orderSoID = rs.getInt(8);
                int orderAdID = rs.getInt(9);
                String orderNote = rs.getString(11);

                Order o = new Order(orderID, orderCusID, orderDate, orderTotalPrice,
                        orderContactName, orderPhone, orderAddress, orderSoID, orderAdID, orderNote);

                listO.add(o);
            }
        } catch (Exception e) {
            System.out.println("ListProduct: " + e.getMessage());
        }
        return listO;
    }

    // Insert a new order into the database and return the generated orderID
    public int insertOrder(Order order) {
        int generatedOrderID = -1; // Initialize to a default value
        try {
            String insertQuery = "INSERT INTO [Order] (orderCusID, orderDate, orderTotalPrice, orderContactName, orderPhone, orderAddress, orderSoID, orderAdID, orderNote) "
                    + "OUTPUT INSERTED.orderID "
                    + // This line returns the generated orderID
                    "VALUES (?, ?, ?, ?, ?, ?, ?, NULL, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(insertQuery);

            preparedStatement.setInt(1, order.getOrderCusID());
            preparedStatement.setDate(2, new java.sql.Date(System.currentTimeMillis()));
            preparedStatement.setFloat(3, (float) order.getOrderTotalPrice());
            preparedStatement.setString(4, order.getOrderContactName());
            preparedStatement.setString(5, order.getOrderPhone());
            preparedStatement.setString(6, order.getOrderAddress());
            preparedStatement.setInt(7, order.getOrderSoID());
            preparedStatement.setString(8, order.getRawNote());

            ResultSet generatedKeys = preparedStatement.executeQuery();
            if (generatedKeys.next()) {
                generatedOrderID = generatedKeys.getInt("orderID");
            }
        } catch (SQLException e) {
            System.out.println("insert order:" + e);
        }
        return generatedOrderID;
    }

    public boolean updateOrderSoID(String orderID, int newSoID) {
        try {
            String updateQuery = "UPDATE [Order] SET orderSoID = ? WHERE orderID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(updateQuery);

            preparedStatement.setInt(1, newSoID);
            preparedStatement.setString(2, orderID);

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Update orderSoID failed: " + e.getMessage());
            return false;
        }
    }

    public static void main(String[] args) {
        ArrayList<Order> or = new ArrayList<>();
        OrderDAO dao = new OrderDAO();
        or = dao.getOrderByCusId(1);
        for (Order order : or) {
            System.out.println(or);
        }
    }

    public ArrayList<Order> getOrderByAdID(int adID) {
        ArrayList<Order> listO = new ArrayList<>();
        String sql = "select * from [Order] where orderAdID = ? and orderDate >= ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, adID);
            st.setDate(2, Date.valueOf("2023-06-10"));
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int orderCusID = rs.getInt(2);
                Date orderDate = rs.getDate(3);
                double orderTotalPrice = rs.getDouble(4);
                String orderContactName = rs.getString(5);
                String orderPhone = rs.getString(6);
                String orderAddress = rs.getString(7);
                int orderSoID = rs.getInt(8);
                int orderAdID = rs.getInt(9);
                String orderNote = rs.getString(10);

                Order o = new Order(orderID, orderCusID, orderDate, orderTotalPrice,
                        orderContactName, orderPhone, orderAddress, orderSoID, orderAdID, orderNote);

                listO.add(o);
            }
        } catch (Exception e) {
            System.out.println("ListOrderByAdID: " + e.getMessage());
        }
        return listO;
    }

    public Order getOrderByOrderID(String oID) {
        Order order = null;
        String sql = "select * from [Order] where orderID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, oID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int orderCusID = rs.getInt(2);
                Date orderDate = rs.getDate(3);
                double orderTotalPrice = rs.getDouble(4);
                String orderContactName = rs.getString(5);
                String orderPhone = rs.getString(6);
                String orderAddress = rs.getString(7);
                int orderSoID = rs.getInt(8);
                int orderAdID = rs.getInt(9);
                String orderNote = rs.getString(11);

                order = new Order(orderID, orderCusID, orderDate, orderTotalPrice,
                        orderContactName, orderPhone, orderAddress, orderSoID, orderAdID, orderNote);

            }
        } catch (Exception e) {
            System.out.println("getOrderByOrderID: " + e.getMessage());
        }
        return order;
    }

    public ArrayList<Order> getListOrderByFilterAndAdID(String search, Date startDate, Date endDate, int statusOrder, int pageNo, int pageSize, int adID) {
        ArrayList<Order> listO = new ArrayList<>();
        String sql = "SELECT * FROM [Order] where orderAdID = ? and orderDate >= ? and orderDate <= ?";

        if (statusOrder != -1) {
            sql += " AND";
            sql += " orderSoID = ?";
        }
        if (!search.isEmpty()) {
            sql += " AND";
            sql += " (orderContactName LIKE ?)";
        }

        sql += " ORDER BY orderID OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1; // Start with the first parameter index
            st.setInt(parameterIndex, adID);
            parameterIndex++;
            st.setDate(parameterIndex, startDate);
            parameterIndex++;
            st.setDate(parameterIndex, endDate);
            parameterIndex++;

            if (statusOrder != -1) {
                st.setInt(parameterIndex, statusOrder);
                parameterIndex++;
            }
            if (!search.isEmpty()) {
                st.setString(parameterIndex, "%" + search + "%");
                parameterIndex++;
            }
            // Set the limit and offset parameters for pagination
            st.setInt(parameterIndex, (pageNo - 1) * pageSize);
            parameterIndex++;
            st.setInt(parameterIndex, pageSize);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int orderCusID = rs.getInt(2);
                Date orderDate = rs.getDate(3);
                double orderTotalPrice = rs.getDouble(4);
                String orderContactName = rs.getString(5);
                String orderPhone = rs.getString(6);
                String orderAddress = rs.getString(7);
                int orderSoID = rs.getInt(8);
                int orderAdID = rs.getInt(9);
                String orderNote = rs.getString(10);

                Order o = new Order(orderID, orderCusID, orderDate, orderTotalPrice,
                        orderContactName, orderPhone, orderAddress, orderSoID, orderAdID, orderNote);

                listO.add(o);
            }
        } catch (Exception e) {
            System.out.println("getListOrderByFilterAndAdID: " + e.getMessage());
        }
        return listO;
    }

    public int getTotalPage(String search, Date startDate, Date endDate, int statusOrder, int pageSize, int adID) {
        String sql = "SELECT COUNT(*) FROM [Order] where orderAdID = ? and orderDate >= ? and orderDate <= ?";

        if (statusOrder != -1) {
            sql += " AND";
            sql += " orderSoID = ?";
        }
        if (!search.isEmpty()) {
            sql += " AND";
            sql += " (orderContactName LIKE ?)";
        }

        //sql += " ORDER BY orderID OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1; // Start with the first parameter index
            st.setInt(parameterIndex, adID);
            parameterIndex++;
            st.setDate(parameterIndex, startDate);
            parameterIndex++;
            st.setDate(parameterIndex, endDate);
            parameterIndex++;

            if (statusOrder != -1) {
                st.setInt(parameterIndex, statusOrder);
                parameterIndex++;
            }
            if (!search.isEmpty()) {
                st.setString(parameterIndex, "%" + search + "%");
                parameterIndex++;
            }
            // Set the limit and offset parameters for pagination
//            st.setInt(parameterIndex, (pageNo - 1) * pageSize);
//            parameterIndex++;
            //st.setInt(parameterIndex, pageSize);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int totalRecord = rs.getInt(1);
                int totalPage = totalRecord / pageSize;
                if (totalRecord % pageSize != 0) {
                    totalPage++;
                }
                return totalPage;
            }
        } catch (Exception e) {
            System.out.println("getTotalPage: " + e.getMessage());
        }
        return 0;
    }
    
    
        public ArrayList<Order> getListOrderNull() {
        ArrayList<Order> listO = new ArrayList<>();
        String sql = " select * from [Order] where orderAdID is null";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int orderCusID = rs.getInt(2);
                Date orderDate = rs.getDate(3);
                double orderTotalPrice = rs.getDouble(4);
                String orderContactName = rs.getString(5);
                String orderPhone = rs.getString(6);
                String orderAddress = rs.getString(7);
                int orderSoID = rs.getInt(8);
                int orderAdID = rs.getInt(9);
                String orderNote = rs.getString(10);

                Order o = new Order(orderID, orderCusID, orderDate, orderTotalPrice,
                        orderContactName, orderPhone, orderAddress, orderSoID, orderAdID, orderNote);

                listO.add(o);
            }
        } catch (Exception e) {
            System.out.println("getListOrderNull: " + e.getMessage());
        }
        return listO;
    }

    public boolean checkOrderAdIDNull(String orderID) {
        String sql = " select * from [Order] where orderAdID is null and orderID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, orderID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("checkOrderAdIDNull: " + e.getMessage());
        }
        return false;
    }

    public boolean updateOrderAdID(String orderID, int adID) {
        try {
            String updateQuery = "UPDATE [Order] SET orderAdID = ? WHERE orderID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(updateQuery);

            preparedStatement.setInt(1, adID);
            preparedStatement.setString(2, orderID);

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Update updateOrderAdID failed: " + e.getMessage());
            return false;
        }
    }
    public ArrayList<Order> getListOrderNullByFilter(int statusOrder, int pageNo, int pageSize) {
        ArrayList<Order> listO = new ArrayList<>();
        String sql = "select * from [Order] where orderAdID is null";

        if (statusOrder != -1) {
            sql += " AND";
            sql += " orderSoID = ?";
        }
        
        sql += " ORDER BY orderID desc OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1; // Start with the first parameter index

            if (statusOrder != -1) {
                st.setInt(parameterIndex, statusOrder);
                parameterIndex++;
            }
            // Set the limit and offset parameters for pagination
            st.setInt(parameterIndex, (pageNo - 1) * pageSize);
            parameterIndex++;
            st.setInt(parameterIndex, pageSize);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int orderCusID = rs.getInt(2);
                Date orderDate = rs.getDate(3);
                double orderTotalPrice = rs.getDouble(4);
                String orderContactName = rs.getString(5);
                String orderPhone = rs.getString(6);
                String orderAddress = rs.getString(7);
                int orderSoID = rs.getInt(8);
                int orderAdID = rs.getInt(9);
                String orderNote = rs.getString(10);

                Order o = new Order(orderID, orderCusID, orderDate, orderTotalPrice,
                        orderContactName, orderPhone, orderAddress, orderSoID, orderAdID, orderNote);

                listO.add(o);
            }
        } catch (Exception e) {
            System.out.println("getListOrderNullByFilter: " + e.getMessage());
        }
        return listO;
    }

    public int getTotalPage2(int statusOrder, int pageSize) {
        String sql = "SELECT COUNT(*) FROM [Order] where orderAdID is null";

        if (statusOrder != -1) {
            sql += " AND";
            sql += " orderSoID = ?";
        }

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1; // Start with the first parameter index

            if (statusOrder != -1) {
                st.setInt(parameterIndex, statusOrder);
                parameterIndex++;
            }
           
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int totalRecord = rs.getInt(1);
                int totalPage = totalRecord / pageSize;
                if (totalRecord % pageSize != 0) {
                    totalPage++;
                }
                return totalPage;
            }
        } catch (Exception e) {
            System.out.println("getTotalPage2: " + e.getMessage());
        }
        return 0;
    }
}
