/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Customer;

/**
 *
 * @author BEAN
 */
public class CustomerDAO extends DBConnect {

    public Customer login(String identifier, String password) {
        String sql = " select * from Customer\n"
                + "where (cusUsername = ? OR cusEmail = ?) and cusPassword = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, identifier);
            st.setString(2, identifier);
            st.setString(3, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cusId = rs.getInt(1);
                String cusName = rs.getString(2);
                String cusUsername = rs.getString(3);
                String cusPassword = rs.getString(4);
                String cusImage = rs.getString(5);
                boolean cusGender = rs.getBoolean(6);
                Date cusDob = rs.getDate(7);
                String cusPhone = rs.getString(8);
                String cusEmail = rs.getString(9);
                String cusAddress = rs.getString(10);
                int cusStatus = rs.getInt(11);
                
                Customer customer = new Customer(cusId, cusName, cusUsername, cusPassword, cusImage, cusGender, cusDob, cusPhone, cusEmail, cusAddress, cusStatus);
                
                return customer;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Customer checkCustomerExistByUsername(String username) {
        String sql = "select * from Customer where cusUsername = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cusId = rs.getInt(1);
                String cusName = rs.getString(2);
                String cusUsername = rs.getString(3);
                String cusPassword = rs.getString(4);
                String cusImage = rs.getString(5);
                boolean cusGender = rs.getBoolean(6);
                Date cusDob = rs.getDate(7);
                String cusPhone = rs.getString(8);
                String cusEmail = rs.getString(9);
                String cusAddress = rs.getString(10);
                int cusStatus = rs.getInt(11);
                Customer customer = new Customer(cusId, cusName, cusUsername, cusPassword, cusImage, cusGender, cusDob, cusPhone, cusEmail, cusAddress, cusStatus);
                return customer;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Customer checkCustomerExistByEmail(String email) {
        String sql = "select * from Customer where cusEmail = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cusId = rs.getInt(1);
                String cusName = rs.getString(2);
                String cusUsername = rs.getString(3);
                String cusPassword = rs.getString(4);
                String cusImage = rs.getString(5);
                boolean cusGender = rs.getBoolean(6);
                Date cusDob = rs.getDate(7);
                String cusPhone = rs.getString(8);
                String cusEmail = rs.getString(9);
                String cusAddress = rs.getString(10);
                int cusStatus = rs.getInt(11);
                Customer customer = new Customer(cusId, cusName, cusUsername, cusPassword, cusImage, cusGender, cusDob, cusPhone, cusEmail, cusAddress, cusStatus);
                return customer;
            }
        } catch (Exception e) {
        }
        return null;
    }
    
      public List<Customer> getListCustomerByFilter(int status, String search, int pageNo, int pageSize) {
        List<Customer> listCustomer = new ArrayList<>();
        String sql = "SELECT * FROM Customer";
        boolean whereAdded = false;

        if (status != -1 || !search.isEmpty()) {
            sql += " WHERE";
            if (status != -1) {
                sql += " cusStatus = ?";
                whereAdded = true;
            }
            if (!search.isEmpty()) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " (cusContactName LIKE ? OR cusUsername LIKE ? OR cusEmail LIKE ? OR cusAddress LIKE ?)";
            }
        }

        sql += " ORDER BY cusID OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1;

            if (status != -1) {
                st.setInt(parameterIndex, status);
                parameterIndex++;
            }

            if (!search.isEmpty()) {
                for (int i = 0; i < 4; i++) {
                    st.setString(parameterIndex, "%" + search + "%");
                    parameterIndex++;
                }
            }

            // Set the limit and offset parameters for pagination
            st.setInt(parameterIndex, (pageNo - 1) * pageSize);
            parameterIndex++;
            st.setInt(parameterIndex, pageSize);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cusID = rs.getInt(1);
                String cusContactName = rs.getString(2);
                String cusUsername = rs.getString(3);
                String cusPassword = rs.getString(4);
                String cusImage = rs.getString(5);
                boolean cusGender = rs.getBoolean(6);
                Date cusDoB = rs.getDate(7);
                String cusPhone = rs.getString(8);
                String cusEmail = rs.getString(9);
                String cusAddress = rs.getString(10);
                int cusStatus = rs.getInt(11);

                Customer customer = new Customer();
                customer.setCusId(cusID);
                customer.setCusContactName(cusContactName);
                customer.setCusUsername(cusUsername);
                customer.setCusPassword(cusPassword);
                customer.setCusImage(cusImage);
                customer.setCusGender(cusGender);
                customer.setCusDob(cusDoB);
                customer.setCusPhone(cusPhone);
                customer.setCusEmail(cusEmail);
                customer.setCusAddress(cusAddress);
                customer.setCusStatus(cusStatus);

                listCustomer.add(customer);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return listCustomer;
    }

    public int getTotalPage(int status, String search, int pageSize) {
        String sql = "SELECT COUNT(*) FROM [SE1746_KS_SWP391_G6].[dbo].[Customer]";
        boolean whereAdded = false;

        if (status != -1 || !search.isEmpty()) {
            sql += " WHERE";
            if (status != -1) {
                sql += " cusStatus = ?";
                whereAdded = true;
            }
            if (!search.isEmpty()) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " (cusContactName LIKE ? OR cusUsername LIKE ? OR cusEmail LIKE ? OR cusAddress LIKE ?)";
            }
        }

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1;

            if (status != -1) {
                st.setInt(parameterIndex, status);
                parameterIndex++;
            }

            if (!search.isEmpty()) {
                for (int i = 0; i < 4; i++) {
                    st.setString(parameterIndex, "%" + search + "%");
                    parameterIndex++;
                }
            }

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int totalRecord = rs.getInt(1);
                int totalPage = totalRecord / pageSize;
                if (totalRecord % pageSize != 0) {
                    totalPage++;
                }
                return totalPage;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return 0;
    }
    
    public void updateCustomer(Customer customer) {
        String sql = "UPDATE Customer SET cusContactName = ?, cusDob = ?, cusAddress = ?, cusEmail = ?, cusPhone = ?, cusStatus = ? WHERE cusID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, customer.getCusContactName());
            st.setDate(2, customer.getCusDob());
            st.setString(3, customer.getCusAddress());
            st.setString(4, customer.getCusEmail());
            st.setString(5, customer.getCusPhone());
            st.setInt(6, customer.getCusStatus());
            st.setInt(7, customer.getCusId());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void updateStatus(int cusID, int status) {
        String sql = "UPDATE customer SET cusStatus = ? WHERE cusID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, cusID);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("UpdateStatus: " + e.getMessage());
        }
    }

    public Customer checkCustomerExistByPhone(String phone) {
        String sql = "select * from Customer where cusPhone = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, phone);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cusId = rs.getInt(1);
                String cusName = rs.getString(2);
                String cusUsername = rs.getString(3);
                String cusPassword = rs.getString(4);
                String cusImage = rs.getString(5);
                boolean cusGender = rs.getBoolean(6);
                Date cusDob = rs.getDate(7);
                String cusPhone = rs.getString(8);
                String cusEmail = rs.getString(9);
                String cusAddress = rs.getString(10);
                int cusStatus = rs.getInt(11);
                Customer customer = new Customer(cusId, cusName, cusUsername, cusPassword, cusImage, cusGender, cusDob, cusPhone, cusEmail, cusAddress, cusStatus);
                return customer;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void register(String cusName, String email, String phone, String cusUsername, String cusPassword) {
        String sql = "INSERT INTO [dbo].[Customer] ([cusContactName], [cusUsername], [cusPassword], [cusEmail], [cusPhone], [cusStatus])\n"
                + "VALUES (?, ?, ?, ?, ?, 0);";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, cusName);
            st.setString(2, cusUsername);
            st.setString(3, cusPassword);
            st.setString(4, email);
            st.setString(5, phone);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("register: " + e);
        }
    }

    public Customer verifyEmail(String email) {
        String sql = "Select * from Customer where cusEmail = ? and cusStatus = 0";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cusId = rs.getInt(1);
                String cusName = rs.getString(2);
                String cusUsername = rs.getString(3);
                String cusPassword = rs.getString(4);
                String cusImage = rs.getString(5);
                boolean cusGender = rs.getBoolean(6);
                Date cusDob = rs.getDate(7);
                String cusPhone = rs.getString(8);
                String cusEmail = rs.getString(9);
                String cusAddress = rs.getString(10);
                int cusStatus = rs.getInt(11);
                Customer customer = new Customer(cusId, cusName, cusUsername, cusPassword, cusImage, cusGender, cusDob, cusPhone, cusEmail, cusAddress, cusStatus);
                return customer;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void updateEmailVerificationStatus(String email, int status) {
        String sql = "Update Customer set cusStatus = ? where cusEmail = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, status);
            st.setString(2, email);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void changePassword(String email, String password) {
        String sql = "UPDATE customer SET cusPassword = ? WHERE cusEmail = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, password);
            st.setString(2, email);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }

    public void updateAddress(String address, int cusID) {
        String sql = "UPDATE customer SET cusAddress = ? WHERE cusID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, address);
            st.setInt(2, cusID);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("UpdateAddress: " + e.getMessage());
        }
    }

    public void updatePhone(String phone, int cusID) {
        String sql = "UPDATE customer SET cusPhone = ? WHERE cusID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, phone);
            st.setInt(2, cusID);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("UpdatePhone: " + e.getMessage());
        }
    }

    public void updateCusName(String cusName, int cusID) {
        String sql = "UPDATE customer SET cusContactName = ? WHERE cusID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, cusName);
            st.setInt(2, cusID);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("UpdateCusName: " + e.getMessage());
        }
    }

    public void updateImage(String image, int cusID) {
        String sql = "UPDATE customer SET cusImage = ? WHERE cusID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, image);
            st.setInt(2, cusID);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("UpdateImage: " + e.getMessage());
        }
    }

    public String[] cutAddress(String address) {
        String[] cuttedAddress = new String[2];
        if (address!=null)
            cuttedAddress = address.split("\\.");
        

        return cuttedAddress;
    }

    public static void main(String[] args) {
//        Customer cus = new Customer();
//        CustomerDAO dao = new CustomerDAO();
//        dao.changePassword("duongdmhe172131@fpt.edu.vn", "123");
        CustomerDAO daoCustomer = new CustomerDAO();
        String[] address = daoCustomer.cutAddress("to 3 phuong chieng le.son la.son la");
        System.out.println(address[1]);
    }

     public Customer getCustomerByID(String cusID) {
        Customer c = new Customer();
        String sql = "select * from Customer where cusID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, cusID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cusId = rs.getInt(1);
                String cusName = rs.getString(2);
                String cusUsername = rs.getString(3);
                String cusPassword = rs.getString(4);
                String cusImage = rs.getString(5);
                boolean cusGender = rs.getBoolean(6);
                Date cusDob = rs.getDate(7);
                String cusPhone = rs.getString(8);
                String cusEmail = rs.getString(9);
                String cusAddress = rs.getString(10);
                int cusStatus = rs.getInt(11);
                c.setCusId(cusId);
                c.setCusContactName(cusName);
                c.setCusUsername(cusUsername);
                c.setCusPassword(cusPassword);
                c.setCusImage(cusImage);
                c.setCusGender(cusGender);
                c.setCusDob(cusDob);
                c.setCusPhone(cusPhone);
                c.setCusEmail(cusEmail);
                c.setCusAddress(cusAddress);
                c.setCusStatus(cusStatus);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return c;
    }

    public ArrayList<Customer> getListCustomer() {
        ArrayList<Customer> listC = new ArrayList<>();
        String sql = "select * from Customer";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int cusId = rs.getInt(1);
                String cusName = rs.getString(2);
                String cusUsername = rs.getString(3);
                String cusPassword = rs.getString(4);
                String cusImage = rs.getString(5);
                boolean cusGender = rs.getBoolean(6);
                Date cusDob = rs.getDate(7);
                String cusPhone = rs.getString(8);
                String cusEmail = rs.getString(9);
                String cusAddress = rs.getString(10);
                int cusStatus = rs.getInt(11);

                Customer c = new Customer(cusId, cusUsername, cusUsername, cusPassword, cusImage, cusGender, cusDob, cusPhone, cusEmail, cusAddress, cusStatus);
                listC.add(c);
            }
        } catch (Exception e) {
            System.out.println("ListCustomer: " + e.getMessage());
        }
        return listC;
    }
}
