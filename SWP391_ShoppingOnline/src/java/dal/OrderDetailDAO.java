/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.OrderDetail;
import model.Product;
import model.ProductDetail;

/**
 *
 * @author Dell
 */
public class OrderDetailDAO extends DBConnect {

    public OrderDetail getOrderDetailtByOrderID(String orderID) {
        String sql = "SELECT * FROM OrderDetail WHERE odOrderID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, orderID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int odID = rs.getInt(1);
                int odOrderID = rs.getInt(2);
                int odPdID = rs.getInt(3);
                int odQuantity = rs.getInt(4);
                double odPrice = rs.getDouble(5);

                OrderDetail od = new OrderDetail(odID, odOrderID, odPdID, odQuantity, odPrice);
                return od;
            }
        } catch (Exception e) {
            System.out.println("OrderDetail: " + e.getMessage());
        }
        return null;
    }
    
    public List<Product> getProductByOrderDetailByOrderID(String orderID) {
        String sql = "SELECT * FROM OrderDetail WHERE odOrderID = ?";
        List<Product> productList = new ArrayList<>();
        
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, orderID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int odID = rs.getInt(1);
                int odOrderID = rs.getInt(2);
                String odPdID = rs.getString(3);

                ProductDetail pd = new ProductDetailDAO().getProductDetailbyPdID(odPdID);
                productList.add(new ProductDAO().getProductbyProID(pd.getPdProID()+""));
            }
        } catch (Exception e) {
            System.out.println("OrderDetail: " + e.getMessage());
        }
        System.out.println(productList);
        return productList;
    }

    public void insertOrderDetail(int odOrderID, int odPdID, int odQuantity, double odPrice) {
        try {
            String insertQuery = "INSERT INTO OrderDetail (odOrderID, odPdID, odQuantity, odPrice) VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(insertQuery);
            preparedStatement.setInt(1, odOrderID);
            preparedStatement.setInt(2, odPdID);
            preparedStatement.setInt(3, odQuantity);
            preparedStatement.setDouble(4, odPrice);
            int rowsInserted = preparedStatement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Order detail inserted successfully.");
            } else {
                System.out.println("Failed to insert order detail.");
            }
        } catch (SQLException ex) {
            System.out.println("SQL Error: " + ex.getMessage());
        }
    }

}
