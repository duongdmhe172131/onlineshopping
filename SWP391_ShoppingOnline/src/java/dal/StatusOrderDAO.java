package dal;

import context.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.StatusOrder;

public class StatusOrderDAO extends DBConnect {

    public StatusOrderDAO() {
        super(); // Call the constructor of the parent class (DBConnect)
    }

    // Insert a new status order into the database
    public void insertStatusOrder(StatusOrder statusOrder) {
        try {
            String insertQuery = "INSERT INTO StatusOrder (soName, soStatus) VALUES (?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(insertQuery);
            preparedStatement.setString(1, statusOrder.getSoName());
            preparedStatement.setInt(2, statusOrder.getSoStatus());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(StatusOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Retrieve a status order by its ID
    public StatusOrder getStatusOrderById(int soID) {
        StatusOrder statusOrder = null;
        try {
            String selectQuery = "SELECT soID, soName, soStatus FROM StatusOrder WHERE soID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            preparedStatement.setInt(1, soID);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String soName = resultSet.getString("soName");
                int soStatus = resultSet.getInt("soStatus");

                statusOrder = new StatusOrder(soID, soName, soStatus);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StatusOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return statusOrder;
    }

    // Retrieve all status orders from the database
    public List<StatusOrder> getAllStatusOrders() {
        List<StatusOrder> statusOrders = new ArrayList<>();
        try {
            String selectQuery = "SELECT soID, soName, soStatus FROM StatusOrder";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int soID = resultSet.getInt("soID");
                String soName = resultSet.getString("soName");
                int soStatus = resultSet.getInt("soStatus");

                StatusOrder statusOrder = new StatusOrder(soID, soName, soStatus);
                statusOrders.add(statusOrder);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StatusOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return statusOrders;
    }

    // Update a status order in the database
    public void updateStatusOrder(StatusOrder statusOrder) {
        try {
            String updateQuery = "UPDATE StatusOrder SET soName = ?, soStatus = ? WHERE soID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(updateQuery);
            preparedStatement.setString(1, statusOrder.getSoName());
            preparedStatement.setInt(2, statusOrder.getSoStatus());
            preparedStatement.setInt(3, statusOrder.getSoID());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(StatusOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Delete a status order from the database
    public void deleteStatusOrder(int soID) {
        try {
            String deleteQuery = "DELETE FROM StatusOrder WHERE soID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(deleteQuery);
            preparedStatement.setInt(1, soID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(StatusOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<StatusOrder> getStatusOrderForSale() {
        List<StatusOrder> statusOrders = new ArrayList<>();
        try {
            String selectQuery = "select * from StatusOrder where soName <> 'Pending' and soStatus = 1";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int soID = resultSet.getInt("soID");
                String soName = resultSet.getString("soName");
                int soStatus = resultSet.getInt("soStatus");

                StatusOrder statusOrder = new StatusOrder(soID, soName, soStatus);
                statusOrders.add(statusOrder);
            }
        } catch (Exception e) {
            System.out.println("getStatusOrderForSale: " + e.getMessage());
        }
        return statusOrders;
    }
}

