/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import static java.util.Comparator.comparing;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import model.Category;
import model.Item;
import model.Product;

/**
 *
 * @author windy
 */
public class ProductDAO extends DBConnect {

    public ArrayList<Product> getListProduct() {
        ArrayList<Product> listP = new ArrayList<>();
        String sql = "select * from Product";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = rs.getDate(7);
                int proAdID = rs.getInt(8);
                int proStatus = rs.getInt(9);
                Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, proStatus);

                listP.add(p);
            }
        } catch (Exception e) {
            System.out.println("ListProduct: " + e.getMessage());
        }
        return listP;
    }

    public ArrayList<Product> getListProductByCategory(String cateID) {
        ArrayList<Product> listP = new ArrayList<>();
        String sql = "select * from Product where proCateID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, cateID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                int proStatus = rs.getInt(7);

                Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proStatus);

                listP.add(p);
            }
        } catch (Exception e) {
            System.out.println("ListProductByCate: " + e.getMessage());
        }
        return listP;
    }

    public ArrayList<Product> searchProduct(String productName, int currentPage, int itemsPerPage) {
        ArrayList<Product> listP = new ArrayList<>();
        String sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product where proName like '%" + productName + "%'"
                + "order by proID\n"
                + "OFFSET ? rows\n"
                + "fetch next ? rows only";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, (currentPage - 1) * itemsPerPage);
            st.setInt(2, itemsPerPage);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                int proStatus = rs.getInt(7);

                Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proStatus);

                listP.add(p);
            }
        } catch (Exception e) {
            System.out.println("searchProduct: " + e.getMessage());
        }
        return listP;
    }

    public Product getProductbyProID(String id) {
        Product pro = null;
        String sql = "select * from Product where proID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = rs.getDate(7);
                int proAdID = rs.getInt(8);
                int proStatus = rs.getInt(9);
                pro = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, proStatus);
            }
        } catch (Exception e) {
            System.out.println("getProductbyProID: " + e.getMessage());
        }
        return pro;
    }

    public ArrayList<Product> sortProduct(String cateID, String productName, String sortOpt, int currentPage, int itemsPerPage) {
        ArrayList<Product> listPro = new ArrayList<>();
        String sql = "";
        if (!(cateID == null || cateID.isEmpty())) {
            switch (sortOpt) {
                case "HighToLow":
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "where proCateID = ?\n"
                            + "order by proPrice desc\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
                    break;
                case "LowToHigh":
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "where proCateID = ?\n"
                            + "order by proPrice\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
                    break;
                default:
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "where proCateID = ?\n"
                            + "order by proID\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
            }
            try {
                PreparedStatement st = conn.prepareStatement(sql);
                st.setString(1, cateID);
                st.setInt(2, (currentPage - 1) * itemsPerPage);
                st.setInt(3, itemsPerPage);
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    int proID = rs.getInt(1);
                    int proCateID = rs.getInt(2);
                    String proName = rs.getString(3);
                    float proPrice = rs.getFloat(4);
                    String proImage = rs.getString(5);
                    String proDetail = rs.getString(6);
                    int proStatus = rs.getInt(7);

                    Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proStatus);

                    listPro.add(p);
                }
            } catch (Exception e) {
                System.out.println("sortProduct: " + e.getMessage());
            }
        } else if (!(productName == null || productName.isEmpty())) {
            switch (sortOpt) {
                case "HighToLow":
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "where proName like '%" + productName + "%'\n"
                            + "order by proPrice desc\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
                    break;
                case "LowToHigh":
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "where proName like '%" + productName + "%'\n"
                            + "order by proPrice\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
                    break;
                default:
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "where proName like '%" + productName + "%'\n"
                            + "order by proID\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
            }
            try {
                PreparedStatement st = conn.prepareStatement(sql);
                st.setInt(1, (currentPage - 1) * itemsPerPage);
                st.setInt(2, itemsPerPage);
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    int proID = rs.getInt(1);
                    int proCateID = rs.getInt(2);
                    String proName = rs.getString(3);
                    float proPrice = rs.getFloat(4);
                    String proImage = rs.getString(5);
                    String proDetail = rs.getString(6);
                    int proStatus = rs.getInt(7);

                    Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proStatus);

                    listPro.add(p);
                }
            } catch (Exception e) {
                System.out.println("sortProduct: " + e.getMessage());
            }
        } else {
            switch (sortOpt) {
                case "HighToLow":
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "order by proPrice desc\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
                    break;
                case "LowToHigh":
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "order by proPrice\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
                    break;
                default:
                    sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product \n"
                            + "order by proID\n"
                            + "OFFSET ? rows\n"
                            + "fetch next ? rows only";
            }
            try {
                PreparedStatement st = conn.prepareStatement(sql);
                st.setInt(1, (currentPage - 1) * itemsPerPage);
                st.setInt(2, itemsPerPage);
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    int proID = rs.getInt(1);
                    int proCateID = rs.getInt(2);
                    String proName = rs.getString(3);
                    float proPrice = rs.getFloat(4);
                    String proImage = rs.getString(5);
                    String proDetail = rs.getString(6);
                    int proStatus = rs.getInt(7);

                    Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proStatus);

                    listPro.add(p);
                }
            } catch (Exception e) {
                System.out.println("sortProduct: " + e.getMessage());
            }
        }

        return listPro;
    }

    public int getNumOfProduct(String option, String value) {
        String sql = "";

        System.out.println("option: " + option);
        if (option.equals("all")) {
            sql = "select count(*) from Product where proStatus = 1";
            try {
                PreparedStatement st = conn.prepareStatement(sql);
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    int numOfPro = rs.getInt(1);
                    return numOfPro;
                }
            } catch (Exception e) {
                System.out.println("getNumOfProduct: " + e.getMessage());
            }
        } else if (option.equals("category")) {
            sql = "select count(*) from Product where proCateID = ?";
            try {
                PreparedStatement st = conn.prepareStatement(sql);
                st.setString(1, value);
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    int numOfPro = rs.getInt(1);
                    return numOfPro;
                }
            } catch (Exception e) {
                System.out.println("getNumOfProduct: " + e.getMessage());
            }
        } else if (option.equals("search")) {
            sql = "select count(*) from Product where proName like '%" + value + "%'";
            try {
                PreparedStatement st = conn.prepareStatement(sql);
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    int numOfPro = rs.getInt(1);
                    return numOfPro;
                }
            } catch (Exception e) {
                System.out.println("getNumOfProduct: " + e.getMessage());
            }
        }

        return 0;
    }

    public ArrayList<Product> getItemsPerPage(int currentPage, int itemsPerPage) {
        ArrayList<Product> listP = new ArrayList<>();
        String sql = "select * from Product where proStatus = 1\n"
                + "order by proID\n"
                + "OFFSET ? rows\n"
                + "fetch next ? rows only";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, (currentPage - 1) * itemsPerPage);
            st.setInt(2, itemsPerPage);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                int proStatus = rs.getInt(9);

                Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proStatus);
                p.setAverageStar(getAverageStar(proID));

                listP.add(p);
            }
        } catch (Exception e) {
            System.out.println("getItemsPerPage: " + e.getMessage());
        }
        return listP;
    }

    public int getAverageStar(int proId) {

        String sql = "select (SUM(f.fbStar) / COUNT(*)) from Feedback f, ProductDetail pd\n"
                + "where f.fbProID = pd.pdID and pd.pdID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, (proId));

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int star = rs.getInt(1);
                return star > 0 ? star : 5;
            }
        } catch (Exception e) {
            System.out.println("get avg star: " + e.getMessage());
        }

        return 5;

    }

    public ArrayList<Product> getItemsPerPageByStatus(int currentPage, int itemsPerPage, int proStatus) {
        ArrayList<Product> listP = new ArrayList<>();
        String sql = "select * from Product where proStatus = ? "
                + "order by proID "
                + "OFFSET ? rows "
                + "fetch next ? rows only";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, proStatus);
            st.setInt(2, (currentPage - 1) * itemsPerPage);
            st.setInt(3, itemsPerPage);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = rs.getDate(7);
                int proAdID = rs.getInt(8);
                int prStatus = rs.getInt(9);
                Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, prStatus);

                listP.add(p);
            }
        } catch (Exception e) {
            System.out.println("getItemsPerPageByCategory: " + e.getMessage());

        }
        return listP;

    }

    public ArrayList<Product> getItemsPerPageByCategory(String cateID, int currentPage, int itemsPerPage) {
        ArrayList<Product> listP = new ArrayList<>();
        String sql = "select proID, proCateID, proName, proPrice, proImage, proDetail, proStatus from Product where proCateID = ? and proStatus = 1\n"
                + "order by proID\n"
                + "OFFSET ? rows\n"
                + "fetch next ? rows only";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, cateID);
            st.setInt(2, (currentPage - 1) * itemsPerPage);
            st.setInt(3, itemsPerPage);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                int proStatus = rs.getInt(7);

                Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proStatus);
                p.setAverageStar(getAverageStar(proID));

                listP.add(p);
            }
        } catch (Exception e) {
            System.out.println("getItemsPerPageByCategory: " + e.getMessage());
        }
        return listP;
    }

    public ArrayList<Item> getListItem(String txt, ArrayList<Item> listI) {
        System.out.println(txt);
        String[] listStr = txt.split("/");
        for (String str : listStr) {
            String[] item = str.split(":");
            String pdID = item[0];
            Item i = getItemByPdID(pdID);
            i.setQuantity(Integer.parseInt(item[1]));
            listI.add(i);
            //addItem(i, listI);
        }
        return listI;
    }

    private Item getItemByPdID(String opdID) {
        Item i = null;
        String sql = "select * from Product inner join ProductDetail on proID=pdProID where pdID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, opdID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int pdID = rs.getInt(10);
                int pdProID = rs.getInt(1);
                String proName = rs.getString(3);
                String proImage = rs.getString(5);
                float proPrice = rs.getFloat(4);
                String pdColor = rs.getString(12);
                int quantity = 1;
                i = new Item(pdID, pdProID, proName, proImage, proPrice, pdColor, quantity);
            }
        } catch (Exception e) {
            System.out.println("getItemByPdID: " + e.getMessage());
        }
        return i;
    }

    private void addItem(Item i, ArrayList<Item> listI) {
        boolean isDuplicated = false;
        if (listI.isEmpty()) {
            listI.add(i);
        } else {
            for (int j = 0; j < listI.size(); j++) {
                if (listI.get(j).getPdID() == i.getPdID()) {
                    isDuplicated = true;
                    System.out.println("bi trugn ne: " + listI.get(j).getQuantity());
                    int quantity = listI.get(j).getQuantity() + 1;
                    listI.get(j).setQuantity(quantity);
                }
            }
            if (isDuplicated == false) {
                listI.add(i);
            }
        }
    }
    public int getAvailableByPdID(String pdID) {
        String sql = "select pdAvailable from Product p inner join ProductDetail pd \n"
                + "  on p.proID = pd.pdProID \n"
                + "  where pd.pdID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, pdID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int available = rs.getInt(1);
                return available;
            }
        } catch (Exception e) {
            System.out.println("getAvailableByPdID: " + e.getMessage());
        }
        return 0;
    }

    public String addItemToCookie(String txt, String pdID) {
       int available = getAvailableByPdID(pdID);
        boolean isDuplicated = false;
        String[] listStr = txt.split("/");
        if (txt.isEmpty()) {
            txt += pdID + ":" + 1;
        } else {
            for (String str : listStr) {
                String[] item = str.split(":");
                if (item[0].equals(pdID)) {
                    isDuplicated = true;
                    int quantity = Integer.parseInt(item[1]) + 1;
                    if(((quantity-1) < available) && ((quantity-1) < 3)){
                        txt = txt.replaceAll(item[0] + ":" + item[1], item[0] + ":" + String.valueOf(quantity));
                    }
                    
                    //return txt;
                }
            }
            if (isDuplicated == false) {
                txt += "/" + pdID + ":" + 1;
            }
        }
        return txt;
    }

    public String updateQuantity(String txt, String pdID, String action) {
       int available = getAvailableByPdID(pdID);
        HashMap<String, String> listItem = new LinkedHashMap<>();
        String[] listStr = txt.split("/");
        for (String str : listStr) {
            String[] item = str.split(":");
            listItem.put(item[0], item[1]);
        }
        for (String i : listItem.keySet()) {
            if (i.equals(pdID)) {
                int quantity = Integer.parseInt(listItem.get(i));
                switch (action) {
                    case "+":
                        if (quantity < available) {
                            if (quantity < 3) {
                                quantity = quantity + 1;
                            }
                        }

                        break;
                    case "-":
                        if (quantity > 1) {
                            quantity = quantity - 1;
                        }

                }
                listItem.replace(i, String.valueOf(quantity));
                break;
            }
        }
        txt = "";
        String lastEntry = "";
        List<String> list = new ArrayList<>(listItem.keySet());
        if (list.size() > 0 && !list.isEmpty()) {
            // Get last entry in HashMap
            lastEntry = list.get(list.size() - 1);
        }
        for (String it : listItem.keySet()) {
            //int size = listItem.size();
            if (it.equals(lastEntry)) {
                txt += it + ":" + listItem.get(it);
            } else {
                txt += it + ":" + listItem.get(it) + "/";
            }
        }
        return txt;
    }

    public String deleteProduct(String txt, String pdID) {
        if (!txt.contains("/")) {
            txt = "";
        } else {
            String[] listStr = txt.split("/");
            for (String str : listStr) {
                System.out.println(str);
                String[] item = str.split(":");

                if (item[0].equals(pdID)) {
                    if (txt.indexOf(item[0]) == 0) {
                        txt = txt.replace(item[0] + ":" + item[1] + "/", "");
                        return txt;
                    } else {
                        System.out.println("vao day a");
                        txt = txt.replace("/" + item[0] + ":" + item[1], "");
                        return txt;
                    }

                }
            }
        }

        return txt;
    }

    public void updateProduct(int proCateID, String proName, float proPrice, String proImage, String proDetail, Date proCreateDate,
            int proAdID, int proStatus, int proID) {
        String sql = "UPDATE [dbo].[Product]\n"
                + "   SET [proCateID] = ?\n"
                + "      ,[proName] = ?\n"
                + "      ,[proPrice] = ?\n"
                + "      ,[proImage] = ?\n"
                + "      ,[proDetail] = ?\n"
                + "      ,[proCreateDate] = ?\n"
                + "      ,[proAdID] = ?\n"
                + "      ,[proStatus] = ?\n"
                + " WHERE proID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, proCateID);
            st.setString(2, proName);
            st.setFloat(3, proPrice);
            st.setString(4, proImage);
            st.setString(5, proDetail);
            st.setDate(6, proCreateDate);
            st.setInt(7, proAdID);
            st.setInt(8, proStatus);
            st.setInt(9, proID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void addNewPoduct(int proCateID, String proName, float proPrice, String proImage, String proDetail, int proAdID, int proStatus) {
        String sql = "INSERT INTO [dbo].[Product]\n"
                + "           ([proCateID]\n"
                + "           ,[proName]\n"
                + "           ,[proPrice]\n"
                + "           ,[proImage]\n"
                + "           ,[proDetail]\n"
                + "           ,[proCreateDate]\n"
                + "           ,[proAdID]\n"
                + "           ,[proStatus])\n"
                + "     VALUES (?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, proCateID);
            st.setString(2, proName);
            st.setFloat(3, proPrice);
            st.setString(4, proImage);
            st.setString(5, proDetail);
            st.setDate(6, new java.sql.Date(new java.util.Date().getTime())); // set default value to today's date
            st.setInt(7, proAdID);
            st.setInt(8, proStatus);
            st.executeUpdate();
        } catch (Exception e) {
        }

    }

    public void updateStatusProduct(int status, int productID) {
        String sql = "update product set proStatus = ? where proID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, productID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

//    public static void main(String[] args) {
//        ProductDAO d = new ProductDAO();
////        int num = d.getNumOfProduct("search", "256");
////        System.out.println("nm: " + num);
////        ArrayList<Product> list = d.sortProduct("1", "", "Default", 2, 2);
////        for (Product product : list) {
////            System.out.println(product.toString());
////        }
////        ArrayList<Item> listI = new ArrayList<>();
////        listI = d.getListItem("1:1/9:1/31:1/1:1/5:1/5:1", listI);
////        for (Item i : listI) {
////            System.out.println(i.toString());
////        }
//////        String ne = "51:1/1:51/16:1/33:3";
//////        System.out.println("index: " + ne.indexOf("1"));
////        String str = d.deleteProduct("5:1", "5");
////        System.out.println("String cua m ne: " + str);
//
//        d.updateStatusProduct(0, 1);
//    }
    public ArrayList<Item> getListItemByOrderID(String orderID) {
        ArrayList<Item> listI = new ArrayList<>();
       String sql = "select t.orderID, t.odID,\n"
                + "  t2.pdID, t2.proName, \n"
                + "  t2.proImage, t.odPrice, \n"
                + "  t2.pdColor, t.odQuantity, \n"
                + "  t2.pdimgURL from \n"
                + "  (select o.orderID, od.odID, od.odPdID, od.odQuantity, od.odPrice from [Order] o\n"
                + "  inner join OrderDetail od  on o.orderID = od.odOrderID where o.orderID = ?) t \n"
                + "  inner join\n"
                + "  (select p1.pdID, p1.proName, p1.pdColor, p1.proImage, pdi.pdimgURL from\n"
                + "  (select pd.pdID, proName,pd.pdColor, p.proImage from Product p  \n"
                + "  inner join ProductDetail pd on p.proID = pd.pdProID) p1\n"
                + "  inner join ProductDetailImage pdi\n"
                + "  on p1.pdID = pdi.pdimgPdID)  t2 on t.odPdID = t2.pdID";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, orderID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                String proName = rs.getString(4);
                String proImage = rs.getString(5);
                float proPrice = rs.getFloat(6);
                String pdColor = rs.getString(7);
                int quantity = rs.getInt(8);

                String pdimgURL = rs.getString(9);

                Item i = new Item(proName, proImage, proPrice, pdColor, quantity, pdimgURL);
                listI.add(i);
            }
        } catch (Exception e) {
            System.out.println("getListItemByOrderID: " + e.getMessage());
        }
        return listI;
    }

    public ArrayList<Product> getListProductByFilter(int cateID, int status, String search, int pageNo, int pageSize) {
        ArrayList<Product> listProduct = new ArrayList<>();
        String sql = "select * from product";
        boolean whereAdded = false;
        if (cateID != -1 || status != -1 || !search.isEmpty()) {
            sql += " WHERE";
            if (cateID != -1) {
                sql += " proCateID = ?";
                whereAdded = true;
            }
            if (status != -1) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " proStatus = ?";
                whereAdded = true;
            }
            if (!search.isEmpty()) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " (proName LIKE ?)";
            }
        }

        System.out.println(sql);

        sql += " ORDER BY proID OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1;
            if (cateID != -1) {
                st.setInt(parameterIndex, cateID);
                parameterIndex++;
            }
            if (status != -1) {
                st.setInt(parameterIndex, status);
                parameterIndex++;
            }
            if (!search.isEmpty()) {
                st.setString(parameterIndex, "%" + search + "%");
                parameterIndex++;

            }
            //set the limit and offset parameters for pagination
            st.setInt(parameterIndex, (pageNo - 1) * pageSize);
            parameterIndex++;
            st.setInt(parameterIndex, pageSize);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                float proPrice = rs.getFloat(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = new java.sql.Date(new java.util.Date().getTime());
                int proAdID = rs.getInt(8);
                int proStatus = rs.getInt(9);
                Product p = new Product(proID, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, proStatus);
                listProduct.add(p);
            }

        } catch (Exception e) {
        }
        return listProduct;
    }

    public int getTotalPage(int cateID, int status, String search, int pageSize) {
        String sql = "select count(*) from product";
        boolean whereAdded = false;
        if (cateID != -1 || status != -1 || !search.isEmpty()) {
            sql += " WHERE";
            if (cateID != -1) {
                sql += " proCateID = ?";
                whereAdded = true;
            }
            if (status != -1) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " proStatus = ?";
                whereAdded = true;
            }
            if (!search.isEmpty()) {
                if (whereAdded) {
                    sql += " AND";
                }
                sql += " (proName LIKE ?)";
            }
        }
        System.out.println(sql);
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1;
            if (cateID != -1) {
                st.setInt(parameterIndex, cateID);
                parameterIndex++;
            }
            if (status != -1) {
                st.setInt(parameterIndex, status);
                parameterIndex++;
            }
            if (!search.isEmpty()) {
                st.setString(parameterIndex, "%" + search + "%");
                parameterIndex++;

            }
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int totalRecord = rs.getInt(1);
                int totalPage = totalRecord / pageSize;
                if (totalPage % pageSize != 0) {
                    totalPage++;
                }
                return totalPage;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public static void main(String[] args) {
        ProductDAO d = new ProductDAO();
       ArrayList<Item> listI = new ArrayList<>();
       listI = d.getListItemByOrderID("11");
        for (Item i : listI) {
            System.out.println(i);
        }

    }

}
