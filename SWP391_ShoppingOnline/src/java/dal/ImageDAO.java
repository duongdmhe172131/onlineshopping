/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Image;
import model.ProductDetail;

/**
 *
 * @author huytu
 */
public class ImageDAO extends DBConnect{
    public ArrayList<Image> getListImagebyProID(String proID) {
        ArrayList<Image> listImg = new ArrayList<>();
        String sql = "select * from Image where imageProID = ? and imageStatus=1";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, proID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int imageID = rs.getInt(1);
                String imageName = rs.getString(2);
                int imageProID = rs.getInt(3);
                String imageURL = rs.getString(4);
                int imageStatus = rs.getInt(5);
                Image img = new Image(imageID, imageName, imageProID, imageURL, imageStatus);
                listImg.add(img);
            }
        } catch (Exception e) {
            System.out.println("getListImagebyProID: " + e.getMessage());
        }
        return listImg;
    }
    
    public static void main(String[] args) {
        ArrayList<Image> listImg = new ArrayList<>();
        ImageDAO dao = new ImageDAO();
        listImg = dao.getListImagebyProID("1");
        for (Image p : listImg) {
            System.out.println(p.toString());
        }
    }
}
