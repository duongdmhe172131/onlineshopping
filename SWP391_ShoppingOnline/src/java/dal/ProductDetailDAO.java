/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import static java.util.Comparator.comparing;
import model.Category;
import model.Product;
import model.ProductDetail;
import model.ProductDetailStock;

/**
 *
 * @author windy
 */
public class ProductDetailDAO extends DBConnect {

    public ArrayList<ProductDetail> getListProductDetailbyProID(String proID) {
        ArrayList<ProductDetail> listPd = new ArrayList<>();
        String sql = "select * from ProductDetail where pdProID = ? and pdStatus=1";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, proID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int pdID = rs.getInt(1);
                int pdProID = rs.getInt(2);
                String pdColor = rs.getString(3);
                int pdSold = rs.getInt(4);
                int pdStatus = rs.getInt(5);
                ProductDetail pd = new ProductDetail(pdID, pdProID, pdColor, pdSold, pdStatus);
                listPd.add(pd);
            }
        } catch (Exception e) {
            System.out.println("ProductDetail: " + e.getMessage());
        }
        return listPd;
    }

    public ProductDetail getProductDetailbyPdID(String pdId) {
        String sql = "select * from ProductDetail where pdID = ? and pdStatus=1";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, pdId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int pdID = rs.getInt(1);
                int pdProID = rs.getInt(2);
                String pdColor = rs.getString(3);
                int pdSold = rs.getInt(4);
                int pdStatus = rs.getInt(5);
                ProductDetail pd = new ProductDetail(pdID, pdProID, pdColor, pdSold, pdStatus);
                return pd;
            }
        } catch (Exception e) {
            System.out.println("ProductDetail: " + e.getMessage());
        }
        return null;
    }

    public ArrayList<ProductDetailStock> getProductDetailStock(String proID) {
        String sql = "select p.*,pd.pdID, pd.pdColor,pd.pdAvailable,pd.pdSold,pd.pdCreateDate,pd.pdStatus from Product p\n"
                + "join ProductDetail pd on p.proID = pd.pdProID\n"
                + "where proID =?\n";
        ArrayList<ProductDetailStock> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, proID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proId = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                double proPrice = rs.getDouble(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = rs.getDate(7);
                int proAdID = rs.getInt(8);
                int proStatus = rs.getInt(9);
                int pdID = rs.getInt(10);
                String pdColor = rs.getString(11);
                int pdAvailable = rs.getInt(12);
                int pdSold = rs.getInt(13);
                Date pdCreateDate = rs.getDate(14);
                int pdStatus = rs.getInt(15);
                ProductDetailStock pdStock = new ProductDetailStock(proId, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, proStatus, pdID, pdColor, pdAvailable, pdSold, pdCreateDate, pdStatus);
                list.add(pdStock);

            }
        } catch (Exception e) {
        }
        return list;
    }

    public ArrayList<ProductDetailStock> searchProductDetailStock(int proID, String color) {
        String sql = "select p.*,pd.pdID, pd.pdColor,pd.pdAvailable,pd.pdSold,pd.pdCreateDate,pd.pdStatus from Product p\n"
                + "join ProductDetail pd on p.proID = pd.pdProID\n"
                + "where proID =? and pd.pdColor like CONCAT('%', ?, '%')";
        ArrayList<ProductDetailStock> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, proID);
            st.setString(2, color);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proId = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                double proPrice = rs.getDouble(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = rs.getDate(7);
                int proAdID = rs.getInt(8);
                int proStatus = rs.getInt(9);

                int pdID = rs.getInt(10);
                String pdColor = rs.getString(11);
                int pdAvailable = rs.getInt(12);
                int pdSold = rs.getInt(13);
                Date pdCreateDate = rs.getDate(14);
                int pdStatus = rs.getInt(15);
                ProductDetailStock pdStock = new ProductDetailStock(proId, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, proStatus, pdID, pdColor, pdAvailable, pdSold, pdCreateDate, pdStatus);
                list.add(pdStock);

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public ArrayList<ProductDetailStock> getProductDetailStockByStatus(String proID, int status) {
        String sql = "select p.*,pd.pdID, pd.pdColor,pd.pdAvailable,pd.pdSold,pd.pdCreateDate,pd.pdStatus from Product p\n"
                + "join ProductDetail pd on p.proID = pd.pdProID\n"
                + "where proID =? and pd.pdStatus = ?";
        ArrayList<ProductDetailStock> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, proID);
            st.setInt(2, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proId = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                double proPrice = rs.getDouble(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = rs.getDate(7);
                int proAdID = rs.getInt(8);
                int proStatus = rs.getInt(9);

                int pdID = rs.getInt(10);
                String pdColor = rs.getString(11);
                int pdAvailable = rs.getInt(12);
                int pdSold = rs.getInt(13);
                Date pdCreateDate = rs.getDate(14);
                int pdStatus = rs.getInt(15);
                ProductDetailStock pdStock = new ProductDetailStock(proId, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, proStatus, pdID, pdColor, pdAvailable, pdSold, pdCreateDate, pdStatus);
                list.add(pdStock);

            }
        } catch (Exception e) {
        }
        return list;
    }

    public ProductDetailStock getProductDetailStock2(String proID, String pdID) {
        String sql = "select p.*,pd.pdID, pd.pdColor,pd.pdAvailable,pd.pdSold,pd.pdCreateDate,pd.pdStatus from Product p\n"
                + "join ProductDetail pd on p.proID = pd.pdProID\n"
                + "where proID = ? and pdID =?\n";
        ArrayList<ProductDetailStock> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, proID);
            st.setString(2, pdID);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int proId = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                double proPrice = rs.getDouble(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = rs.getDate(7);
                int proAdID = rs.getInt(8);
                int proStatus = rs.getInt(9);

                int pdId = rs.getInt(10);
                String pdColor = rs.getString(11);
                int pdAvailable = rs.getInt(12);
                int pdSold = rs.getInt(13);
                Date pdCreateDate = rs.getDate(14);
                int pdStatus = rs.getInt(15);
                ProductDetailStock pdStock = new ProductDetailStock(proId, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, proStatus, pdId, pdColor, pdAvailable, pdSold, pdCreateDate, pdStatus);
                return pdStock;

            }
        } catch (Exception e) {
        }
        return null;
    }

    public ArrayList<ProductDetailStock> getListProductDetailStockByFilter(int proId, String color, int status, int pageNo, int pageSize) {
        ArrayList<ProductDetailStock> list = new ArrayList<>();
        // Xây dựng câu truy vấn SQL
        String sql = "SELECT p.*, pd.pdID, pd.pdColor, pd.pdAvailable, pd.pdSold, pd.pdCreateDate, pd.pdStatus "
                + "FROM Product p "
                + "JOIN ProductDetail pd ON p.proID = pd.pdProID "
                + "WHERE p.proID = ? ";

        if (!color.isEmpty() || status != -1) {
            if (status != -1) {
                sql += "AND pd.pdStatus = ? ";
            }
            if (!color.isEmpty()) {
                sql += "AND pd.pdColor LIKE ?";
            }
        }

        // Phân trang
        sql += " ORDER BY pd.pdID ASC "
                + "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        System.out.println(sql);
        try {
            // Chuẩn bị statement
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1;
            st.setInt(parameterIndex, proId);
            parameterIndex++;
            if (status != -1) {
                st.setInt(parameterIndex, status);
                parameterIndex++;
            }
            if (!color.isEmpty()) {
                st.setString(parameterIndex, "%" + color + "%");
                parameterIndex++;
            }

            // Tính toán vị trí trang
            int offset = (pageNo - 1) * pageSize;
            st.setInt(parameterIndex, offset);
            parameterIndex++;
            st.setInt(parameterIndex, pageSize);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                //... (xử lý các trường dữ liệu như trong hàm cũ)  int proId = rs.getInt(1);
                int proID = rs.getInt(1);
                int proCateID = rs.getInt(2);
                String proName = rs.getString(3);
                double proPrice = rs.getDouble(4);
                String proImage = rs.getString(5);
                String proDetail = rs.getString(6);
                Date proCreateDate = rs.getDate(7);
                int proAdID = rs.getInt(8);
                int proStatus = rs.getInt(9);
                int pdID = rs.getInt(10);
                String pdColor = rs.getString(11);
                int pdAvailable = rs.getInt(12);
                int pdSold = rs.getInt(13);
                Date pdCreateDate = rs.getDate(14);
                int pdStatus = rs.getInt(15);
                ProductDetailStock pdStock = new ProductDetailStock(proID, proCateID, proName, proPrice, proImage, proDetail, proCreateDate, proAdID, proStatus, pdID, pdColor, pdAvailable, pdSold, pdCreateDate, pdStatus);
                list.add(pdStock);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public int getTotalPage(int proId, String color, int status, int pageSize) {
        ArrayList<ProductDetailStock> list = new ArrayList<>();
        // Xây dựng câu truy vấn SQL
        String sql = "SELECT count(*) "
                + "FROM Product p "
                + "JOIN ProductDetail pd ON p.proID = pd.pdProID "
                + "WHERE p.proID = ? ";

        if (!color.isEmpty() || status != -1) {
            if (status != -1) {
                sql += "AND pd.pdStatus = ? ";
            }
            if (!color.isEmpty()) {
                sql += "AND pd.pdColor LIKE ?";
            }
        }

        // Phân trang
        System.out.println(sql);
        try {
            // Chuẩn bị statement
            PreparedStatement st = conn.prepareStatement(sql);
            int parameterIndex = 1;
            st.setInt(parameterIndex, proId);
            parameterIndex++;
            if (status != -1) {
                st.setInt(parameterIndex, status);
                parameterIndex++;
            }
            if (!color.isEmpty()) {
                st.setString(parameterIndex, "%" + color + "%");
                parameterIndex++;
            }

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int totalRecord = rs.getInt(1);
                int totalPage = totalRecord / pageSize;
                if (totalPage % pageSize != 0) {
                    totalPage++;
                }
                return totalPage;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public void updateStock(String color, int available, int status, int pdID, int proID) {
        String sql = "update [dbo].[ProductDetail] set pdColor = ? , pdAvailable = ? , pdStatus = ?"
                + "where pdID = ? and pdProID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, color);
            st.setInt(2, available);
            st.setInt(3, status);
            st.setInt(4, pdID);
            st.setInt(5, proID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateStockImage(int pdID, String filename) {
        String sql = "update [dbo].[ProductDetailImage] set [pdimgURL] = ? where [pdimgPdID] = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, filename);
            st.setInt(2, pdID);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("updateStockImage:  " + e);
        }
    }

    public void updateStatusStock(int status, int pdID, int pdProID) {
        String sql = "update ProductDetail set pdStatus = ? where pdID = ? and pdProID = ?";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, pdID);
            st.setInt(3, pdProID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void addNewImgProductDetail(String pdimgName, int pdimgPdID, String pdimgURL, int pdimgStatus) {
        String sql = "INSERT INTO [dbo].[ProductDetailImage]\n"
                + "           ([pdimgName]\n"
                + "           ,[pdimgPdID]\n"
                + "           ,[pdimgURL]\n"
                + "           ,[pdimgStatus])\n"
                + "     VALUES(?,?,?,?)";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, pdimgName);
            st.setInt(2, pdimgPdID);
            st.setString(3, pdimgURL);
            st.setInt(4, pdimgStatus);
            st.executeUpdate();
            st.close();
        } catch (Exception e) {
            e.printStackTrace(); // Handle the exception properly, e.g., logging or throwing it.
        }
    }

    public void addNewProductDetail(int pdProID, String pdColor, int pdAvailable, int pdAdID, int pdStatus) {
        String sql = "INSERT INTO [dbo].[ProductDetail]\n"
                + "           ([pdProID]\n"
                + "           ,[pdColor]\n"
                + "           ,[pdSold]\n"
                + "           ,[pdAvailable]\n"
                + "           ,[pdCreateDate]\n"
                + "           ,[pdAdID]\n"
                + "           ,[pdStatus])\n"
                + "     VALUES(?,?,'0',?,?,?,?)";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, pdProID);
            st.setString(2, pdColor);
            st.setInt(3, pdAvailable);
            st.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
            st.setInt(5, pdAdID);
            st.setInt(6, pdStatus);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public int getPdIDtop() {
        String sql = "SELECT TOP 1 [pdID]\n"
                + "  FROM [SE1746_KS_SWP391_G6].[dbo].[ProductDetail]\n"
                + "  ORDER BY pdID DESC";
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int lastPdID = rs.getInt(1);
                return lastPdID;
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static void main(String[] args) {
        ProductDetailDAO dao = new ProductDetailDAO();
//        listPd = dao.getProductDetailStock("1");
//        for (ProductDetailStock productDetailStock : listPd) {
//            System.out.println(productDetailStock);
//        }

//        ArrayList<ProductDetailStock> listPd = dao.getListProductDetailStockByFilter(1, "gold", 1, 1, 1);
//        for (ProductDetailStock productDetailStock : listPd) {
//            System.out.println(productDetailStock);
//
//        }
//        System.out.println(dao.getTotalPage(1, "gold", 1, 1));
        // dao.addNewProductDetail(1, "g", 2, 2, 1);
        System.out.println(dao.getPdIDtop());
    }

    public String getProductDetailImage(String pdId) {
        String sql = "select * from [ProductDetailImage] where pdimgPdID = ?";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, pdId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString(4);
            }
        } catch (Exception e) {
            System.out.println("ProductDetail: " + e.getMessage());
        }
        return null;
    }

}
