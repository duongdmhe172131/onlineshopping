/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBConnect;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Customer;
import model.Feedback;
import model.Product;

/**
 *
 * @author 84375
 */
public class FeedbackDAO extends DBConnect {

    public ArrayList<Feedback> getListFeedback(String status, String search, String star) {

        if (star.isEmpty()) {
            star = "-1";
        }
        if (status.isEmpty()) {
            status = "-1";
        }
        ArrayList<Feedback> listF = new ArrayList<>();
        String sql = "  select * from Feedback f  left join Customer u on f.fbCusID =u.cusID \n"
                + "              left join Product p on f.fbProID = p.proID  "
                + "	where ( -1 = " + star + " or  f.fbStar =  " + star + " ) and ( -1 = " + status + " or  f.fbStatus =  " + status + " )\n"
                + " and (u.cusUsername like '%" + search + "%' or p.proName like '%" + search + "%') ";
        System.out.println(sql);
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int fbId = rs.getInt(1);
                int fbCusId = rs.getInt(2);
                int fbProId = rs.getInt(3);
                int fbStar = (int) rs.getFloat(4);
                String fbContent = rs.getString(5);
                String fbImage = rs.getString(6);
                Date fbDate = rs.getDate(7);
                int fbStatus = rs.getInt(8);
                Feedback F = new Feedback(fbId, fbCusId, fbProId, fbStar, fbContent, fbImage, fbDate, fbStatus);
                Customer c = new Customer();
                c.setCusUsername(rs.getString("cusUsername"));
                c.setCusEmail(rs.getString("cusEmail"));
                c.setCusPhone(rs.getString("cusPhone"));
                c.setCusContactName(rs.getString("cusContactName"));
                Product p = new Product();
                p.setProName(rs.getString("proName"));
                p.setProImage(rs.getString("proImage"));
                F.setCustomer(c);
                F.setProduct(p);
                listF.add(F);
            }
        } catch (Exception e) {
            System.out.println("ListFeedbeck: " + e.getMessage());
            e.printStackTrace();
        }
        return listF;
    }

    public Feedback getFeedbackByid(String fid) {

        String sql = "  select * from Feedback f  left join Customer u on f.fbCusID =u.cusID \n"
                + "              left join Product p on f.fbProID = p.proID  "
                + "	where f.fbID =" + fid;
        System.out.println(sql);
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int fbId = rs.getInt(1);
                int fbCusId = rs.getInt(2);
                int fbProId = rs.getInt(3);
                int fbStar = (int) rs.getFloat(4);
                String fbContent = rs.getString(5);
                String fbImage = rs.getString(6);
                Date fbDate = rs.getDate(7);
                int fbStatus = rs.getInt(8);
                Feedback F = new Feedback(fbId, fbCusId, fbProId, fbStar, fbContent, fbImage, fbDate, fbStatus);
                Customer c = new Customer();
                c.setCusUsername(rs.getString("cusUsername"));
                c.setCusEmail(rs.getString("cusEmail"));
                c.setCusPhone(rs.getString("cusPhone"));
                Product p = new Product();
                p.setProName(rs.getString("proName"));
                F.setCustomer(c);
                F.setProduct(p);
                return F;
            }
        } catch (Exception e) {
            System.out.println("ListFeedbeck: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Feedback> getListFeedback() {
        ArrayList<Feedback> listF = new ArrayList<>();
        String sql = "select * from Feedback f  left join Customer u on f.fbCusID =u.cusID \n"
                + "              left join Product p on f.fbProID = p.proID ";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int fbId = rs.getInt(1);
                int fbCusId = rs.getInt(2);
                int fbProId = rs.getInt(3);
                int fbStar = (int) rs.getFloat(4);
                String fbContent = rs.getString(5);
                String fbImage = rs.getString(6);
                Date fbDate = rs.getDate(7);
                int fbStatus = rs.getInt(8);
                Feedback F = new Feedback(fbId, fbCusId, fbProId, fbStar, fbContent, fbImage, fbDate, fbStatus);
                Customer c = new Customer();
                c.setCusUsername(rs.getString("cusUsername"));
                c.setCusEmail(rs.getString("cusEmail"));
                c.setCusPhone(rs.getString("cusPhone"));
                Product p = new Product();
                p.setProName(rs.getString("proName"));
                F.setCustomer(c);
                F.setProduct(p);
                listF.add(F);
            }
        } catch (Exception e) {
            System.out.println("ListFeedbeck: " + e.getMessage());
        }
        return listF;
    }

    public ArrayList<Feedback> getListFeedbackByPid(String pid) {
        ArrayList<Feedback> listF = new ArrayList<>();
        String sql = "  select * from Feedback f join Customer c on f.fbCusID = c.cusID where fbProID = " + pid;

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int fbId = rs.getInt(1);
                int fbCusId = rs.getInt(2);
                int fbProId = rs.getInt(3);
                int fbStar = (int) rs.getFloat(4);
                String fbContent = rs.getString(5);
                String fbImage = rs.getString(6);
                Date fbDate = rs.getDate(7);
                int fbStatus = rs.getInt(8);
                Customer cus = new Customer();
                cus.setCusContactName(rs.getString(10));
                Feedback F = new Feedback(fbId, fbCusId, fbProId, fbStar, fbContent, fbImage, fbDate, fbStatus);
                F.setCustomer(cus);
                listF.add(F);
            }
        } catch (Exception e) {
            System.out.println("ListFeedbeck: " + e.getMessage());
        }
        return listF;
    }

    public void insertFeedback(String pid, int uid, String content, String rate, String img) {
        String sql = "INSERT INTO [dbo].[Feedback]\n"
                + "           ([fbCusID]   ,[fbProID] ,[fbStar]\n"
                + "           ,[fbContent] ,[fbImage]\n"
                + "           ,[fbDate],[fbStatus]) VALUES (?,?,?,?,?,GETDATE(),1)";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, uid);
            st.setString(2, pid);
            st.setString(3, rate);
            st.setString(4, content);
            st.setString(5, img);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("insertFeedback: " + e.getMessage());
        }
    }

    public void changeStatusFeedback(String fid, String ss) {
        String sql = "UPDATE [dbo].[Feedback]  SET [fbStatus] = ?\n"
                + " WHERE [fbID] = ? ";

        try {
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, ss);
            st.setString(2, fid);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("insertFeedback: " + e.getMessage());
            e.printStackTrace();

        }
    }
}
