package dal;

import context.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Slider;

public class SliderDAO extends DBConnect {

    public SliderDAO() {
        super(); // Call the constructor of the parent class (DBConnect)
    }

    // Insert a new slider into the database
    public void insertSlider(Slider slider) {
        try {
            String insertQuery = "INSERT INTO Slider (sliderTitle, sliderImage, sliderNote, sliderStatus) VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(insertQuery);
            preparedStatement.setString(1, slider.getSliderTitle());
            preparedStatement.setString(2, slider.getSliderImage());
            preparedStatement.setString(3, slider.getSliderNote());
            preparedStatement.setInt(4, slider.getSliderStatus());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Retrieve a slider by its ID
    public Slider getSliderById(int sliderID) {
        Slider slider = null;
        try {
            String selectQuery = "SELECT sliderID, sliderTitle, sliderImage, sliderNote, sliderStatus FROM Slider WHERE sliderID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            preparedStatement.setInt(1, sliderID);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String sliderTitle = resultSet.getString("sliderTitle");
                String sliderImage = resultSet.getString("sliderImage");
                String sliderNote = resultSet.getString("sliderNote");
                int sliderStatus = resultSet.getInt("sliderStatus");

                slider = new Slider(sliderID, sliderTitle, sliderImage, sliderNote, sliderStatus);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return slider;
    }

    // Retrieve all sliders from the database
    public List<Slider> getAllSliders() {
        List<Slider> sliders = new ArrayList<>();
        try {
            String selectQuery = "SELECT sliderID, sliderTitle, sliderImage, sliderNote, sliderStatus FROM Slider";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int sliderID = resultSet.getInt("sliderID");
                String sliderTitle = resultSet.getString("sliderTitle");
                String sliderImage = resultSet.getString("sliderImage");
                String sliderNote = resultSet.getString("sliderNote");
                int sliderStatus = resultSet.getInt("sliderStatus");

                Slider slider = new Slider(sliderID, sliderTitle, sliderImage, sliderNote, sliderStatus);
                sliders.add(slider);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sliders;
    }
    
    public List<Slider> getAllSlidersHome() {
        List<Slider> sliders = new ArrayList<>();
        try {
            String selectQuery = "SELECT sliderID, sliderTitle, sliderImage, sliderNote, sliderStatus FROM Slider Where sliderStatus>0 order by sliderStatus";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int sliderID = resultSet.getInt("sliderID");
                String sliderTitle = resultSet.getString("sliderTitle");
                String sliderImage = resultSet.getString("sliderImage");
                String sliderNote = resultSet.getString("sliderNote");
                int sliderStatus = resultSet.getInt("sliderStatus");

                Slider slider = new Slider(sliderID, sliderTitle, sliderImage, sliderNote, sliderStatus);
                sliders.add(slider);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sliders;
    }

    // Update a slider in the database
    public void updateSlider(Slider slider) {
        try {
            String updateQuery = "UPDATE Slider SET sliderTitle = ?, sliderImage = ?, sliderNote = ?, sliderStatus = ? WHERE sliderID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(updateQuery);
            preparedStatement.setString(1, slider.getSliderTitle());
            preparedStatement.setString(2, slider.getSliderImage());
            preparedStatement.setString(3, slider.getSliderNote());
            preparedStatement.setInt(4, slider.getSliderStatus());
            preparedStatement.setInt(5, slider.getSliderID());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Delete a slider from the database
    public void deleteSlider(int sliderID) {
        try {
            String deleteQuery = "DELETE FROM Slider WHERE sliderID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(deleteQuery);
            preparedStatement.setInt(1, sliderID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

