package dal;

import context.DBConnect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Role;

public class RoleDAO extends DBConnect {

    public RoleDAO() {
        super(); // Call the constructor of the parent class (DBConnect)
    }

    // Insert a new role into the database
    public void insertRole(Role role) {
        try {
            String insertQuery = "INSERT INTO Role (roleName, roleStatus) VALUES (?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(insertQuery);
            preparedStatement.setString(1, role.getRoleName());
            preparedStatement.setInt(2, role.getRoleStatus());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RoleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Retrieve a role by its ID
    public Role getRoleById(int roleID) {
        Role role = null;
        try {
            String selectQuery = "SELECT roleID, roleName, roleStatus FROM Role WHERE roleID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            preparedStatement.setInt(1, roleID);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String roleName = resultSet.getString("roleName");
                int roleStatus = resultSet.getInt("roleStatus");

                role = new Role(roleID, roleName, roleStatus);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return role;
    }

    // Retrieve all roles from the database
    public List<Role> getAllRoles() {
        List<Role> roles = new ArrayList<>();
        try {
            String selectQuery = "SELECT roleID, roleName, roleStatus FROM Role";
            PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int roleID = resultSet.getInt("roleID");
                String roleName = resultSet.getString("roleName");
                int roleStatus = resultSet.getInt("roleStatus");

                Role role = new Role(roleID, roleName, roleStatus);
                roles.add(role);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return roles;
    }

    // Update a role in the database
    public void updateRole(Role role) {
        try {
            String updateQuery = "UPDATE Role SET roleName = ?, roleStatus = ? WHERE roleID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(updateQuery);
            preparedStatement.setString(1, role.getRoleName());
            preparedStatement.setInt(2, role.getRoleStatus());
            preparedStatement.setInt(3, role.getRoleID());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RoleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Delete a role from the database
    public void deleteRole(int roleID) {
        try {
            String deleteQuery = "DELETE FROM Role WHERE roleID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(deleteQuery);
            preparedStatement.setInt(1, roleID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RoleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

