/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.marketing;

import dal.SliderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import model.Slider;

/**
 *
 * @author BEAN
 */
@WebServlet(name="ManageSliderDetailController", urlPatterns={"/marketing/sliderDetails"})
@MultipartConfig
public class ManageSliderDetailController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageSliderDetailController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageSliderDetailController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
          String id = request.getParameter("sliderId");
          SliderDAO dao = new SliderDAO();
          Slider slider = dao.getSliderById(Integer.parseInt(id));
           request.setAttribute("slider", slider);
        request.getRequestDispatcher("/marketing-manage-sliderDetail.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        Part filePart = request.getPart("image");
        String fileName = request.getParameter("imageName");

        if(filePart.getSubmittedFileName() != "")
        {
             // Get the filename
        fileName = getSubmittedFileName(filePart);

        // Define the directory where the file will be saved
        String uploadPath = getServletContext().getRealPath("") + File.separator + "img";
        File uploadDir = new File(uploadPath);

        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // Save the file to the server
        try (InputStream input = filePart.getInputStream();
             OutputStream output = new FileOutputStream(uploadPath + File.separator + fileName)) {
            int read;
            byte[] buffer = new byte[1024];
            while ((read = input.read(buffer)) != -1) {
                output.write(buffer, 0, read);
            }
        }
        }
       
        String id = request.getParameter("id");
        String title = request.getParameter("title");
        String note = request.getParameter("note");
        String status = request.getParameter("status");
        
        Slider slider = new Slider();
        
        slider.setSliderID(Integer.parseInt(id));
        slider.setSliderImage(fileName);
        slider.setSliderTitle(title);
        slider.setSliderNote(note);
        slider.setSliderStatus(Integer.parseInt(status));
        
        new SliderDAO().updateSlider(slider);
        
        response.sendRedirect("./manage-slider");
    }

    
     private String getSubmittedFileName(Part part) {
        String header = part.getHeader("content-disposition");
        String[] elements = header.split(";");
        for (String element : elements) {
            if (element.trim().startsWith("filename")) {
                return element.substring(element.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return "";
    }
     
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
