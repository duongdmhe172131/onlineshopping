/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.marketing;

import dal.ProductDAO;
import dal.ProductDetailDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Admin;
import model.Product;
import model.ProductDetailStock;

/**
 *
 * @author BEAN
 */
public class ManageProductDetailStockController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageProductDetailStockController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageProductDetailStockController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Admin ad = (Admin) session.getAttribute("admin");

        //check session admin
        if (ad == null) {
            request.getRequestDispatcher("../admin/login").forward(request, response);
        }
        if (ad != null) {
            int adID = ad.getAdId();
        }
        String color = "";
        int status = -1;
        int pageNo = 1;
        final int pageSize = 5;

        int proId = Integer.parseInt(request.getParameter("proId"));

        try {
            color = request.getParameter("search") == null ? "" : request.getParameter("search");
            status = request.getParameter("status") == null ? -1 : Integer.parseInt(request.getParameter("status"));
            pageNo = request.getParameter("pageNo") == null ? 1 : Integer.parseInt(request.getParameter("pageNo"));

        } catch (Exception e) {
        }

        System.out.println("color: " + color + "status: " + status);
        ProductDetailDAO daoProductDetail = new ProductDetailDAO();
        ArrayList<ProductDetailStock> listProductDetail = daoProductDetail.getListProductDetailStockByFilter(proId, color, status, pageNo, pageSize); //        String color = request.getParameter("search-color");
        int totalPage = daoProductDetail.getTotalPage(proId, color, status, pageSize);

        request.setAttribute("search", color);
        request.setAttribute("status", status);
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("currentPage", pageNo);
        request.setAttribute("proId", proId);

        request.setAttribute("listPdStock", listProductDetail);
        request.getRequestDispatcher("/marketing-manage-productDetailStock.jsp").forward(request, response);

//        ProductDetailDAO daoProductDetail = new ProductDetailDAO();
//        String proID = request.getParameter("proId");
//        ArrayList<ProductDetailStock> listPdStock = daoProductDetail.getProductDetailStock(proID);
//        request.setAttribute("listPdStock", listPdStock);
//        request.setAttribute("proId", proID);
//        request.getRequestDispatcher("/marketing-manage-productDetailStock.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
