/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.marketing;

import dal.ProductDetailDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import model.ProductDetailStock;

/**
 *
 * @author BEAN
 */
@MultipartConfig
public class EditProductStockController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditProductStockController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditProductStockController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDetailDAO daoProductDetail = new ProductDetailDAO();
        String proID = request.getParameter("proId");
        String pdID = request.getParameter("pdId");
        ProductDetailStock pdStock = daoProductDetail.getProductDetailStock2(proID, pdID);
        System.out.println(pdStock);
        request.setAttribute("pdStock", pdStock);
        request.getRequestDispatcher("/marketing-manage-editProductStock.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        try {
            String fileName = request.getParameter("imageOld");
        
            Part filePart = request.getPart("stockImg");

            if(filePart.getSubmittedFileName() != "") {
                 // Get the filename
             fileName = getSubmittedFileName(filePart);

                // Define the directory where the file will be saved
                String uploadPath = getServletContext().getRealPath("") + File.separator + "pdimg";
                File uploadDir = new File(uploadPath);

                if (!uploadDir.exists()) {
                    uploadDir.mkdir();
                }

                // Save the file to the server
                try (InputStream input = filePart.getInputStream();
                     OutputStream output = new FileOutputStream(uploadPath + File.separator + fileName)) {
                    int read;
                    byte[] buffer = new byte[1024];
                    while ((read = input.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }
                }
            }
                    
           
            String color = request.getParameter("color");

            int available = Integer.parseInt(request.getParameter("available"));
            int status = Integer.parseInt(request.getParameter("status"));
            String pdIdString = request.getParameter("pdID");
            int pdID = Integer.parseInt(pdIdString);
            System.out.println(color + " " + available + " " + status + " " + pdID);
            String proIdString = request.getParameter("proId") ;
            int proID = Integer.parseInt(proIdString);
            ProductDetailDAO daoProductDetail = new ProductDetailDAO();
            daoProductDetail.updateStock(color, available, status, pdID, proID);
            System.out.println(pdID + " " + fileName + "edit stock");
            daoProductDetail.updateStockImage(pdID, fileName);
            session.setAttribute("alertMessage", true);
            
            response.sendRedirect("manage-productDetailStock?proId=" + proIdString + "&pdId=" + pdIdString);
       
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
     private String getSubmittedFileName(Part part) {
        String header = part.getHeader("content-disposition");
        String[] elements = header.split(";");
        for (String element : elements) {
            if (element.trim().startsWith("filename")) {
                return element.substring(element.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return "";
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
