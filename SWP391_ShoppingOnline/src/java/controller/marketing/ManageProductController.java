/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.marketing;

import dal.CategoryDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Admin;
import model.Category;
import model.Product;

/**
 *
 * @author BEAN
 */
public class ManageProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageProductController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageProductController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Admin ad = (Admin) session.getAttribute("admin");

        //check session admin
        if (ad == null) {
            request.getRequestDispatcher("../admin/login").forward(request, response);
        } if (ad !=null) {
            int adID = ad.getAdId();
        }
        String search = "";
        int cateID = -1;
        int status = -1;
        int pageNo = 1;
        final int pageSize = 10;
        try {
            search = request.getParameter("search") == null ? "" : request.getParameter("search");
            cateID = request.getParameter("cateID") == null ? -1 : Integer.parseInt(request.getParameter("cateID"));
            status = request.getParameter("status") == null ? -1 : Integer.parseInt(request.getParameter("status"));
            pageNo = request.getParameter("pageNo") == null ? 1 : Integer.parseInt(request.getParameter("pageNo"));

        } catch (Exception e) {
        }
        
        //System.out.println("cate"+cateID);

        ProductDAO daoProduct = new ProductDAO();
        ArrayList<Product> listProduct = daoProduct.getListProductByFilter(cateID, status, search, pageNo, pageSize);
        int totalPage = daoProduct.getTotalPage(cateID, status, search, pageSize);
        
        CategoryDAO daoCategory = new CategoryDAO();
        ArrayList<Category>  listCate = daoCategory.getListCategory();
        
        
       // System.out.println(totalPage);
        
        
        request.setAttribute("search", search);
        request.setAttribute("cateID", cateID);
        request.setAttribute("status", status);
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("currentPage", pageNo);

        request.setAttribute("listCate", listCate);
        request.setAttribute("listProduct", listProduct);
        request.getRequestDispatcher("/marketing-manage-product.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String service = request.getParameter("service");
        ProductDAO daoProduct = new ProductDAO();
        if (service == null) {
            service = "manage-product";
        }

        if (service.equals("updateStatusProduct")) {
            int proID = Integer.parseInt(request.getParameter("proId"));
            int status = Integer.parseInt(request.getParameter("status"));
            daoProduct.updateStatusProduct(status, proID);
            response.sendRedirect("manage-product");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
