/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.marketing;

import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author BEAN
 */
@MultipartConfig
public class ManageProductDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String proId = request.getParameter("proId");
        ProductDAO daoProduct = new ProductDAO();
        Product product = daoProduct.getProductbyProID(proId);
       
        request.setAttribute("product", product);
        request.getRequestDispatcher("/marketing-manage-productDetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         // Get the file part from the request

        Part filePart = request.getPart("productImage");

        // Get the filename
        String fileName = getSubmittedFileName(filePart);

        // Define the directory where the file will be saved
        String uploadPath = getServletContext().getRealPath("") + File.separator + "img";
        File uploadDir = new File(uploadPath);

        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // Save the file to the server
        try (InputStream input = filePart.getInputStream();
             OutputStream output = new FileOutputStream(uploadPath + File.separator + fileName)) {
            int read;
            byte[] buffer = new byte[1024];
            while ((read = input.read(buffer)) != -1) {
                output.write(buffer, 0, read);
            }
        }
        
        String productImage = fileName;
        int proID = Integer.parseInt(request.getParameter("proId"));
        // System.out.println(proID);
        Date proDate = Date.valueOf(request.getParameter("proDate"));
        request.getParameter("proImage");
        String productName = request.getParameter("productName");
        Float productPrice = Float.parseFloat(request.getParameter("productPrice"));
        String description = request.getParameter("detail");
        int proCateID = Integer.parseInt(request.getParameter("category"));
        int status = Integer.parseInt(request.getParameter("status"));

        ProductDAO daoProduct = new ProductDAO();
        if (productImage == null || productImage.isEmpty()) {
            productImage = request.getParameter("proImage");
        }
        daoProduct.updateProduct(proCateID, productName, productPrice, productImage, description, proDate, 3, status, proID);
        HttpSession session = request.getSession();
        session.setAttribute("alertMessage", true);

        response.sendRedirect("manage-product");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

     private String getSubmittedFileName(Part part) {
        String header = part.getHeader("content-disposition");
        String[] elements = header.split(";");
        for (String element : elements) {
            if (element.trim().startsWith("filename")) {
                return element.substring(element.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return "";
    }
}
