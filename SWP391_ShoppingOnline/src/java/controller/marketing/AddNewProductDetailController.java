/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.marketing;

import dal.ProductDAO;
import dal.ProductDetailDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import model.Product;
import model.ProductDetailStock;

/**
 *
 * @author BEAN
 */
@MultipartConfig
public class AddNewProductDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddNewProductDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddNewProductDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO daoProduct = new ProductDAO();
        String proID = request.getParameter("proId");
        Product product = daoProduct.getProductbyProID(proID);

        request.setAttribute("product", product);
        request.setAttribute("proId", proID);

        request.getRequestDispatcher("/marketing-add-newProductDetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            
            Part filePart = request.getPart("productImage");

            // Get the filename
            String fileName = getSubmittedFileName(filePart);

            // Define the directory where the file will be saved
            String uploadPath = getServletContext().getRealPath("") + File.separator + "pdimg";
            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            // Save the file to the server
            try ( InputStream input = filePart.getInputStream();  OutputStream output = new FileOutputStream(uploadPath + File.separator + fileName)) {
                int read;
                byte[] buffer = new byte[1024];
                while ((read = input.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }
            }

            int pdProID = Integer.parseInt(request.getParameter("proId"));
            String color = request.getParameter("color");
            int available = Integer.parseInt(request.getParameter("available"));
            int status = Integer.parseInt(request.getParameter("status"));
            ProductDetailDAO daoProductDetail = new ProductDetailDAO();
            daoProductDetail.addNewProductDetail(pdProID, color, available, 3, status);

           daoProductDetail.addNewImgProductDetail("", daoProductDetail.getPdIDtop(), fileName, status);
//            System.out.println("?????" + pdProID);
//            System.out.println("?????" + daoProductDetail.getPdIDtop());
//            
//            System.out.println("????" + fileName);
            response.sendRedirect("manage-productDetailStock?proId=" + pdProID);
        } catch (Exception e) {
            System.out.println("loi o day " + e.getMessage());
        }
    }

    private String getSubmittedFileName(Part part) {
        String header = part.getHeader("content-disposition");
        String[] elements = header.split(";");
        for (String element : elements) {
            if (element.trim().startsWith("filename")) {
                return element.substring(element.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return "";
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
