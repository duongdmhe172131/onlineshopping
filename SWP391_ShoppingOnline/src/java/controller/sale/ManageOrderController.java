/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.sale;

import dal.CustomerDAO;
import dal.OrderDAO;
import dal.StatusOrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import model.Admin;
import model.Customer;
import model.Order;
import model.StatusOrder;

/**
 *
 * @author windy
 */
public class ManageOrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageOrderController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageOrderController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderDAO daoOrder = new OrderDAO();
        CustomerDAO daoCustomer = new CustomerDAO();
        StatusOrderDAO daoStatusOrder = new StatusOrderDAO();
        int adID = 0;
        String search = "";
        boolean checkUpdate = false;
        Date startDate = Date.valueOf("2000-01-01");
        long millis = System.currentTimeMillis();
        Date endDate = new java.sql.Date(millis);
        int statusOrder = -1;
        int pageNo = 1;
        final int pageSize = 3;

        HttpSession session = request.getSession();
        Admin ad = (Admin) session.getAttribute("admin");

        //check session
        if (ad == null) {
            request.getRequestDispatcher("../admin/login").forward(request, response);
        }

        if (ad != null) {
            adID = ad.getAdId();
        }

        String orderID = request.getParameter("orderID");
        if (orderID != null && !orderID.isEmpty()) {
            boolean check = daoOrder.checkOrderAdIDNull(orderID);
            if (check) {
                checkUpdate = daoOrder.updateOrderAdID(orderID, adID);
                if (checkUpdate) {
                    response.sendRedirect("../admin/home");
                }
            }
        } else {
            try {

                search = request.getParameter("search") == null ? "" : request.getParameter("search");
                startDate = request.getParameter("startDate") == null ? Date.valueOf("2000-01-01") : Date.valueOf(request.getParameter("startDate"));
                endDate = request.getParameter("endDate") == null ? endDate : Date.valueOf(request.getParameter("endDate"));
                statusOrder = request.getParameter("statusOrder") == null ? -1 : Integer.parseInt(request.getParameter("statusOrder"));
                pageNo = request.getParameter("pageNo") == null ? 1 : Integer.parseInt(request.getParameter("pageNo"));

            } catch (Exception e) {
                System.out.println("manageOrderController: " + e.getMessage());
            }

            //ArrayList<Order> listOrder = daoOrder.getOrderByAdID(adID);
            ArrayList<Order> listOrder = daoOrder.getListOrderByFilterAndAdID(search, startDate, endDate, statusOrder, pageNo, pageSize, adID);
            int totalPage = daoOrder.getTotalPage(search, startDate, endDate, statusOrder, pageSize, adID);
            ArrayList<Customer> listCustomer = daoCustomer.getListCustomer();
            List<StatusOrder> listStatusOrder = daoStatusOrder.getStatusOrderForSale();

            request.setAttribute("statusOrder", statusOrder);
            request.setAttribute("listOrder", listOrder);
            request.setAttribute("listCustomer", listCustomer);
            request.setAttribute("startDate", startDate);
            request.setAttribute("endDate", endDate);
            request.setAttribute("search", search);
            request.setAttribute("currentPage", pageNo);
            request.setAttribute("totalPage", totalPage);
            request.setAttribute("listStatusOrder", listStatusOrder);
            request.getRequestDispatcher("/manageOrder.jsp").forward(request, response);

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean checkUpdate = false;
        OrderDAO daoOrder = new OrderDAO();
        HttpSession session = request.getSession();

        Admin ad = (Admin) session.getAttribute("admin");
        int adID = ad.getAdId();
        String orderID = request.getParameter("orderID");
        boolean check = daoOrder.checkOrderAdIDNull(orderID);
        if (check) {
            checkUpdate = daoOrder.updateOrderAdID(orderID, adID);
            if (checkUpdate) {
                response.sendRedirect("../admin/home");
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
