/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import dal.CategoryDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Category;
import model.Product;

/**
 *
 * @author windy
 */
@WebServlet(name = "SearchFilterController", urlPatterns = {"/search"})
public class SearchFilterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchFilterController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchFilterController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int numOfProduct = 0;
        int itemsPerPage = 4;
        int totalPage = 0;
        int currentPage = 1;
        
        CategoryDAO daoCategory = new CategoryDAO();
        ProductDAO daoProduct = new ProductDAO();
        ArrayList<Category> listCategory = daoCategory.getListCategory();
        ArrayList<Product> listProduct = new ArrayList<>();
        
        String productName = request.getParameter("productName");
        String currentPage_raw = request.getParameter("currentPage");
        String option = request.getParameter("option");
        String status = request.getParameter("status");
        String sortStatus = request.getParameter("sortStatus");
        String cateID = request.getParameter("cateID");
        String sortOpt = request.getParameter("sort");
        
        if(!(currentPage_raw == null || currentPage_raw.isEmpty())){
            currentPage = Integer.parseInt(currentPage_raw);
        } 
        
        //check if users search or not
        if(option != null && !option.isEmpty()){
            numOfProduct = daoProduct.getNumOfProduct("search", productName);
            listProduct = daoProduct.searchProduct(productName,currentPage, itemsPerPage);
            status = "searched"; 
        } else{                      
            // users choose to sort
            sortStatus = "ok";
            //sort into a category
            if(cateID != null && !cateID.isEmpty()){
                System.out.println("get duoc cateID");
                numOfProduct = daoProduct.getNumOfProduct("category", cateID);
                listProduct = daoProduct.sortProduct(cateID, "", sortOpt, currentPage, itemsPerPage);
            } 
            // sort searched product
            else if(productName != null && !productName.isEmpty()){           
                numOfProduct = daoProduct.getNumOfProduct("search", productName);
                listProduct = daoProduct.sortProduct("", productName, sortOpt, currentPage, itemsPerPage);
            }
            //sort all product
            else{                                                     
                numOfProduct = daoProduct.getNumOfProduct("all","all");
                System.out.println("nhay vao day a");
                listProduct = daoProduct.sortProduct("", "", sortOpt, currentPage, itemsPerPage);
            }
        }
        
        totalPage = numOfProduct/itemsPerPage;
        if(numOfProduct % itemsPerPage != 0){
            totalPage++;
        }

        request.setAttribute("listCategory", listCategory);
        request.setAttribute("listProduct", listProduct);
        request.setAttribute("productName", productName);
        request.setAttribute("status", status);
        request.setAttribute("sortStatus", sortStatus);
        request.setAttribute("sortOpt", sortOpt);
        request.setAttribute("cateID", cateID);  
        request.setAttribute("option", option);  
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("currentPage", currentPage);
        request.getRequestDispatcher("index.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int numOfProduct = 0;
        int itemsPerPage = 4;
        int totalPage = 0;
        int currentPage = 1;
        
        CategoryDAO daoCategory = new CategoryDAO();
        ProductDAO daoProduct = new ProductDAO();
        ArrayList<Category> listCategory = daoCategory.getListCategory();
        ArrayList<Product> listProduct = new ArrayList<>();
        //ArrayList<Product> listProductRaw = new ArrayList<>();
        
        String productName = request.getParameter("productName");
        String currentPage_raw = request.getParameter("currentPage");
        String option = request.getParameter("option");
        String status = request.getParameter("status");
        String cateID = request.getParameter("cateID");
        String sortOpt = request.getParameter("sort");
        
        if(!(currentPage_raw == null || currentPage_raw.isEmpty())){
            currentPage = Integer.parseInt(currentPage_raw);
        } 
        
        //check if users search or not
        if(option != null && !option.isEmpty()){
            numOfProduct = daoProduct.getNumOfProduct("search", productName);
            listProduct = daoProduct.searchProduct(productName,currentPage, itemsPerPage);
            status = "searched"; 
        } else{                                                       // users choose to sort
            //sort into a category
            if(cateID != null && !cateID.isEmpty()){
                numOfProduct = daoProduct.getNumOfProduct("category", cateID);
                listProduct = daoProduct.sortProduct(cateID, "", sortOpt, currentPage, itemsPerPage);
                //listProductRaw = daoProduct.getListProductByCategory(cateID);
            } else if(status != null && !status.isEmpty()){           // sort searched product
                numOfProduct = daoProduct.getNumOfProduct("search", productName);
                listProduct = daoProduct.sortProduct("", productName, sortOpt, currentPage, itemsPerPage);
                //listProductRaw = daoProduct.searchProduct(productName);
            }
            else{                                                     //sort all product
                numOfProduct = daoProduct.getNumOfProduct("all","all");
                listProduct = daoProduct.sortProduct("", "", sortOpt, currentPage, itemsPerPage);
                //listProductRaw = daoProduct.getListProduct();
            }
//            if(sortOpt.equals("HighToLow")){
//                listProduct = daoProduct.sortProduct(listProductRaw, true);
//            } else if (sortOpt.equals("LowToHigh")) {
//                listProduct = daoProduct.sortProduct(listProductRaw, false);
//            } else {
//                listProduct = listProductRaw;
//            }
        }
        
        totalPage = numOfProduct/itemsPerPage;
        if(numOfProduct % itemsPerPage != 0){
            totalPage++;
        }

        request.setAttribute("listCategory", listCategory);
        request.setAttribute("listProduct", listProduct);
        request.setAttribute("productName", productName);
        request.setAttribute("status", status);
        request.setAttribute("sortOpt", sortOpt);
        request.setAttribute("cateID", cateID);  
        request.setAttribute("option", option);  
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("currentPage", currentPage);
        request.getRequestDispatcher("index.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
