/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import dal.CustomerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Customer;
import util.CryptoUtils;
import util.Encode;
import util.SendMail;
import util.InputValidator;

/**
 *
 * @author BEAN
 */
@WebServlet(name = "RegisterController", urlPatterns = {"/register"})
public class RegisterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //get form
            String cusName = request.getParameter("cusName");
            String email = request.getParameter("email");
            String phone = request.getParameter("phone");
            String cusUsername = request.getParameter("cusUsername");
            String cusPassword = request.getParameter("cusPassword");
            String rePassword = request.getParameter("rePassword");

            // create
            CustomerDAO daoCustomer = new CustomerDAO();
            boolean isValid = true;

            //check email, username, phone exist
            if (daoCustomer.checkCustomerExistByEmail(email) != null) {
                request.setAttribute("emailError", "Email already exists");
                isValid = false;
            }

            if (daoCustomer.checkCustomerExistByPhone(phone) != null) {
                request.setAttribute("phoneError", "Phone number already exists");
                isValid = false;
            }

            if (daoCustomer.checkCustomerExistByUsername(cusUsername) != null) {
                request.setAttribute("usernameError", "Username already exists");
                isValid = false;
            }

            // check validate
            if (!InputValidator.validateEmail(email)) {
                request.setAttribute("emailError", "Please enter a valid email address");
                isValid = false;
            }

            if (!InputValidator.validatePhone(phone)) {
                request.setAttribute("phoneError", "Please enter a valid phone number");
                isValid = false;
            }

            if (!InputValidator.validateUsername(cusUsername)) {
                request.setAttribute("usernameError", "Username must be between 6 and 20 characters");
                isValid = false;
            }

            if (!InputValidator.validatePassword(cusPassword)) {
                request.setAttribute("passwordError", "Password must be between 6 and 20 characters");
                isValid = false;
            }
            if (!cusPassword.equals(rePassword)) {
                request.setAttribute("rePasswordError", "Passwords do not match");
                isValid = false;
            }

            //sendredict
            if (isValid) {
                HttpSession session = request.getSession();
                session.setAttribute("registrationSuccess", true);
                response.sendRedirect(request.getContextPath() + "/login");

                //create new account
                daoCustomer.register(cusName, email, phone, cusUsername, Encode.hashPassword(cusPassword));

                String serverAddress = request.getServerName() + ":" + request.getServerPort();
                String verifyLink = "http://" + serverAddress + request.getContextPath() + "/VerifyEmailHandler?key=" + CryptoUtils.encrypt(email) + "&user="+ CryptoUtils.encrypt(cusUsername);
                // Tạo một Runnable để gửi email
                Runnable emailTask = () -> {
                    String to = email;
                    String subject = "Confirm Email";
                    String content = "<p>You or someone else used email to create the " + cusUsername + " account!</p>"
                            + "<p>Please access this link: <a href='" + verifyLink + "'>" + verifyLink + "</a> to activate your account</p>"
                            + "<br>"
                            + "<p>Note: The link can only be used once time. After the above time!</p>";

                    String user = "vitaminshare2022@gmail.com"; // Update with your email address
                    String pass = "odhv cisw gmgk bbjl"; // Update with your email password

                    try {
                        SendMail.send(to, subject, content, user, pass);
                        System.out.println("successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                };

                // Thực thi tác vụ gửi email trong một thread riêng biệt
                Thread emailThread = new Thread(emailTask);
                emailThread.start();

            } else {
                request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
            }
        } catch (Exception e) {
        }
    }
}
