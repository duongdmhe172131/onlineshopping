/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import dal.CustomerDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import model.Customer;
import util.CaptchaGenerator;
import util.CryptoUtils;

/**
 *
 * @author BEAN
 */
public class VerifyEmailHandler extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String decryptKey = null;
        String key = null;
        String cusUsername = null;
        String user = null;
        try {
            key = request.getParameter("key");
            cusUsername = request.getParameter("user");
            decryptKey = CryptoUtils.decrypt(key);
            user = CryptoUtils.decrypt(cusUsername);
        } catch (Exception e) {
            e.getStackTrace();
            e.getStackTrace();
        }
        System.out.println(key + "   " + decryptKey + " " + user);
        HttpSession session = request.getSession();

        session.setAttribute("user", user);
        session.setAttribute("email", decryptKey);
        request.getRequestDispatcher("jsp/confirmEmail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        String realCaptcha = (String) session.getAttribute("captchaText");

        String email = request.getParameter("email");

        String captcha = request.getParameter("captcha");

        System.out.println(email);
        if (captcha != null && captcha.equals(realCaptcha)) {
            // Captcha đúng, tiếp tục xử lý
            CustomerDAO daoCustomer = new CustomerDAO();
            Customer customer = daoCustomer.verifyEmail(email);
            if (customer != null) {
                daoCustomer.updateEmailVerificationStatus(customer.getCusEmail(), 1);
                response.sendRedirect("login");
                session.setAttribute("alertMessage", true);
//                session.removeAttribute("email");
//                session.removeAttribute("user");
            } else {
                response.getWriter().println("Failed");
            }
        } else {
            // Captcha sai, thông báo lỗi
            request.setAttribute("message", "Captcha incorrect");
            request.getRequestDispatcher("jsp/confirmEmail.jsp").forward(request, response);
        }
    }

}
