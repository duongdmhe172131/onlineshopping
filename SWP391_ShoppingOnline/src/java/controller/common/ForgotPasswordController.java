/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import dal.CustomerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Random;
import model.Customer;
import util.Encode;
import util.SendMail;
/////////
/**
 *
 * @author BEAN
 */
public class ForgotPasswordController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>PO
     *
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String service = request.getParameter("service");
        if (service == null) {
            service = "forgotPassword";
            request.getRequestDispatcher("jsp/forgotPassword.jsp").forward(request, response);
        }
        if (service.equals("validateOtp")) {
            request.getRequestDispatcher("jsp/enterOtp.jsp").forward(request, response);
        }
        if (service.equals("resetPassword")) {
            request.getRequestDispatcher("jsp/newPassword.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String service = request.getParameter("service");
        if (service == null) {
            service = "forgotPassword";
        }
        if (service.equals("forgotPassword")) {
            String email = request.getParameter("email");
            CustomerDAO daoCustomer = new CustomerDAO();
            Customer customer = daoCustomer.checkCustomerExistByEmail(email);
            if (customer == null) {
                request.setAttribute("message", "Email is not Exist");
                request.getRequestDispatcher("jsp/forgotPassword.jsp").forward(request, response);
            } else {
                String sendTo = customer.getCusEmail();

                HttpSession session = request.getSession();
                session.setAttribute("emailResetPassword", sendTo);
                String subject = "Reset Password";
                String otp = SendMail.generateOTP(6);
                session.setAttribute("generatedOTP", otp);
                String content = "<h1>iLocalShop Reset Password</h1>"
                        + "<p>Your OTP (One Time Password) is: <strong>" + otp + "</strong></p>"
                        + "<p>Please use this OTP to reset your password.</p>";
                String user = "vitaminshare2022@gmail.com";
                String password = "odhv cisw gmgk bbjl";
                SendMail.send(sendTo, subject, content, user, password);
                request.getRequestDispatcher("jsp/enterOtp.jsp").forward(request, response);
            }
        }
        if (service.equals("validateOtp")) {
            String userEnteredOTP = request.getParameter("otp");
            HttpSession session = request.getSession();
            String generatedOTP = (String) session.getAttribute("generatedOTP");
            if (userEnteredOTP.equals(generatedOTP)) {
                response.sendRedirect("forgotPassword?service=resetPassword");
                session.removeAttribute("generatedOTP");
            } else {
                request.setAttribute("message", "Invalid OTP. Please try again.");
                request.getRequestDispatcher("jsp/enterOtp.jsp").forward(request, response);                                                                                                        
            }
        }
        if (service.equals("resetPassword")) {
            String newPassword = request.getParameter("newPassword");
            String confirmPassword = request.getParameter("confirmPassword");
            HttpSession session = request.getSession();
            String emailResetPassword = (String) session.getAttribute("emailResetPassword");
            
            if (!newPassword.equals(confirmPassword)) {
                request.setAttribute("message", "Passwords do not match. Please try again!");
                request.getRequestDispatcher("jsp/newPassword.jsp").forward(request, response);
            } else if (newPassword.length() < 8 || newPassword.length() > 20) {
                request.setAttribute("message", "Password must be between 8 and 20 characters.");
                request.getRequestDispatcher("jsp/newPassword.jsp").forward(request, response);
            } else {
                CustomerDAO daoCustomer = new CustomerDAO();
                //hashing 
                daoCustomer.changePassword(emailResetPassword,Encode.hashPassword(newPassword));
                response.sendRedirect("home");
            }
        }
    }
}
