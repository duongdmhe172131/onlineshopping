/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.admin;

import dal.AdminDAO;
import dal.RoleDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Admin;
import model.Role;

/**
 *
 * @author ASUS PC
 */
public class AdminDetailsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     * 
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminDetailsController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminDetailsController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
    // + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * 
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int adID = Integer.parseInt(request.getParameter("id"));
        AdminDAO adDAO = new AdminDAO();
        Admin ad = adDAO.getAdminByAdID(adID);
        RoleDAO daoRole = new RoleDAO();
        List<Role> listRole = daoRole.getAllRoles();
        request.setAttribute("listRole", listRole);
        request.setAttribute("status", 1);
        request.setAttribute("data", ad);
        request.getRequestDispatcher("/admin-adminDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * 
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AdminDAO adDAO = new AdminDAO();
        try {
            Admin ad = new Admin();
            ad.setAdId(Integer.parseInt(request.getParameter("id")));
            ad.setAdFirstName(request.getParameter("firstName"));
            ad.setAdLastName(request.getParameter("lastName"));
            ad.setAdEmail(request.getParameter("email"));
            ad.setAdPhone(request.getParameter("phone"));
            ad.setAdAddress(request.getParameter("address"));
            ad.setAdRoleId(Integer.parseInt(request.getParameter("roleId")));
            String dob = request.getParameter("dob");
            ad.setAdDob(Date.valueOf(dob));
            ad.setAdStatus(Integer.parseInt(request.getParameter("status")));
            adDAO.updateAdmin(ad);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        response.sendRedirect("./manage");

    }

    /**
     * Returns a short description of the servlet.
     * 
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
