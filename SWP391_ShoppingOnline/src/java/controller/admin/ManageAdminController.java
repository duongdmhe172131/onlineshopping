/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dal.AdminDAO;
import dal.CustomerDAO;
import dal.RoleDAO;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Admin;
import model.Customer;
import model.Role;
import util.Encode;
import util.InputValidator;

/**
 *
 * @author BEAN
 */
public class ManageAdminController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageUserController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageUserController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
    // + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String search = "";
        int roleId = -1;
        int status = -1;
        int pageNo = 1;
        final int pageSize = 10;
        try {

            search = request.getParameter("search") == null ? "" : request.getParameter("search");
            roleId = request.getParameter("roleId") == null ? -1 : Integer.parseInt(request.getParameter("roleId"));
            status = request.getParameter("status") == null ? -1 : Integer.parseInt(request.getParameter("status"));
            pageNo = request.getParameter("pageNo") == null ? 1 : Integer.parseInt(request.getParameter("pageNo"));

        } catch (Exception e) {
        }

        AdminDAO daoAdmin = new AdminDAO();
        List<Admin> listAdmin = daoAdmin.getListAdminByFilter(roleId, status, search, pageNo, pageSize);
        int totalPage = daoAdmin.getTotalPage(roleId, status, search, pageSize);
        RoleDAO daoRole = new RoleDAO();
        List<Role> listRole = daoRole.getAllRoles();

        request.setAttribute("search", search);
        request.setAttribute("roleId", roleId);
        request.setAttribute("status", status);
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("currentPage", pageNo);

        request.setAttribute("listUser", listAdmin);
        request.setAttribute("listRole", listRole);
        request.getRequestDispatcher("/admin-manager-account.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String service = request.getParameter("service");
        AdminDAO adminDAO = new AdminDAO();

        if (service == null) {
            service = "displayListAdmin";
        }
        if (service.equals("searchName")) {
            String name = request.getParameter("name");

            if (name == null || name.isEmpty()) {
                ArrayList<Admin> listAdmin = adminDAO.getListAdmin();
                request.setAttribute("listAdmin", listAdmin);
                request.getRequestDispatcher("manageAdmin.jsp").forward(request, response);
            } else {
                ArrayList<Admin> adminSearch = adminDAO.searchAdminByName(name);
                request.setAttribute("listAdmin", adminSearch);
                request.getRequestDispatcher("manageAdmin.jsp").forward(request, response);
            }
        }

        if (service.equals("filterByRoleID")) {
            int roleID = Integer.parseInt(request.getParameter("roleID"));
            if (roleID == 0) {
                ArrayList<Admin> listAdmin = adminDAO.getListAdmin();
                request.setAttribute("listAdmin", listAdmin);
                request.getRequestDispatcher("manageAdmin.jsp").forward(request, response);
            } else {
                ArrayList<Admin> admin = adminDAO.filterByRoleID(roleID);
                request.setAttribute("listAdmin", admin);
                request.getRequestDispatcher("manageAdmin.jsp").forward(request, response);
            }
        }

        if (service.equals("addNewAdmin")) {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");
            String phone = request.getParameter("phone");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            int roleID = Integer.parseInt(request.getParameter("add-roleId"));
            if (adminDAO.checkAdminExistByUsername(username) != null || adminDAO.checkAdminExistEmail(email) != null) {
                response.sendRedirect("manage?exist=2");
                return;
            }
            if (!InputValidator.validateEmail(email) || !phone.matches("\\d+") || phone.length() != 10
                    || !InputValidator.validateUsername(username) || !InputValidator.validatePassword(password)) {
                response.sendRedirect("manage?error=1");
                return;
            }
            adminDAO.addNewAdmin(lastName, firstName, email, phone, username, Encode.hashPassword(password), roleID, 0);
            //adminDAO.addNewAdmin(lastName, firstName, email, phone, username, password, roleID, 0);

            response.sendRedirect("manage");
        }

        if (service.equals("updateStatusAdmin")) {
            int adID = Integer.parseInt(request.getParameter("status"));
            int status = Integer.parseInt(request.getParameter("adId"));
            adminDAO.updateStatusAdmin(adID, status);
            response.sendRedirect("manage-admin");
        }

        if (service.equals("loadAdmin")) {
            int adID = Integer.parseInt(request.getParameter("adId"));
            Admin admin = adminDAO.getAdminByAdID(adID);
            request.setAttribute("detail", admin);
            request.getRequestDispatcher("manageAdmin.jsp").forward(request, response);
        }

        if (service.equals("editAdmin")) {
            String firtName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");
            int roldId = Integer.parseInt(request.getParameter("roleId"));
            int adID = Integer.parseInt(request.getParameter("adId"));
            adminDAO.update(firtName, lastName, email, roldId, adID);
            response.sendRedirect("manage-admin");
        }

    }

}
