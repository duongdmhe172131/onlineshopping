/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dal.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import model.Admin;
import util.InputValidator;

/**
 *
 * @author BEAN
 */
@MultipartConfig
public class AdminProfileController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminProfileController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminProfileController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Admin ad = (Admin) session.getAttribute("admin");
        AdminDAO daoAdmin = new AdminDAO();
        //check session
        if (ad == null) {
            request.getRequestDispatcher("../admin/login").forward(request, response);
        } else {
            request.setAttribute("ad", daoAdmin.getAdminByAdID(ad.getAdId()));
            request.getRequestDispatcher("/admin-profile.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        String fileName = "";
//        
//        Part filePart = request.getPart("adminImage");
//
//        // Get the filename
//         fileName = getSubmittedFileName(filePart);
//
//        // Define the directory where the file will be saved
//        String uploadPath = getServletContext().getRealPath("") + File.separator + "img";
//        File uploadDir = new File(uploadPath);
//
//        if (!uploadDir.exists()) {
//            uploadDir.mkdir();
//        }
//
//        // Save the file to the server
//        try (InputStream input = filePart.getInputStream();
//             OutputStream output = new FileOutputStream(uploadPath + File.separator + fileName)) {
//            int read;
//            byte[] buffer = new byte[1024];
//            while ((read = input.read(buffer)) != -1) {
//                output.write(buffer, 0, read);
//            }
//        }
        
        HttpSession session = request.getSession();
        Admin ad = (Admin) session.getAttribute("admin");
        String adminFirstName = request.getParameter("adminFirstName");
        String adminLastName = request.getParameter("adminLastName");
        String adminAddress = request.getParameter("adminAddress");
        String adminUsername = request.getParameter("adminUsername");
        ad.setAdFirstName(adminFirstName);
        ad.setAdLastName(adminLastName);
        ad.setAdAddress(adminAddress);
        ad.setAdUsername(adminUsername);
        ad.setAdImage("");
        AdminDAO daoAdmin = new AdminDAO();
        if (InputValidator.isValidName(adminFirstName) && InputValidator.isValidName(adminLastName)) {
            daoAdmin.updateProfileAdmin("", adminFirstName, adminLastName, adminAddress, adminUsername);
            session.removeAttribute("admin");
            session.setAttribute("admin", ad);
            session.setAttribute("alertMessage", true);
            response.sendRedirect("profile");
        } else {
            request.setAttribute("message", "First Name and Last Name can't contain special character!");
            request.getRequestDispatcher("/admin-profile.jsp").forward(request, response);
        }
    }
    
      private String getSubmittedFileName(Part part) {
        String header = part.getHeader("content-disposition");
        String[] elements = header.split(";");
        for (String element : elements) {
            if (element.trim().startsWith("filename")) {
                return element.substring(element.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return "";
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
}
