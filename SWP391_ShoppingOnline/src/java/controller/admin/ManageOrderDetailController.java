/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.admin;

import dal.OrderDAO;
import dal.ProductDAO;
import dal.StatusOrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Item;
import model.Order;
import model.StatusOrder;

/**
 *
 * @author windy
 */
public class ManageOrderDetailController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageOrderDetailController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageOrderDetailController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       OrderDAO daoOrder = new OrderDAO();
        ProductDAO daoProduct = new ProductDAO();
        ArrayList<Item> listItem = new ArrayList<Item>();
        StatusOrderDAO daoStatusOrder = new StatusOrderDAO();
        
        String orderID = request.getParameter("orderID");
        Order order = daoOrder.getOrderByOrderID(orderID);
        listItem = daoProduct.getListItemByOrderID(orderID);
        List<StatusOrder> listStatusOrder = daoStatusOrder.getStatusOrderForSale();
        
        request.setAttribute("order", order);
        request.setAttribute("orderID", orderID);
        request.setAttribute("listItem", listItem);
        request.setAttribute("listStatusOrder", listStatusOrder);
        request.getRequestDispatcher("/manageOrderDetail.jsp").forward(request, response);
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        boolean checkUpdate = false;
        int statusOrder = -1;
        OrderDAO daoOrder = new OrderDAO();
        
        String status = request.getParameter("status");
        System.out.println("status can tim ne: " + status);
        String orderID = request.getParameter("orderID");
        
        if(status != null || !status.isEmpty()){
            statusOrder = Integer.parseInt(status);
        }
        
        checkUpdate = daoOrder.updateOrderSoID(orderID, statusOrder);
        
        if(checkUpdate){
            request.setAttribute("orderID", orderID);
            doGet(request, response);
        } 
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
