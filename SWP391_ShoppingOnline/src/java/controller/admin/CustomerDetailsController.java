/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;

import dal.AdminDAO;
import dal.CustomerDAO;
import dal.RoleDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Admin;
import model.Customer;
import model.Role;
import util.InputValidator;

/**
 *
 * @author ASUS PC
 */
public class CustomerDetailsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     * 
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CustomerDetailsController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CustomerDetailsController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
    // + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * 
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cusID = request.getParameter("id");
        CustomerDAO dao = new CustomerDAO();
        Customer customer = dao.getCustomerByID(cusID);
        request.setAttribute("status", 1);
        request.setAttribute("data", customer);
        request.getRequestDispatcher("/admin-customerDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * 
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int cusID = Integer.parseInt(request.getParameter("id"));
        try {
            CustomerDAO dao = new CustomerDAO();
            Customer customer = new Customer();
            String contactName = request.getParameter("contactName");
            Date dob = Date.valueOf("1999-01-01");
            if (request.getParameter("dob") != null) {
                dob = Date.valueOf(request.getParameter("dob"));
            }
            String address = request.getParameter("address");
            String email = request.getParameter("email");
            String phone = request.getParameter("phone");
            String username = request.getParameter("username");
            int status = Integer.parseInt(request.getParameter("status"));

            customer.setCusContactName(contactName);
            customer.setCusDob(dob);
            customer.setCusAddress(address);
            customer.setCusEmail(email);
            customer.setCusPhone(phone);
            customer.setCusUsername(username);
            customer.setCusStatus(status);
            customer.setCusId(cusID);

            dao.updateCustomer(customer);
            response.sendRedirect("customer?id=" + cusID);
            return;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        response.sendRedirect("customer-details?id=" + cusID + "&error=1");
    }

    /**
     * Returns a short description of the servlet.
     * 
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
