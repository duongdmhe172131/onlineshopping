package controller.customer;

import dal.OrderDAO;
import dal.ProductOfOrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Order;
import model.ProductOfOrder;

/**
 *
 * @author anhdu
 */
@WebServlet(name = "PurchaseController", urlPatterns = {"/PurchaseController","/purchase"})
public class PurchaseController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PaymentController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PaymentController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String ordId = request.getParameter("ordId");
        
        Order order = new OrderDAO().getOrderByOrderID(ordId);
        double amount = 500000;
        int price = 500000;
        
        if(order.getRawNote().contains("💵")) {
            amount = order.getOrderTotalPrice();
            price = (int) order.getOrderTotalPrice();
        }
        
        //get order details
        ProductOfOrderDAO daoProductOfOrder = new ProductOfOrderDAO();
        ArrayList<ProductOfOrder> productOfOrderList = new ArrayList<>();
        productOfOrderList = daoProductOfOrder.getProductOfOrderbyProID(ordId);
        
        request.setAttribute("order", order);
        request.setAttribute("amount", String.format("%,d", (int)amount));
        request.setAttribute("price", price);
        request.setAttribute("ordId", ordId);
        request.setAttribute("productOfOrderList", productOfOrderList);
        request.getRequestDispatcher("vnpay_pay.jsp").forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
