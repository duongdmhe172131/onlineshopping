/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.CustomerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Customer;
import util.InputValidator;

/**
 *
 * @author Dell
 */
public class UpdateAddressController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateAddressController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateAddressController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CustomerDAO daoCustomer = new CustomerDAO();
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        String cusAddress = customer.getCusAddress();
        String[] cuttedAddres = daoCustomer.cutAddress(cusAddress);

        if (request.getParameter("error-phone") != null) {
            request.setAttribute("msg", "Invalid phone!");
        }

        if (cusAddress != null && !cusAddress.equals("..")) {

            try {
                request.setAttribute("addressDetail", cuttedAddres[0]);
                request.setAttribute("city", cuttedAddres[1]);
                request.setAttribute("district", cuttedAddres[2]);
                request.setAttribute("ward", cuttedAddres[3]);
            } catch (Exception e) {
                System.out.println("UpdateAddresController: " + e);
            }

            request.getRequestDispatcher("jsp/updateAddress.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("jsp/updateAddress.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        CustomerDAO daoCustomer = new CustomerDAO();

        Customer customer = (Customer) session.getAttribute("customer");
        String cusName = (String) request.getParameter("cusName");
        String addressDetail = (String) request.getParameter("cusAddress");
        String cusAddress = (String) request.getParameter("city");
        String city = (String) request.getParameter("district");
        String province = (String) request.getParameter("ward");
        String cusPhone = (String) request.getParameter("cusPhone");
        String image = (String) request.getParameter("image");

        if (!new InputValidator().validatePhone(cusPhone)) {
            response.sendRedirect("updateAddress?error-phone");
            return;
        }

        String cusFullAddress = addressDetail + "." + cusAddress + "." + city + "." + province;

        daoCustomer.updateAddress(cusFullAddress, customer.getCusId());
        daoCustomer.updateCusName(cusName, customer.getCusId());
        daoCustomer.updatePhone(cusPhone, customer.getCusId());
        daoCustomer.updateImage(image, customer.getCusId());

        request.getSession().setAttribute("customer", daoCustomer.checkCustomerExistByUsername(customer.getCusUsername()));

//        request.getRequestDispatcher("jsp/address.jsp").forward(request, response);
        response.sendRedirect("address");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
