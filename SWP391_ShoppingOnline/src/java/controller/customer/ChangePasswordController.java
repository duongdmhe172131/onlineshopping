/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common.ProfileController;

import dal.CustomerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Customer;
import util.Encode;

/**
 *
 * @author Dell
 */
public class ChangePasswordController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePasswordController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePasswordController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("jsp/changePassword.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        CustomerDAO daoCustomer = new CustomerDAO();

        Customer customer = (Customer) session.getAttribute("customer");
        String curPass = (String) request.getParameter("curPassword");
        String newPass = request.getParameter("newPassword");
        String reEnterPass = request.getParameter("reEnterPassword");
        String msg = "";
        String success = "";

        if (curPass != null && newPass != null && reEnterPass != null) {

            if (customer.getCusPassword().equals(Encode.hashPassword(curPass)) && !curPass.equals(newPass)
                    && newPass.equals(reEnterPass) && !(newPass.length() < 8 || newPass.length() > 20)) {
                daoCustomer.changePassword(customer.getCusEmail(), Encode.hashPassword(newPass));
                success = "Changed password successfully!";
                System.out.println(success);
                request.setAttribute("success", success);
                request.getRequestDispatcher("jsp/changePassword.jsp").forward(request, response);
            }else if (newPass.length() < 8 || newPass.length() > 20) {
                msg = "Password must be between 8 and 20 characters.";
                request.setAttribute("showCaptcha", true);
                request.setAttribute("msg", msg);
//                System.out.println(msg);
                request.getRequestDispatcher("jsp/changePassword.jsp").forward(request, response);
            }  
            else if (newPass.equals(curPass)) {
                msg = "Current password and new password cannot be the same, please try again.";
                request.setAttribute("showCaptcha", true);
                request.setAttribute("msg", msg);
//                System.out.println(msg);
                request.getRequestDispatcher("jsp/changePassword.jsp").forward(request, response);
            } else if (!newPass.equals(reEnterPass)) {
                msg = "New password and re-enter password are not match, please try again.";
                request.setAttribute("showCaptcha", true);
                request.setAttribute("msg", msg);
//                System.out.println(msg);
                request.getRequestDispatcher("jsp/changePassword.jsp").forward(request, response);
            } else {
                msg = "Current password is not match, please try again.";
                request.setAttribute("showCaptcha", true);
                request.setAttribute("msg", msg);
//                System.out.println(msg);
                request.getRequestDispatcher("jsp/changePassword.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
