/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.CustomerDAO;
import dal.OrderDAO;
import dal.OrderDetailDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Customer;
import model.Item;
import model.Order;
import util.InputValidator;

/**
 *
 * @author windy
 */
@WebServlet(name = "OrderController", urlPatterns = {"/order"})
public class OrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CustomerDAO daoCustomer = new CustomerDAO();
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");

        Cookie[] listCookie = request.getCookies();
        String txt = "";
        ArrayList<Item> listItem = new ArrayList<>();
        ProductDAO daoProduct = new ProductDAO();

        if (listCookie != null) {
            for (Cookie cookie : listCookie) {
                if (cookie.getName().equals("cart" + customer.getCusId())) {
                    txt += cookie.getValue();
                }
            }
        }

        if (txt.isEmpty()) {
            response.sendRedirect("cart");
            return;
        }

        listItem = daoProduct.getListItem(txt, listItem);
        float price = 0;

        for (Item item : listItem) {
            price += item.getProPrice() * item.getQuantity();
        }

        if (request.getParameter("error-phone") != null) {
            request.setAttribute("msg", "Invalid phone!");

        }


        try {
            String[] address = customer.getCusAddress().split("\\.");
            request.setAttribute("addressDetail", address[0]);
            request.setAttribute("city", address[1]);
            request.setAttribute("district", address[2]);
            request.setAttribute("ward", address[3]);

        } catch (Exception e) {
            System.out.println(e);
        }

        request.setAttribute("price", price);
        request.setAttribute("customer", customer);
        request.getRequestDispatcher("cartContact.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CustomerDAO daoCustomer = new CustomerDAO();
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");

        String price = request.getParameter("price");
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String note = request.getParameter("note");

        String addressDetail = request.getParameter("address");
        String city = request.getParameter("city");
        String district = request.getParameter("district");
        String ward = request.getParameter("ward");
        String address = addressDetail + "." + city + "." + district + "." + ward;

        //check payment method
        if (request.getParameter("paymentMethod").equals("fullpayment")) {
            note = "💵" + note;
        }

        //check phone
        if (!new InputValidator().validatePhone(phone)) {
            response.sendRedirect("order?error-phone");
            return;
        }

        Order order = new Order();

        order.setOrderCusID(customer.getCusId());
        order.setOrderTotalPrice(Float.parseFloat(price));
        order.setOrderContactName(name);
        order.setOrderPhone(phone);
        order.setOrderAddress(address);
        order.setOrderSoID(1);
        order.setOrderAdID(3); // default
        order.setOrderNote(note);

        int orderId = new OrderDAO().insertOrder(order);

        Cookie[] listCookie = request.getCookies();
        String txt = "";
        ArrayList<Item> listItem = new ArrayList<>();
        ProductDAO daoProduct = new ProductDAO();

        if (listCookie != null) {
            for (Cookie cookie : listCookie) {
                if (cookie.getName().equals("cart" + customer.getCusId())) {
                    txt += cookie.getValue();
                    cookie.setMaxAge(0);
                    Cookie new_cookie = new Cookie("cart" + customer.getCusId(), "");
                    response.addCookie(new_cookie);
                }
            }
        }

        listItem = daoProduct.getListItem(txt, listItem);
        for (Item item : listItem) {
            new OrderDetailDAO().insertOrderDetail(orderId, item.getPdID(), item.getQuantity(), item.getProPrice() * item.getQuantity());
        }

        response.sendRedirect("purchase?ordId=" + orderId);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
