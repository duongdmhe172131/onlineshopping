/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Customer;
import model.Item;

/**
 *
 * @author windy
 */
@WebServlet(name = "CartController", urlPatterns = {"/cart"})
public class CartController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CartController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CartController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Customer c = (Customer) session.getAttribute("customer");
        String cusName = c.getCusContactName().replaceAll(" ", "");
        String cusID = String.valueOf(c.getCusId());
        Cookie[] listCookie = request.getCookies();
        String txt = "";
        ArrayList<Item> listItem = new ArrayList<>();
        ProductDAO daoProduct = new ProductDAO();

        if (c == null) {
            response.sendRedirect("login.jsp");
        }

        String pdID = request.getParameter("pdID");
        String proID = request.getParameter("proID");

        //Customer access to cart
        if (pdID == null || pdID.isEmpty()) {

            //check list cookies
            if (listCookie != null) {
                //get cookie of customer
                for (Cookie cookie : listCookie) {
                    if (cookie.getName().equals("cart" + c.getCusId())) {
                        txt += cookie.getValue();
                    }
                }
            }

            if (txt.equals("")) {
                listItem = null;
                response.sendRedirect("cart.jsp");
            } else {
                listItem = daoProduct.getListItem(txt, listItem);
                request.setAttribute("listItem", listItem);
                request.setAttribute("cusID", cusID);
                request.getRequestDispatcher("cart.jsp").forward(request, response);
            }

        } else {
            //Customer add to cart
            //check list cookies
            if (listCookie != null) {
                //get cookie of customer
                for (Cookie cookie : listCookie) {
                    if (cookie.getName().equals("cart" + c.getCusId())) {
                        txt += cookie.getValue();
                        cookie.setMaxAge(0);
                    }
                }
            }
            txt = daoProduct.addItemToCookie(txt, pdID);
            request.setAttribute("message", "message");
            Cookie new_cookie = new Cookie("cart" + c.getCusId(), txt);
            new_cookie.setMaxAge(60 * 60 * 24 * 30);
            response.addCookie(new_cookie);

           request.getRequestDispatcher("productdetail?proID=" + proID).forward(request, response);
        }

//        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO daoProduct = new ProductDAO();
        ArrayList<Item> listItem = new ArrayList<>();
        Cookie[] listCookie = request.getCookies();
        HttpSession session = request.getSession();
        Customer c = (Customer) session.getAttribute("customer");
        String cusName = c.getCusContactName().replaceAll(" ", "");
        String pdID = request.getParameter("pdID");
        String action = request.getParameter("action");
        String deleteStatus = request.getParameter("delete");
        String emptyCart = request.getParameter("deleteAll");
        String txt = "";

        if (listCookie != null) {
            for (Cookie cookie : listCookie) {
                if (cookie.getName().equals("cart" + c.getCusId())) {
                    txt += cookie.getValue();
                    cookie.setMaxAge(0);
                }
            }
        }
       
        if(!(emptyCart == null || emptyCart.isEmpty())){
            txt = "";
        } else
        if (deleteStatus == null || deleteStatus.isEmpty()) {
            txt = daoProduct.updateQuantity(txt, pdID, action);
        } else {
            txt = daoProduct.deleteProduct(txt, pdID);
        }

        Cookie new_cookie = new Cookie("cart" + c.getCusId(), txt);
        new_cookie.setMaxAge(60 * 60 * 24 * 30);
        response.addCookie(new_cookie);

        if (txt.equals("")) {
            listItem = null;
            response.sendRedirect("cart.jsp");
        } else {
            listItem = daoProduct.getListItem(txt, listItem);
            request.setAttribute("listItem", listItem);
            request.getRequestDispatcher("cart.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
