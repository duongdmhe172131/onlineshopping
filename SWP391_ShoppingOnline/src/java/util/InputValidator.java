/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import java.sql.Date;
import java.util.regex.Pattern;

/**
 *
 * @author BEAN
 */
public class InputValidator {

    public static boolean validatePhone(String phone) {
        if (phone.startsWith("0")) {
            return phone.matches("\\d{10}"); // phone in Vn
        } else if (phone.startsWith("84")) {
            return phone.matches("84[3-9]\\d{8}");// phone global
        }
        return false;
    }

    public static boolean validateEmail(String email) {
        String emailRegex = "^[A-Za-z0-9+_.-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
        return email.matches(emailRegex);
    }

    public static boolean validatePassword(String password) {
        return password.length() >= 6 && password.length() <= 20 && !password.contains(" ");
    }

    public static boolean validateUsername(String username) {
        return username.length() >= 6 && username.length() <= 20 && !username.contains(" ");
    }

    public static boolean validateDob(Date dob) {
        return dob.before(new Date(System.currentTimeMillis()));
    }
    
     public static boolean isValidName(String name) {
        // Kiểm tra xem name có rỗng không
        if (name.isEmpty()) {
            return false;
        }

        // Sử dụng regex để kiểm tra
        String regex = "^[a-zA-Z ]+$"; // Chấp nhận chữ cái và dấu cách

        // Tạo một Pattern từ regex
        Pattern pattern = Pattern.compile(regex);

        // Kiểm tra name
        return pattern.matcher(name).matches();
    }

}
