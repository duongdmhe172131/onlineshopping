/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import java.util.*;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author BEAN
 */
public class SendMail {

    public static void send(String to, String subject, String content, String user, String pass) {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }

        });

        try {
            Message message = new MimeMessage(session);
            try {
                message.setFrom(new InternetAddress(user, "iLocalShop"));

            } catch (Exception e) {
            }
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            //set subject and body text  
            message.setSubject(subject);
            message.setContent(content, "text/html;charset=UTF-8");

            //set some other header information
            message.setSentDate(new Date());
            message.saveChanges();

            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public static String generateOTP(int length) {
        String numbers = "0123456789";
        Random random = new Random();
        StringBuilder otp = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(numbers.length());
            otp.append(numbers.charAt(index));
        }
        return otp.toString();
    }

    public static String generateRandomPassword(int length) {
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lower = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String specialCharacters = "@#%_";

        String allowedCharacters = upper + lower + numbers + specialCharacters;
        Random random = new Random();

        StringBuilder password = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(allowedCharacters.length());
            password.append(allowedCharacters.charAt(randomIndex));
        }

        return password.toString();
    }

    public static void sendPassword(String email, String name, String password) {
        Runnable emailTask = () -> {
            String to = email;
            String subject = "Password Reset Request";
            String content = "Dear " + name + ",<br><br>"
                    + "We have received a request to reset your password. Your new password has been generated successfully.<br><br>"
                    + "Your new password: <strong>" + password + "</strong><br>" // In đậm mật khẩu
                    + "Please use this new password to log in to your account. We recommend changing your password after logging in for security reasons.<br><br>"
                    + "If you did not initiate this password reset, please contact our support team immediately.<br><br>"
                    + "Thank you for using our services.<br><br>"
                    + "Best regards,";
            String user = "vitaminshare2022@gmail.com"; // Update with your email address
            String pass = "odhv cisw gmgk bbjl"; // Update with your email password

            try {
                SendMail.send(to, subject, content, user, pass);
                System.out.println("successful");
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        // Thực thi tác vụ gửi email trong một thread riêng biệt
        Thread emailThread = new Thread(emailTask);
        emailThread.start();
    }

    public static void main(String[] args) {
        System.out.println(generateRandomPassword(8));
    }
}
