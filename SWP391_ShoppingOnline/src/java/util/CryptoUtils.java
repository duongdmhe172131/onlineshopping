package util;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class CryptoUtils {

    private static final String AES_SECRET_KEY = "aBcDeFgH12345678"; // Thay thế bằng khóa bí mật của bạn

    public static String encrypt(String input) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        SecretKeySpec secretKeySpec = new SecretKeySpec(AES_SECRET_KEY.getBytes(), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] encoded = cipher.doFinal(input.getBytes());
        return Base64.getUrlEncoder().withoutPadding().encodeToString(encoded);
    }

    public static String decrypt(String encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        SecretKeySpec secretKeySpec = new SecretKeySpec(AES_SECRET_KEY.getBytes(), "AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        byte[] original = cipher.doFinal(Base64.getUrlDecoder().decode(encrypted));
        return new String(original);
    }

    public static void main(String[] args) {
        try {
            System.out.println(encrypt("duongdmhe172131@gmail.com"));
            System.out.println(decrypt("smazLnm4l2oU3vWjWdvS-tc_nyhgOaOAIJuMSEUJnLQ"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
