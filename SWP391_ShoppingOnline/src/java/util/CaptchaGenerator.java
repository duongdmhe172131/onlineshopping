
package util;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;

public class CaptchaGenerator {

    private static final int WIDTH = 90;
    private static final int HEIGHT = 50;

    public static BufferedImage generateCaptcha() {
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();

        // Tạo nền trắng
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, WIDTH, HEIGHT);

        // Tạo ký tự số ngẫu nhiên
        Random random = new Random();
        String captchaText = String.valueOf(random.nextInt(10)) + String.valueOf(random.nextInt(10)) +String.valueOf(random.nextInt(10)) + String.valueOf(random.nextInt(10));

        // Thiết lập font và vẽ ký tự số
        g.setFont(new Font("Arial", Font.BOLD, 24));
        g.setColor(Color.WHITE);
        g.drawString(captchaText, 10, 30);

        // Ngẫu nhiên tạo một số đường kẻ ngẫu nhiên để làm phức tạp hơn

        return image;
    }
    
  public static String extractCaptchaValue(BufferedImage captchaImage) {
        int width = captchaImage.getWidth();
        int height = captchaImage.getHeight();
        StringBuilder captchaValue = new StringBuilder();

        for (int x = 10; x < width; x += 30) {
            for (int y = 10; y < height - 10; y += 30) {
                int pixel = captchaImage.getRGB(x, y);
                if ((pixel & 0x00FFFFFF) != 0) {
                    captchaValue.append((x - 10) / 30);
                }
            }
        }

        return captchaValue.toString();
    }
   

}
