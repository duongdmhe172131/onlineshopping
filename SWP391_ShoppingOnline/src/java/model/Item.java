/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.ProductDetailDAO;

/**
 *
 * @author windy
 */
public class Item {

    private int pdID;
    private int pdProID;
    private String proName;
    private String proImage;
    private float proPrice;
    private String pdColor;
    private int quantity;
    private String pdimgURL;

    public Item() {
    }

    public Item(int pdID, int pdProID, String proName, String proImage, float proPrice, String pdColor, int quantity) {
        this.pdID = pdID;
        this.pdProID = pdProID;
        this.proName = proName;
        this.proImage = proImage;
        this.proPrice = proPrice;
        this.pdColor = pdColor;
        this.quantity = quantity;
    }

    public Item(String proName, String proImage, float proPrice, String pdColor, int quantity, String pdimgURL) {
        this.proName = proName;
        this.proImage = proImage;
        this.proPrice = proPrice;
        this.pdColor = pdColor;
        this.quantity = quantity;
        this.pdimgURL = pdimgURL;
    }

    public Item(String proName, String proImage, float proPrice, String pdColor, int quantity) {
        this.proName = proName;
        this.proImage = proImage;
        this.proPrice = proPrice;
        this.pdColor = pdColor;
        this.quantity = quantity;
    }

    public int getPdID() {
        return pdID;
    }

    public void setPdID(int pdID) {
        this.pdID = pdID;
    }

    public int getPdProID() {
        return pdProID;
    }

    public void setPdProID(int pdProID) {
        this.pdProID = pdProID;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProImage() {
        return new ProductDetailDAO().getProductDetailImage(pdID + "");
    }

    public void setProImage(String proImage) {
        this.proImage = proImage;
    }

    public float getProPrice() {
        return proPrice;
    }

    public void setProPrice(float proPrice) {
        this.proPrice = proPrice;
    }

    public String getPdColor() {
        return pdColor;
    }

    public void setPdColor(String pdColor) {
        this.pdColor = pdColor;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPdimgURL() {
        return pdimgURL;
    }

    public void setPdimgURL(String pdimgURL) {
        this.pdimgURL = pdimgURL;
    }

    @Override
    public String toString() {
        return "Item{" + "pdID=" + pdID + ", pdProID=" + pdProID + ", proName=" + proName + ", proImage=" + proImage + ", proPrice=" + proPrice + ", pdColor=" + pdColor + ", quantity=" + quantity + ", pdimgURL=" + pdimgURL + '}';
    }

}
