/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.CategoryDAO;
import java.sql.Date;

/**
 *
 * @author windy
 */
public class Product {

    private int proID;
    private int proCateID;
    private String proName;
    private float proPrice;
    private String proImage;
    private String proDetail;
    private Date proCreateDate;
    private int proAdID;
    private int proStatus;

    private int averageStar;

    public Date getProCreateDate() {
        return proCreateDate;
    }

    public void setProCreateDate(Date proCreateDate) {
        this.proCreateDate = proCreateDate;
    }

    public int getProAdID() {
        return proAdID;
    }

    public void setProAdID(int proAdID) {
        this.proAdID = proAdID;
    }

    public Product() {
    }

    public Product(int proID, int proCateID, String proName, float proPrice, String proImage, String proDetail, Date proCreateDate, int proAdID, int proStatus, int averageStar) {
        this.proID = proID;
        this.proCateID = proCateID;
        this.proName = proName;
        this.proPrice = proPrice;
        this.proImage = proImage;
        this.proDetail = proDetail;
        this.proCreateDate = proCreateDate;
        this.proAdID = proAdID;
        this.proStatus = proStatus;
        this.averageStar = averageStar;
    }

    public Product(int proID, int proCateID, String proName, float proPrice, String proImage, String proDetail, int proStatus) {
        this.proID = proID;
        this.proCateID = proCateID;
        this.proName = proName;
        this.proPrice = proPrice;
        this.proImage = proImage;
        this.proDetail = proDetail;
        this.proStatus = proStatus;
    }

    public Product(int proID, int proCateID, String proName, float proPrice, String proImage, String proDetail, Date proCreateDate, int proAdID, int proStatus) {
        this.proID = proID;
        this.proCateID = proCateID;
        this.proName = proName;
        this.proPrice = proPrice;
        this.proImage = proImage;
        this.proDetail = proDetail;
        this.proCreateDate = proCreateDate;
        this.proAdID = proAdID;
        this.proStatus = proStatus;
    }
    
    
   

    public int getProID() {
        return proID;
    }

    public void setProID(int proID) {
        this.proID = proID;
    }

    public int getProCateID() {
        return proCateID;
    }

    public void setProCateID(int proCateID) {
        this.proCateID = proCateID;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public float getProPrice() {
        return proPrice;
    }

    public void setProPrice(float proPrice) {
        this.proPrice = proPrice;
    }

    public String getProImage() {
        return proImage;
    }

    public void setProImage(String proImage) {
        this.proImage = proImage;
    }

    public String getProDetail() {
        return proDetail;
    }

    public void setProDetail(String proDetail) {
        this.proDetail = proDetail;
    }

    public int getProStatus() {
        return proStatus;
    }

    public void setProStatus(int proStatus) {
        this.proStatus = proStatus;
    }

    public int getAverageStar() {
        return averageStar;
    }

    public void setAverageStar(int averageStar) {
        this.averageStar = averageStar;
    }


    public String getCateNameByCateID() {
        int cateId = getProCateID();
        String cateName="";
        if (cateId == 1) {
            return cateName = "Apple";
        } else if (cateId == 2) {
            return cateName = "Samsung";
        } else if (cateId == 3) {
            return cateName = "Xiaomi";
        } else if (cateId == 4) {
            return cateName = "Realme";
        } else if (cateId == 5) {
            return cateName = "Oppo";
        } else if (cateId == 6) {
            return cateName = "Vivo";
        }
        return null;
    }

    @Override
    public String toString() {
        return "Product{" + "proID=" + proID + ", proCateID=" + proCateID + ", proName=" + proName + ", proPrice=" + proPrice + ", proImage=" + proImage + ", proDetail=" + proDetail + ", proSold=" + proStatus + '}';
    }

    
 
}
