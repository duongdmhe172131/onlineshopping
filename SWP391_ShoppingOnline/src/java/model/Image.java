/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author huytu
 */
public class Image {
    private int imageID;
    private String imageName;
    private int imageProID;
    private String imageURL;
    private int imageStatus;

    public Image() {
    }

    public Image(int imageID, String imageName, int imageProID, String imageURL, int imageStatus) {
        this.imageID = imageID;
        this.imageName = imageName;
        this.imageProID = imageProID;
        this.imageURL = imageURL;
        this.imageStatus = imageStatus;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public int getImageProID() {
        return imageProID;
    }

    public void setImageProID(int imageProID) {
        this.imageProID = imageProID;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(int imageStatus) {
        this.imageStatus = imageStatus;
    }

    @Override
    public String toString() {
        return "ProductDetail{" + "imageID=" + imageID + ", imageName=" + imageName + ", imageProID=" + imageProID + ", imageURL=" + imageURL + ", imageStatus=" + imageStatus + '}';
    }
    
}
