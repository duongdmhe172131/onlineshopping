/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.text.DecimalFormat;

/**
 *
 * @author Dell
 */
public class Order {

    private int orderID;
    private int orderCusID;
    private Date orderDate;
    private double orderTotalPrice;
    private String orderContactName;
    private String orderPhone;
    private String orderAddress;
    private int orderSoID;
    private int orderAdID;
    private String orderNote;

    public Order() {
    }

    public Order(int orderID, int orderCusID, Date orderDate, double orderTotalPrice, String orderContactName, String orderPhone, String orderAddress, int orderSoID, int orderAdID, String orderNote) {
        this.orderID = orderID;
        this.orderCusID = orderCusID;
        this.orderDate = orderDate;
        this.orderTotalPrice = orderTotalPrice;
        this.orderContactName = orderContactName;
        this.orderPhone = orderPhone;
        this.orderAddress = orderAddress;
        this.orderSoID = orderSoID;
        this.orderAdID = orderAdID;
        this.orderNote = orderNote;
    }
    
    
    public String getOrderStatus(){
        if(orderSoID == 1) return "Pending";
        if(orderSoID == 2) return "Processing";
        if(orderSoID == 3) return "Shipping";
        if(orderSoID == 4) return "Delivered";
        if(orderSoID == 5) return "Rejected";
        return "Canceled";
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getOrderCusID() {
        return orderCusID;
    }

    public void setOrderCusID(int orderCusID) {
        this.orderCusID = orderCusID;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public double getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(double orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public String getOrderContactName() {
        return orderContactName;
    }

    public void setOrderContactName(String orderContactName) {
        this.orderContactName = orderContactName;
    }

    public String getOrderPhone() {
        return orderPhone;
    }

    public void setOrderPhone(String orderPhone) {
        this.orderPhone = orderPhone;
    }

    public String getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(String orderAddress) {
        this.orderAddress = orderAddress;
    }

    public int getOrderSoID() {
        return orderSoID;
    }

    public void setOrderSoID(int orderSoID) {
        this.orderSoID = orderSoID;
    }

    public int getOrderAdID() {
        return orderAdID;
    }

    public void setOrderAdID(int orderAdID) {
        this.orderAdID = orderAdID;
    }

    public String getOrderNote() {
        if(orderNote.length()>0 && orderNote.charAt(0) == '💵')
            return orderNote.substring(2);
        
        return orderNote;
    }

    public void setOrderNote(String orderNote) {
        this.orderNote = orderNote;
    }
    
    public String getRawNote(){
        return this.orderNote;
    }
    
    public String getPrice(){
        DecimalFormat decimalFormat = new DecimalFormat("###,###.###");
        String formattedNumber = decimalFormat.format(orderTotalPrice);
        return formattedNumber;
    }
    
    public String getPaymentMethod(){
        if(orderNote.length()>0 && orderNote.charAt(0) == '💵')
            return "Full payment";
        
        return "Deposit 500.000";
    }

    @Override
    public String toString() {
        return "Order{" + "orderID=" + orderID + ", orderCusID=" + orderCusID + ", orderDate=" + orderDate + ", orderTotalPrice=" + orderTotalPrice + ", orderContactName=" + orderContactName + ", orderPhone=" + orderPhone + ", orderAddress=" + orderAddress + ", orderSoID=" + orderSoID + ", orderAdID=" + orderAdID + ", orderNote=" + orderNote + '}';
    }
    
}
