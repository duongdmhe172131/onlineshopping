/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.ProductDetailDAO;

/**
 *
 * @author Dell
 */
public class ProductOfOrder {

    private int proID;
    private int proCateID;
    private String proName;
    private float proPrice;
    private String proImage;
    private String proDetail;
    private boolean proStatus;
    private int odQuantity;
    private int odOrderID;
    private String pdColor;
    
    private int pdId;

    public ProductOfOrder() {
    }

    public ProductOfOrder(int proID, int proCateID, String proName, float proPrice, String proImage, String proDetail, boolean proStatus, int odQuantity, int odOrderID, String pdColor) {
        this.proID = proID;
        this.proCateID = proCateID;
        this.proName = proName;
        this.proPrice = proPrice;
        this.proImage = proImage;
        this.proDetail = proDetail;
        this.proStatus = proStatus;
        this.odQuantity = odQuantity;
        this.odOrderID = odOrderID;
        this.pdColor = pdColor;
    }

    public int getPdId() {
        return pdId;
    }

    public void setPdId(int pdId) {
        this.pdId = pdId;
    }
    
    public int getProID() {
        return proID;
    }

    public void setProID(int proID) {
        this.proID = proID;
    }

    public int getProCateID() {
        return proCateID;
    }

    public void setProCateID(int proCateID) {
        this.proCateID = proCateID;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public float getProPrice() {
        return proPrice;
    }

    public void setProPrice(float proPrice) {
        this.proPrice = proPrice;
    }

    public String getProImage() {
        return proImage;
    }

    public void setProImage(String proImage) {
        this.proImage = proImage;
    }

    public String getProDetail() {
        return proDetail;
    }

    public void setProDetail(String proDetail) {
        this.proDetail = proDetail;
    }

    public boolean isProStatus() {
        return proStatus;
    }

    public void setProStatus(boolean proStatus) {
        this.proStatus = proStatus;
    }

    public int getOdQuantity() {
        return odQuantity;
    }

    public void setOdQuantity(int odQuantity) {
        this.odQuantity = odQuantity;
    }

    public int getOdOrderID() {
        return odOrderID;
    }

    public void setOdOrderID(int odOrderID) {
        this.odOrderID = odOrderID;
    }

    public String getPdColor() {
        return pdColor;
    }

    public void setPdColor(String pdColor) {
        this.pdColor = pdColor;
    }
    
    public String getPdImage() {
        return new ProductDetailDAO().getProductDetailImage(pdId+"");
    }

    @Override
    public String toString() {
        return "ProductOfOrder{" + "proID=" + proID + ", proCateID=" + proCateID + ", proName=" + proName + ", proPrice=" + proPrice + ", proImage=" + proImage + ", proDetail=" + proDetail + ", proStatus=" + proStatus + ", odQuantity=" + odQuantity + ", odOrderID=" + odOrderID + ", pdColor=" + pdColor + '}';
    }
    
}
