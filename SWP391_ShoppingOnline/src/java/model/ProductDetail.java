/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.ProductDetailDAO;

/**
 *
 * @author huytu
 */
public class ProductDetail {
    private int pdID;
    private int	pdProID;
    private String pdColor;
    private int	pdSold;
    private int	pdStatus;

    public ProductDetail() {
    }

    public ProductDetail(int pdID, int pdProID, String pdColor, int pdSold, int pdStatus) {
        this.pdID = pdID;
        this.pdProID = pdProID;
        this.pdColor = pdColor;
        this.pdSold = pdSold;
        this.pdStatus = pdStatus;
    }

    public int getPdID() {
        return pdID;
    }

    public void setPdID(int pdID) {
        this.pdID = pdID;
    }

    public int getPdProID() {
        return pdProID;
    }

    public void setPdProID(int pdProID) {
        this.pdProID = pdProID;
    }

    public String getPdColor() {
        return pdColor;
    }

    public void setPdColor(String pdColor) {
        this.pdColor = pdColor;
    }

    public int getPdSold() {
        return pdSold;
    }

    public void setPdSold(int pdSold) {
        this.pdSold = pdSold;
    }

    public int getPdStatus() {
        return pdStatus;
    }

    public void setPdStatus(int pdStatus) {
        this.pdStatus = pdStatus;
    }

    public String getPdImage() {
        return new ProductDetailDAO().getProductDetailImage(pdID+"");
    }
    
    @Override
    public String toString() {
        return "ProductDetail{" + "pdID=" + pdID + ", pdProID=" + pdProID + ", pdColor=" + pdColor + ", pdSold=" + pdSold + ", pdStatus=" + pdStatus + '}';
    }
    
    
}
