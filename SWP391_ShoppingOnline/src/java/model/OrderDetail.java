/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Dell
 */
public class OrderDetail {

    private int odID;
    private int odOrderID;
    private int odPdID;
    private int odQuantity;
    private double odPrice;

    public OrderDetail() {
    }

    public OrderDetail(int odID, int odOrderID, int odPdID, int odQuantity, double odPrice) {
        this.odID = odID;
        this.odOrderID = odOrderID;
        this.odPdID = odPdID;
        this.odQuantity = odQuantity;
        this.odPrice = odPrice;
    }

    public int getOdID() {
        return odID;
    }

    public void setOdID(int odID) {
        this.odID = odID;
    }

    public int getOdOrderID() {
        return odOrderID;
    }

    public void setOdOrderID(int odOrderID) {
        this.odOrderID = odOrderID;
    }

    public int getOdPdID() {
        return odPdID;
    }

    public void setOdPdID(int odPdID) {
        this.odPdID = odPdID;
    }

    public int getOdQuantity() {
        return odQuantity;
    }

    public void setOdQuantity(int odQuantity) {
        this.odQuantity = odQuantity;
    }

    public double getOdPrice() {
        return odPrice;
    }

    public void setOdPrice(double odPrice) {
        this.odPrice = odPrice;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "odID=" + odID + ", odOrderID=" + odOrderID + ", odPdID=" + odPdID + ", odQuantity=" + odQuantity + ", odPrice=" + odPrice + '}';
    }
    
}
