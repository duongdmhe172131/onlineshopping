package model;

public class Slider {
    private int sliderID;
    private String sliderTitle;
    private String sliderImage;
    private String sliderNote;
    private int sliderStatus;

    public Slider() {
        // Default constructor
    }
    
    public Slider(int sliderID, String sliderTitle, String sliderImage, String sliderNote, int sliderStatus) {
        this.sliderID = sliderID;
        this.sliderTitle = sliderTitle;
        this.sliderImage = sliderImage;
        this.sliderNote = sliderNote;
        this.sliderStatus = sliderStatus;
    }

    public Slider(String sliderTitle, String sliderImage, String sliderNote, int sliderStatus) {
        this.sliderTitle = sliderTitle;
        this.sliderImage = sliderImage;
        this.sliderNote = sliderNote;
        this.sliderStatus = sliderStatus;
    }

    public int getSliderID() {
        return sliderID;
    }

    public void setSliderID(int sliderID) {
        this.sliderID = sliderID;
    }

    public String getSliderTitle() {
        return sliderTitle;
    }

    public void setSliderTitle(String sliderTitle) {
        this.sliderTitle = sliderTitle;
    }

    public String getSliderImage() {
        return sliderImage;
    }

    public void setSliderImage(String sliderImage) {
        this.sliderImage = sliderImage;
    }

    public String getSliderNote() {
        return sliderNote;
    }

    public void setSliderNote(String sliderNote) {
        this.sliderNote = sliderNote;
    }

    public int getSliderStatus() {
        return sliderStatus;
    }

    public void setSliderStatus(int sliderStatus) {
        this.sliderStatus = sliderStatus;
    }

    @Override
    public String toString() {
        return "Slider{" +
                "sliderID=" + sliderID +
                ", sliderTitle='" + sliderTitle + '\'' +
                ", sliderImage='" + sliderImage + '\'' +
                ", sliderNote='" + sliderNote + '\'' +
                ", sliderStatus=" + sliderStatus +
                '}';
    }
}

