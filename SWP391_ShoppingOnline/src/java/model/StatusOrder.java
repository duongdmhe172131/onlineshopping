package model;

public class StatusOrder {
    private int soID;
    private String soName;
    private int soStatus;

    public StatusOrder() {
        // Default constructor
    }

    public StatusOrder(int soID, String soName, int soStatus) {
        this.soID = soID;
        this.soName = soName;
        this.soStatus = soStatus;
    }

    public int getSoID() {
        return soID;
    }

    public void setSoID(int soID) {
        this.soID = soID;
    }

    public String getSoName() {
        return soName;
    }

    public void setSoName(String soName) {
        this.soName = soName;
    }

    public int getSoStatus() {
        return soStatus;
    }

    public void setSoStatus(int soStatus) {
        this.soStatus = soStatus;
    }

    @Override
    public String toString() {
        return "StatusOrder{" +
                "soID=" + soID +
                ", soName='" + soName + '\'' +
                ", soStatus=" + soStatus +
                '}';
    }
}

