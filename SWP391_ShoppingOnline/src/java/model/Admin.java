/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.AdminDAO;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author BEAN
 */
public class Admin{
    private int adId;
    private String adLastName;
    private String adFirstName;
    private String adUsername;
    private String adPassword;
    private String adImage;
    private Boolean adGender;
    private Date adDob;
    private String adPhone;
    private String adEmail;
    private String adAddress;
    private int adStatus;
    private int adRoleId;

    public Admin() {
    }

    public Admin(int adId, String adLastName, String adFirstName, String adUsername, String adPassword, String adImage, Boolean adGender, Date adDob, String adPhone, String adEmail, String adAddress, int adStatus, int adRoleId) {
        this.adId = adId;
        this.adLastName = adLastName;
        this.adFirstName = adFirstName;
        this.adUsername = adUsername;
        this.adPassword = adPassword;
        this.adImage = adImage;
        this.adGender = adGender;
        this.adDob = adDob;
        this.adPhone = adPhone;
        this.adEmail = adEmail;
        this.adAddress = adAddress;
        this.adStatus = adStatus;
        this.adRoleId = adRoleId;
    }

    public int getAdId() {
        return adId;
    }

    public void setAdId(int adId) {
        this.adId = adId;
    }

    public String getAdLastName() {
        return adLastName;
    }

    public void setAdLastName(String adLastName) {
        this.adLastName = adLastName;
    }

    public String getAdFirstName() {
        return adFirstName;
    }

    public void setAdFirstName(String adFirstName) {
        this.adFirstName = adFirstName;
    }

    public String getAdUsername() {
        return adUsername;
    }

    public void setAdUsername(String adUsername) {
        this.adUsername = adUsername;
    }

    public String getAdPassword() {
        return adPassword;
    }

    public void setAdPassword(String adPassword) {
        this.adPassword = adPassword;
    }

    public String getAdImage() {
        return adImage;
    }

    public void setAdImage(String adImage) {
        this.adImage = adImage;
    }

    public Boolean getAdGender() {
        return adGender;
    }

    public void setAdGender(Boolean adGender) {
        this.adGender = adGender;
    }

    public Date getAdDob() {
        return adDob;
    }

    public void setAdDob(Date adDob) {
        this.adDob = adDob;
    }

    public String getAdPhone() {
        return adPhone;
    }

    public void setAdPhone(String adPhone) {
        this.adPhone = adPhone;
    }

    public String getAdEmail() {
        return adEmail;
    }

    public void setAdEmail(String adEmail) {
        this.adEmail = adEmail;
    }

    public String getAdAddress() {
        return adAddress;
    }

    public void setAdAddress(String adAddress) {
        this.adAddress = adAddress;
    }

    public int getAdStatus() {
        return adStatus;
    }

    public void setAdStatus(int adStatus) {
        this.adStatus = adStatus;
    }

    public int getAdRoleId() {
        return adRoleId;
    }

    public void setAdRoleId(int adRoleId) {
        this.adRoleId = adRoleId;
    }
    
    
    public String getFullName() {
        return adFirstName + " " + adLastName;
    }
    
    public String getRoleName() {
        AdminDAO adminDAO = new AdminDAO();
        String name = adminDAO.getRoleName(adRoleId);
        return name;
    }

    @Override
    public String toString() {
        return "Customer{" + "adId=" + adId + ", adLastName=" + adLastName + ", adFirstName=" + adFirstName + ", adUsername=" + adUsername + ", adPassword=" + adPassword + ", adImage=" + adImage + ", adGender=" + adGender + ", Dob=" + adDob + ", adPhone=" + adPhone + ", adEmail=" + adEmail + ", adAddress=" + adAddress + ", adStatus=" + adStatus + ", adRoleId=" + adRoleId + '}';
    }
    
    
     
    
}
