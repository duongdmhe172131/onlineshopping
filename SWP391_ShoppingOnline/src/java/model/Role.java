package model;

public class Role {
    private int roleID;
    private String roleName;
    private int roleStatus;

    public Role() {
        // Default constructor
    }

    public Role(int roleID, String roleName, int roleStatus) {
        this.roleID = roleID;
        this.roleName = roleName;
        this.roleStatus = roleStatus;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(int roleStatus) {
        this.roleStatus = roleStatus;
    }

    @Override
    public String toString() {
        return "Role{" +
               "roleID=" + roleID +
               ", roleName='" + roleName + '\'' +
               ", roleStatus=" + roleStatus +
               '}';
    }
}

