/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.ProductDetailDAO;
import java.sql.Date;

/**
 *
 * @author BEAN
 */
public class ProductDetailStock {
    private int proID;
    private int proCateID;
    private String proName;
    private double proPrice;
    private String proImage;
    private String proDetail;
    private Date proCreateDate;
    private int proAdID;
    private int proStatus;
   
    
    private int pdId;
    private String pdColor;
    private int pdAvailable;
    private int pdSold;
    private Date pdCreateDate;
    private int pdStatus;

    public ProductDetailStock() {
    }

    public ProductDetailStock(int proID, int proCateID, String proName, double proPrice, String proImage, String proDetail, Date proCreateDate, int proAdID, int proStatus, int pdId, String pdColor, int pdAvailable, int pdSold, Date pdCreateDate, int pdStatus) {
        this.proID = proID;
        this.proCateID = proCateID;
        this.proName = proName;
        this.proPrice = proPrice;
        this.proImage = proImage;
        this.proDetail = proDetail;
        this.proCreateDate = proCreateDate;
        this.proAdID = proAdID;
        this.proStatus = proStatus;
        this.pdId = pdId;
        this.pdColor = pdColor;
        this.pdAvailable = pdAvailable;
        this.pdSold = pdSold;
        this.pdCreateDate = pdCreateDate;
        this.pdStatus = pdStatus;
    }

    public int getProID() {
        return proID;
    }

    public void setProID(int proID) {
        this.proID = proID;
    }

    public int getProCateID() {
        return proCateID;
    }

    public void setProCateID(int proCateID) {
        this.proCateID = proCateID;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public double getProPrice() {
        return proPrice;
    }

    public void setProPrice(double proPrice) {
        this.proPrice = proPrice;
    }

    public String getProImage() {
        return proImage;
    }

    public void setProImage(String proImage) {
        this.proImage = proImage;
    }

    public String getProDetail() {
        return proDetail;
    }

    public void setProDetail(String proDetail) {
        this.proDetail = proDetail;
    }

    public Date getProCreateDate() {
        return proCreateDate;
    }

    public void setProCreateDate(Date proCreateDate) {
        this.proCreateDate = proCreateDate;
    }

    public int getProAdID() {
        return proAdID;
    }

    public void setProAdID(int proAdID) {
        this.proAdID = proAdID;
    }

    public int getProStatus() {
        return proStatus;
    }

    public void setProStatus(int proStatus) {
        this.proStatus = proStatus;
    }

    public int getPdId() {
        return pdId;
    }

    public void setPdId(int pdId) {
        this.pdId = pdId;
    }

    public String getPdColor() {
        return pdColor;
    }

    public void setPdColor(String pdColor) {
        this.pdColor = pdColor;
    }

    public int getPdAvailable() {
        return pdAvailable;
    }

    public void setPdAvailable(int pdAvailable) {
        this.pdAvailable = pdAvailable;
    }

    public int getPdSold() {
        return pdSold;
    }

    public void setPdSold(int pdSold) {
        this.pdSold = pdSold;
    }

    public Date getPdCreateDate() {
        return pdCreateDate;
    }

    public void setPdCreateDate(Date pdCreateDate) {
        this.pdCreateDate = pdCreateDate;
    }

    public int getPdStatus() {
        return pdStatus;
    }

    public void setPdStatus(int pdStatus) {
        this.pdStatus = pdStatus;
    }
    
    public String getPdImage() {
        return new ProductDetailDAO().getProductDetailImage(pdId+"");
    }

    @Override
    public String toString() {
        return "ProductDetailStock{" + "proID=" + proID + ", proCateID=" + proCateID + ", proName=" + proName + ", proPrice=" + proPrice + ", proImage=" + proImage + ", proDetail=" + proDetail + ", proCreateDate=" + proCreateDate + ", proAdID=" + proAdID + ", proStatus=" + proStatus + ", pdId=" + pdId + ", pdColor=" + pdColor + ", pdAvailable=" + pdAvailable + ", pdSold=" + pdSold + ", pdCreateDate=" + pdCreateDate + ", pdStatus=" + pdStatus + '}';
    }

    
    
    
            
}
