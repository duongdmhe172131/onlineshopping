/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author BEAN
 */
public class Customer {
    private int cusId;
    private String cusContactName;
    private String cusUsername;
    private String cusPassword;
    private String cusImage;
    private boolean cusGender;
    private Date cusDob;
    private String cusPhone;
    private String cusEmail;
    private String cusAddress;
    private int cusStatus;

    public Customer() {
    }
    
    public Customer(String cusContactName, String cusPhone, String cusAddress) {
        this.cusContactName = cusContactName;
        this.cusPhone = cusPhone;
        this.cusAddress = cusAddress;
    }
    
    

    public Customer(int cusId, String cusContactName, String cusUsername, String cusPassword, String cusImage, boolean cusGender, Date cusDob, String cusPhone, String cusEmail, String cusAddress, int cusStatus) {
        this.cusId = cusId;
        this.cusContactName = cusContactName;
        this.cusUsername = cusUsername;
        this.cusPassword = cusPassword;
        this.cusImage = cusImage;
        this.cusGender = cusGender;
        this.cusDob = cusDob;
        this.cusPhone = cusPhone;
        this.cusEmail = cusEmail;
        this.cusAddress = cusAddress;
        this.cusStatus = cusStatus;
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public String getCusContactName() {
        return cusContactName;
    }

    public void setCusContactName(String cusContactName) {
        this.cusContactName = cusContactName;
    }

    public String getCusUsername() {
        return cusUsername;
    }

    public void setCusUsername(String cusUsername) {
        this.cusUsername = cusUsername;
    }

    public String getCusPassword() {
        return cusPassword;
    }

    public void setCusPassword(String cusPassword) {
        this.cusPassword = cusPassword;
    }

    public String getCusImage() {
        return cusImage;
    }

    public void setCusImage(String cusImage) {
        this.cusImage = cusImage;
    }

    public boolean isCusGender() {
        return cusGender;
    }

    public void setCusGender(boolean cusGender) {
        this.cusGender = cusGender;
    }

    public Date getCusDob() {
        return cusDob;
    }

    public void setCusDob(Date cusDob) {
        this.cusDob = cusDob;
    }

    public String getCusPhone() {
        return cusPhone;
    }

    public void setCusPhone(String cusPhone) {
        this.cusPhone = cusPhone;
    }

    public String getCusEmail() {
        return cusEmail;
    }

    public void setCusEmail(String cusEmail) {
        this.cusEmail = cusEmail;
    }

    public String getCusAddress() {
        return cusAddress;
    }

    public void setCusAddress(String cusAddress) {
        this.cusAddress = cusAddress;
    }

    public int getCusStatus() {
        return cusStatus;
    }

    public void setCusStatus(int cusStatus) {
        this.cusStatus = cusStatus;
    }
    
    @Override
    public String toString() {
        return "Customer{" + "cusId=" + cusId + ", cusContactName=" + cusContactName + ", cusUsername=" + cusUsername + ", cusPassword=" + cusPassword + ", cusImage=" + cusImage + ", cusGender=" + cusGender + ", cusDob=" + cusDob + ", cusPhone=" + cusPhone + ", cusEmail=" + cusEmail + ", cusAddress=" + cusAddress + ", cusStatus=" + cusStatus + '}';
    }
           
    
    
    
}
